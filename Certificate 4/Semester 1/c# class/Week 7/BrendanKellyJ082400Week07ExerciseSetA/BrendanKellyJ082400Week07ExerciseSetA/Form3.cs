﻿// Date: 30/08/2018
// Programmer: Brendan Kelly
// Purpose: Various list box demonstrations

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace BrendanKellyJ082400Week07ExerciseSetA
{
    public partial class Form3 : Form
    {
        List<string> str_names = new List<string>();
        
        

        public Form3()
        {
            InitializeComponent();

        }
        
        


        private void btn_number_of_names_Click(object sender, EventArgs e)
        {
            
            MessageBox.Show("There are " + lst_output.Items.Count + " names in the list box");
        }

        private void btn_add_Click(object sender, EventArgs e)
        {
            string str_input = txt_input.Text;
            lst_output.Items.Add(str_input);

        }

        private void btn_sort_Click(object sender, EventArgs e)
        {
            lst_output.Sorted = true;
        }

        private void btn_delete_entered_Click(object sender, EventArgs e)
        {
            int int_find_string = lst_output.FindString(txt_input.Text);
            if (int_find_string < 0)
            {
                MessageBox.Show("Unable to find that name");
            }
            else
            {
                lst_output.Items.RemoveAt(lst_output.FindString(txt_input.Text));
            }
        }

        private void btn_delete_selected_Click(object sender, EventArgs e)
        {
            if (lst_output.SelectedIndex < 0)
            {
                MessageBox.Show("Please selected an option before trying to delete", "Error");
            }
            else
            {
                lst_output.Items.RemoveAt(lst_output.SelectedIndex);
            }
        }
    }
}
