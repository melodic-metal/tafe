import java.util.Scanner;

public class CountMovieSpaces2 {

   public static void main(String[] args) {
   
      System.out.print("Please enter a movie quote >> ");
      Scanner keyboard = new Scanner(System.in);
      String userInput = keyboard.nextLine();
      int totalWhitespace = 0;
      
      for (int i = 0; i < userInput.length(); i++) {
         if (userInput.charAt(i) == ' ') {
            totalWhitespace++;
         }
      }
      System.out.println("There are " + totalWhitespace + " whitespaces in the string");      
   
   }

}