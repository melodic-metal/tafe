﻿// Programmer : Brendan Kelly 
// Date : August 2018
// Purpose : This application calculates a square of a number

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace BrendanKellyJ082400Examples
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void btn_calc_Click(object sender, EventArgs e)
        {
            double dblNo1 = double.Parse(txt_input.Text);
            double dblResult = Math.Pow(dblNo1, 2);

            txt_output.Text = dblResult.ToString("F2");

        }
    }
}
