// Date: 25/08/2018
// Programmer: Brendan Kelly J082400
// Purpose: Count by 3's

public class CountByThrees {

   public static void main(String[] args) {
      int number = 100;
      for(int i = 3; i <= number; i++) {
         if (i % 3 ==0) {
            System.out.print(i + " ");
         }
         if (i % 10 == 0) {
            System.out.println();
         }

      }

    
      
   }
      
}
   

