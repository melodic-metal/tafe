﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace BrendanKellyj082400Week04ExercisesSetA
{
    public partial class Form5 : Form
    {
        public Form5()
        {
            InitializeComponent();
        }

        private void btn_calc_Click(object sender, EventArgs e)
        {
            int int_start_odo, int_end_odo, int_num_days, int_total_km, int_free_km, int_per_days_cost; // set odometer to 0
            double dec_cost; // set cost to $0
            if (!int.TryParse(txt_days.Text, out int_num_days) || !int.TryParse(txt_start_odo.Text, out int_start_odo)
                || !int.TryParse(txt_end_odo.Text, out int_end_odo)) // check inputs to see if they're valid
            {
                MessageBox.Show("Please check inputs again");
                return;
            }

            if (int_start_odo > int_end_odo)
            {
                MessageBox.Show("Start odomoter cannot be less than end odometer\nPlease check inputs again");
                // start odometer cannot be less than end odometer. Check input, then reset all the fields
                txt_days.Clear();
                txt_end_odo.Clear();
                txt_start_odo.Clear();
                txt_days.Focus();
                return;
            }

            int_total_km = int_end_odo - int_start_odo; // find number of km driven
            int_per_days_cost = int_num_days * 24; // find total of per day costs
            int_free_km = int_num_days * 100; // check how many free KM the client gets
            dec_cost = ((int_total_km - int_free_km) * .10) + int_per_days_cost; // 
            txt_cost.Text = dec_cost.ToString(); 
            
        }
    }
}
