﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace BrendanKellyJ082400Week05ExerciseSetA
{
    public partial class Form2 : Form
    {
        public Form2()
        {
            InitializeComponent();
        }

        private void btn_calc_Click(object sender, EventArgs e)
        {
            int i = 0, int_userInput;
            int.TryParse(txt_input.Text, out int_userInput);
            lst_table.Items.Add("Number\t\tX" + int_userInput);
            while (i <= 12)
            {
                lst_table.Items.Add(i + " X " + int_userInput.ToString() + " = \t\t" + (i * int_userInput).ToString());
                i++;
            }
        }
    }
}
