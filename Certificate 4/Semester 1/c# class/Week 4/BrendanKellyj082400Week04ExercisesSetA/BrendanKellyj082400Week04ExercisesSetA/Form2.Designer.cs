﻿namespace BrendanKellyj082400Week04ExercisesSetA
{
    partial class Form2
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.txt_start = new System.Windows.Forms.TextBox();
            this.lbl_start = new System.Windows.Forms.Label();
            this.lbl_end = new System.Windows.Forms.Label();
            this.txt_end = new System.Windows.Forms.TextBox();
            this.lbl_cost = new System.Windows.Forms.Label();
            this.txt_cost = new System.Windows.Forms.TextBox();
            this.btn_calc = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // txt_start
            // 
            this.txt_start.Location = new System.Drawing.Point(149, 64);
            this.txt_start.Name = "txt_start";
            this.txt_start.Size = new System.Drawing.Size(100, 20);
            this.txt_start.TabIndex = 0;
            // 
            // lbl_start
            // 
            this.lbl_start.AutoSize = true;
            this.lbl_start.Location = new System.Drawing.Point(65, 67);
            this.lbl_start.Name = "lbl_start";
            this.lbl_start.Size = new System.Drawing.Size(72, 13);
            this.lbl_start.TabIndex = 1;
            this.lbl_start.Text = "Start Reading";
            // 
            // lbl_end
            // 
            this.lbl_end.AutoSize = true;
            this.lbl_end.Location = new System.Drawing.Point(65, 107);
            this.lbl_end.Name = "lbl_end";
            this.lbl_end.Size = new System.Drawing.Size(69, 13);
            this.lbl_end.TabIndex = 3;
            this.lbl_end.Text = "End Reading";
            // 
            // txt_end
            // 
            this.txt_end.Location = new System.Drawing.Point(149, 104);
            this.txt_end.Name = "txt_end";
            this.txt_end.Size = new System.Drawing.Size(100, 20);
            this.txt_end.TabIndex = 2;
            // 
            // lbl_cost
            // 
            this.lbl_cost.AutoSize = true;
            this.lbl_cost.Location = new System.Drawing.Point(106, 160);
            this.lbl_cost.Name = "lbl_cost";
            this.lbl_cost.Size = new System.Drawing.Size(28, 13);
            this.lbl_cost.TabIndex = 5;
            this.lbl_cost.Text = "Cost";
            // 
            // txt_cost
            // 
            this.txt_cost.Enabled = false;
            this.txt_cost.Location = new System.Drawing.Point(149, 157);
            this.txt_cost.Name = "txt_cost";
            this.txt_cost.Size = new System.Drawing.Size(100, 20);
            this.txt_cost.TabIndex = 4;
            // 
            // btn_calc
            // 
            this.btn_calc.Location = new System.Drawing.Point(114, 205);
            this.btn_calc.Name = "btn_calc";
            this.btn_calc.Size = new System.Drawing.Size(75, 23);
            this.btn_calc.TabIndex = 6;
            this.btn_calc.Text = "Calculate";
            this.btn_calc.UseVisualStyleBackColor = true;
            this.btn_calc.Click += new System.EventHandler(this.btn_calc_Click);
            // 
            // Form2
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(284, 261);
            this.Controls.Add(this.btn_calc);
            this.Controls.Add(this.lbl_cost);
            this.Controls.Add(this.txt_cost);
            this.Controls.Add(this.lbl_end);
            this.Controls.Add(this.txt_end);
            this.Controls.Add(this.lbl_start);
            this.Controls.Add(this.txt_start);
            this.Name = "Form2";
            this.Text = "Form2";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox txt_start;
        private System.Windows.Forms.Label lbl_start;
        private System.Windows.Forms.Label lbl_end;
        private System.Windows.Forms.TextBox txt_end;
        private System.Windows.Forms.Label lbl_cost;
        private System.Windows.Forms.TextBox txt_cost;
        private System.Windows.Forms.Button btn_calc;
    }
}