import java.util.Scanner;

public class CharacterInfo2 {

   public static void main(String[] args) {
      char userInput;
      
      System.out.println("Please enter a character >> ");
      Scanner keyboard = new Scanner(System.in);
      userInput = keyboard.next().charAt(0);
      if (Character.isUpperCase(userInput)) {
         System.out.println(userInput + " is upper case");
      }
      else {
         System.out.println(userInput + " is lower case");
      }
      
      userInput = Character.toLowerCase(userInput);
      System.out.println("After toLowerCase(), userInput is " + userInput);
      
      userInput = Character.toUpperCase(userInput);
      System.out.println("After toUpperCase(), userInput is " + userInput);
      
      if(Character.isLetterOrDigit(userInput)) {
         System.out.println(userInput + " is a letter or digit");
      }
      else {
         System.out.println(userInput + " is neither a letter nor a digit");
      }
      if (Character.isWhitespace(userInput)) {
         System.out.println(userInput + " is whitespace");
      }
      else
         System.out.println(userInput + " is not whitespace");          
   
   }

}