/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author kellyb
 */
package week.pkg13;
import javax.swing.*;
import java.awt.*;       

public class JFrameWithComponents extends JFrame {
    /**
     * @param args the command line arguments
     */
    JLabel label = new JLabel("Enter your name:");
    JTextField field = new JTextField(12);
    JButton button = new JButton("OK");
    
    public JFrameWithComponents() {
        super("My first frame");
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setLayout(new FlowLayout());
        add(label);
        add(field);
        add(button);
    }
   
   
}
