﻿namespace BrendanKellyJ082400Assessment1
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btn_inches = new System.Windows.Forms.Button();
            this.btn_feet = new System.Windows.Forms.Button();
            this.btn_yards = new System.Windows.Forms.Button();
            this.btn_miles = new System.Windows.Forms.Button();
            this.lbl_input = new System.Windows.Forms.Label();
            this.txt_input = new System.Windows.Forms.TextBox();
            this.lbl_equals = new System.Windows.Forms.Label();
            this.lbl_after = new System.Windows.Forms.Label();
            this.txt_output = new System.Windows.Forms.TextBox();
            this.btn_exit = new System.Windows.Forms.Button();
            this.grp_conversions = new System.Windows.Forms.GroupBox();
            this.lbl_before = new System.Windows.Forms.Label();
            this.grp_conversions.SuspendLayout();
            this.SuspendLayout();
            // 
            // btn_inches
            // 
            this.btn_inches.Location = new System.Drawing.Point(20, 27);
            this.btn_inches.Name = "btn_inches";
            this.btn_inches.Size = new System.Drawing.Size(110, 23);
            this.btn_inches.TabIndex = 0;
            this.btn_inches.Text = "&Inches to CM";
            this.btn_inches.UseVisualStyleBackColor = true;
            this.btn_inches.Click += new System.EventHandler(this.btn_inches_Click);
            // 
            // btn_feet
            // 
            this.btn_feet.Location = new System.Drawing.Point(21, 56);
            this.btn_feet.Name = "btn_feet";
            this.btn_feet.Size = new System.Drawing.Size(109, 23);
            this.btn_feet.TabIndex = 1;
            this.btn_feet.Text = "&Feet to Metres";
            this.btn_feet.UseVisualStyleBackColor = true;
            this.btn_feet.Click += new System.EventHandler(this.btn_feet_Click);
            // 
            // btn_yards
            // 
            this.btn_yards.Location = new System.Drawing.Point(21, 86);
            this.btn_yards.Name = "btn_yards";
            this.btn_yards.Size = new System.Drawing.Size(109, 23);
            this.btn_yards.TabIndex = 2;
            this.btn_yards.Text = "&Yards to Metres";
            this.btn_yards.UseVisualStyleBackColor = true;
            this.btn_yards.Click += new System.EventHandler(this.btn_yards_Click);
            // 
            // btn_miles
            // 
            this.btn_miles.Location = new System.Drawing.Point(20, 114);
            this.btn_miles.Name = "btn_miles";
            this.btn_miles.Size = new System.Drawing.Size(110, 23);
            this.btn_miles.TabIndex = 3;
            this.btn_miles.Text = "&Miles To KM";
            this.btn_miles.UseVisualStyleBackColor = true;
            this.btn_miles.Click += new System.EventHandler(this.btn_miles_Click);
            // 
            // lbl_input
            // 
            this.lbl_input.AutoSize = true;
            this.lbl_input.Location = new System.Drawing.Point(2, 29);
            this.lbl_input.Name = "lbl_input";
            this.lbl_input.Size = new System.Drawing.Size(140, 13);
            this.lbl_input.TabIndex = 5;
            this.lbl_input.Text = "Enter Value to be converted";
            // 
            // txt_input
            // 
            this.txt_input.Location = new System.Drawing.Point(25, 54);
            this.txt_input.Name = "txt_input";
            this.txt_input.Size = new System.Drawing.Size(76, 20);
            this.txt_input.TabIndex = 6;
            this.txt_input.Enter += new System.EventHandler(this.txt_input_Enter);
            // 
            // lbl_equals
            // 
            this.lbl_equals.AutoSize = true;
            this.lbl_equals.Location = new System.Drawing.Point(181, 57);
            this.lbl_equals.Name = "lbl_equals";
            this.lbl_equals.Size = new System.Drawing.Size(13, 13);
            this.lbl_equals.TabIndex = 7;
            this.lbl_equals.Text = "=";
            this.lbl_equals.Visible = false;
            // 
            // lbl_after
            // 
            this.lbl_after.AutoSize = true;
            this.lbl_after.Location = new System.Drawing.Point(293, 57);
            this.lbl_after.Name = "lbl_after";
            this.lbl_after.Size = new System.Drawing.Size(35, 13);
            this.lbl_after.TabIndex = 8;
            this.lbl_after.Text = "label3";
            this.lbl_after.Visible = false;
            // 
            // txt_output
            // 
            this.txt_output.Location = new System.Drawing.Point(210, 54);
            this.txt_output.Name = "txt_output";
            this.txt_output.ReadOnly = true;
            this.txt_output.Size = new System.Drawing.Size(75, 20);
            this.txt_output.TabIndex = 9;
            this.txt_output.Visible = false;
            // 
            // btn_exit
            // 
            this.btn_exit.Location = new System.Drawing.Point(184, 206);
            this.btn_exit.Name = "btn_exit";
            this.btn_exit.Size = new System.Drawing.Size(75, 23);
            this.btn_exit.TabIndex = 10;
            this.btn_exit.Text = "E&xit";
            this.btn_exit.UseVisualStyleBackColor = true;
            this.btn_exit.Click += new System.EventHandler(this.btn_exit_Click);
            // 
            // grp_conversions
            // 
            this.grp_conversions.Controls.Add(this.btn_inches);
            this.grp_conversions.Controls.Add(this.btn_feet);
            this.grp_conversions.Controls.Add(this.btn_yards);
            this.grp_conversions.Controls.Add(this.btn_miles);
            this.grp_conversions.Location = new System.Drawing.Point(5, 92);
            this.grp_conversions.Name = "grp_conversions";
            this.grp_conversions.Size = new System.Drawing.Size(146, 148);
            this.grp_conversions.TabIndex = 11;
            this.grp_conversions.TabStop = false;
            this.grp_conversions.Text = "Conversions";
            // 
            // lbl_before
            // 
            this.lbl_before.AutoSize = true;
            this.lbl_before.Location = new System.Drawing.Point(107, 57);
            this.lbl_before.Name = "lbl_before";
            this.lbl_before.Size = new System.Drawing.Size(35, 13);
            this.lbl_before.TabIndex = 12;
            this.lbl_before.Text = "label1";
            this.lbl_before.Visible = false;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(381, 254);
            this.Controls.Add(this.lbl_before);
            this.Controls.Add(this.btn_exit);
            this.Controls.Add(this.txt_output);
            this.Controls.Add(this.lbl_after);
            this.Controls.Add(this.lbl_equals);
            this.Controls.Add(this.txt_input);
            this.Controls.Add(this.lbl_input);
            this.Controls.Add(this.grp_conversions);
            this.Name = "Form1";
            this.Text = "Form1";
            this.grp_conversions.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btn_inches;
        private System.Windows.Forms.Button btn_feet;
        private System.Windows.Forms.Button btn_yards;
        private System.Windows.Forms.Button btn_miles;
        private System.Windows.Forms.Label lbl_input;
        private System.Windows.Forms.TextBox txt_input;
        private System.Windows.Forms.Label lbl_equals;
        private System.Windows.Forms.Label lbl_after;
        private System.Windows.Forms.TextBox txt_output;
        private System.Windows.Forms.Button btn_exit;
        private System.Windows.Forms.GroupBox grp_conversions;
        private System.Windows.Forms.Label lbl_before;
    }
}

