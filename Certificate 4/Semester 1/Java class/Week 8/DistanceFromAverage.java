import java.util.Scanner;

public class DistanceFromAverage
{
   public static void main(String[] args)
   {
      double[] userValues = new double[20];
      int userInput;
      int x = 1, y = 0, total = 0;
      double average;
      Scanner keyboard = new Scanner(System.in);
      
      System.out.print("Enter an integer. 99999 to quit >> ");
      userInput = keyboard.nextInt();
      if(userInput == 99999)
         System.out.println("Please enter a value");
      while(userInput != 99999 && x < userValues.length)
      {
         userValues[x] = userInput;
         total = total + userInput;
         System.out.print("Enter an integer. 99999 to quit >> ");
         userInput = keyboard.nextInt();
         if(userInput == 99999)
            break;
         x++;
      }
      
      average = total / x;
      System.out.println("You entered " + x + " numbers and their average is " + average);
      for (y = 1; y < x; ++y)
      {
         System.out.println(userValues[y] + " is " + (userValues[y] - average) + " away from the average");
      }
   }
}