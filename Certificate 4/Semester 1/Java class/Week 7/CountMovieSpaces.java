public class CountMovieSpaces {

   public static void main(String[] args) {
   
      String movieQuote = "Ask yourself, do i feel lucky?";
      int totalWhitespace = 0;
      
      for (int i = 0; i < movieQuote.length(); i++) {
         if (movieQuote.charAt(i) == ' ') {
            totalWhitespace++;
         }
      }
      System.out.println("There are " + totalWhitespace + " whitespaces in the string");      
   
   }

}