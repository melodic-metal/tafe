﻿namespace WindowsFormsApp1
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lbl_adults = new System.Windows.Forms.Label();
            this.btn_fare = new System.Windows.Forms.Button();
            this.txt_adults = new System.Windows.Forms.TextBox();
            this.lbl_conc = new System.Windows.Forms.Label();
            this.txt_conc = new System.Windows.Forms.TextBox();
            this.lbl_total = new System.Windows.Forms.Label();
            this.lbl_received = new System.Windows.Forms.Label();
            this.txt_total = new System.Windows.Forms.TextBox();
            this.txt_amount = new System.Windows.Forms.TextBox();
            this.btn_change = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.txt_change = new System.Windows.Forms.TextBox();
            this.SuspendLayout();
            // 
            // lbl_adults
            // 
            this.lbl_adults.AutoSize = true;
            this.lbl_adults.Location = new System.Drawing.Point(71, 74);
            this.lbl_adults.Name = "lbl_adults";
            this.lbl_adults.Size = new System.Drawing.Size(87, 13);
            this.lbl_adults.TabIndex = 0;
            this.lbl_adults.Text = "Number of adults";
            // 
            // btn_fare
            // 
            this.btn_fare.Location = new System.Drawing.Point(144, 149);
            this.btn_fare.Name = "btn_fare";
            this.btn_fare.Size = new System.Drawing.Size(75, 23);
            this.btn_fare.TabIndex = 1;
            this.btn_fare.Text = "calc fare";
            this.btn_fare.UseVisualStyleBackColor = true;
            this.btn_fare.Click += new System.EventHandler(this.btn_fare_Click);
            // 
            // txt_adults
            // 
            this.txt_adults.Location = new System.Drawing.Point(181, 71);
            this.txt_adults.Name = "txt_adults";
            this.txt_adults.Size = new System.Drawing.Size(100, 20);
            this.txt_adults.TabIndex = 2;
            // 
            // lbl_conc
            // 
            this.lbl_conc.AutoSize = true;
            this.lbl_conc.Location = new System.Drawing.Point(74, 102);
            this.lbl_conc.Name = "lbl_conc";
            this.lbl_conc.Size = new System.Drawing.Size(47, 13);
            this.lbl_conc.TabIndex = 3;
            this.lbl_conc.Text = "lbl_conc";
            // 
            // txt_conc
            // 
            this.txt_conc.Location = new System.Drawing.Point(181, 102);
            this.txt_conc.Name = "txt_conc";
            this.txt_conc.Size = new System.Drawing.Size(100, 20);
            this.txt_conc.TabIndex = 4;
            // 
            // lbl_total
            // 
            this.lbl_total.AutoSize = true;
            this.lbl_total.Location = new System.Drawing.Point(74, 204);
            this.lbl_total.Name = "lbl_total";
            this.lbl_total.Size = new System.Drawing.Size(71, 13);
            this.lbl_total.TabIndex = 5;
            this.lbl_total.Text = "total fare cost";
            // 
            // lbl_received
            // 
            this.lbl_received.AutoSize = true;
            this.lbl_received.Location = new System.Drawing.Point(74, 236);
            this.lbl_received.Name = "lbl_received";
            this.lbl_received.Size = new System.Drawing.Size(86, 13);
            this.lbl_received.TabIndex = 6;
            this.lbl_received.Text = "amount received";
            // 
            // txt_total
            // 
            this.txt_total.Location = new System.Drawing.Point(181, 196);
            this.txt_total.Name = "txt_total";
            this.txt_total.Size = new System.Drawing.Size(100, 20);
            this.txt_total.TabIndex = 7;
            // 
            // txt_amount
            // 
            this.txt_amount.Location = new System.Drawing.Point(181, 228);
            this.txt_amount.Name = "txt_amount";
            this.txt_amount.Size = new System.Drawing.Size(100, 20);
            this.txt_amount.TabIndex = 8;
            // 
            // btn_change
            // 
            this.btn_change.Location = new System.Drawing.Point(144, 295);
            this.btn_change.Name = "btn_change";
            this.btn_change.Size = new System.Drawing.Size(75, 23);
            this.btn_change.TabIndex = 9;
            this.btn_change.Text = "calc change";
            this.btn_change.UseVisualStyleBackColor = true;
            this.btn_change.Click += new System.EventHandler(this.btn_change_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(77, 339);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(66, 13);
            this.label1.TabIndex = 10;
            this.label1.Text = "calc change";
            // 
            // txt_change
            // 
            this.txt_change.Location = new System.Drawing.Point(181, 339);
            this.txt_change.Name = "txt_change";
            this.txt_change.Size = new System.Drawing.Size(100, 20);
            this.txt_change.TabIndex = 11;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(363, 420);
            this.Controls.Add(this.txt_change);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.btn_change);
            this.Controls.Add(this.txt_amount);
            this.Controls.Add(this.txt_total);
            this.Controls.Add(this.lbl_received);
            this.Controls.Add(this.lbl_total);
            this.Controls.Add(this.txt_conc);
            this.Controls.Add(this.lbl_conc);
            this.Controls.Add(this.txt_adults);
            this.Controls.Add(this.btn_fare);
            this.Controls.Add(this.lbl_adults);
            this.Name = "Form1";
            this.Text = "Form1";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lbl_adults;
        private System.Windows.Forms.Button btn_fare;
        private System.Windows.Forms.TextBox txt_adults;
        private System.Windows.Forms.Label lbl_conc;
        private System.Windows.Forms.TextBox txt_conc;
        private System.Windows.Forms.Label lbl_total;
        private System.Windows.Forms.Label lbl_received;
        private System.Windows.Forms.TextBox txt_total;
        private System.Windows.Forms.TextBox txt_amount;
        private System.Windows.Forms.Button btn_change;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox txt_change;
    }
}

