// Date: 13/09/2018
// Programmer: Brendan Kelly 
// Purpose: set up fields for a limerick

public class Limerick extends Poem {
  
   private static String poemName;
   public Limerick(String name) {
      super(name, 5);
      poemName = name;
   }
   
   public static String getName() {
      return poemName;
   }

}