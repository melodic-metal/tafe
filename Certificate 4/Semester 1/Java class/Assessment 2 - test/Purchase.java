// Date: 04/10/2018
// Programmer: Brendan Kelly
// Purpose: Purchase class to set invoices and sales amount

public class Purchase {

   private int invoiceNumber;
   private double saleAmount;
   private final double salesTax = .07; // initialise the required variables
   
   public Purchase (int invoiceNo, double amount) {
      invoiceNumber = invoiceNo;
      amount = amount + (amount * salesTax);
      saleAmount = amount;
      //create constructor with the required information
   }

   
   public int getInvoiceNumber() {
      return invoiceNumber; // return invoice number
   }

   
   public double getSaleAmount() { 
      return saleAmount; // return the sale amount
   }
   
   public void displayInformation() {
   
      System.out.println("Invoice number: " + invoiceNumber + "; sales amount: " + saleAmount);
   }   
   
}   