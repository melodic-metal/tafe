setInterval(writeTime, 1000);
setInterval(changeToBeep, 15000);
let hoursArea = document.getElementById("hours");
let minutesArea = document.getElementById("minutes");
let secondsArea = document.getElementById("seconds");

function writeTime() {

    hoursArea.innerText = moment().format("HH");
    minutesArea.innerText = moment().format("mm");
    secondsArea.innerText = moment().format("ss");

}

function changeToBeep() {
    hoursArea.innerText = "beep";
    minutesArea.innerText = "beep";
    secondsArea.innerText = "beep";

}