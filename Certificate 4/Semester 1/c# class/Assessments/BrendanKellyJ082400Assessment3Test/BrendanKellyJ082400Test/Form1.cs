﻿// Date: 4/10/18
// Programmer: Brendan Kelly J082400
// Purpose: 

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace BrendanKellyJ082400Test
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
            txt_ticket_cost.ReadOnly = true;
            txt_change_back.ReadOnly = true; //Set the two outputs to read-only
        }
        decimal total_ticket_cost = 0; 
        private void btn_ticket_cost_Click(object sender, EventArgs e)
        {
            
            int int_adults = 0;
            int int_children = 0;
            const decimal CHILD_TICKET = 30.5m;
            const decimal ADULT_TICKET = 50.0m; //initialise all the required variables and constants
            if(!int.TryParse(txt_adults.Text, out int_adults))
            {
                MessageBox.Show("Please enter valid number into Adults textbox");
                txt_adults.Clear();
                txt_adults.Focus();
                // data validation for the Adults textbox
            }
            else if (!int.TryParse(txt_children.Text, out int_children))
            {
                MessageBox.Show("Please enter valid number into Children textbox");
                txt_children.Clear();
                txt_children.Focus();
                // data validation for the Children textbox
            }

            total_ticket_cost = (int_children * CHILD_TICKET) + (int_adults * ADULT_TICKET); // Perform the arithmetic

            txt_ticket_cost.Text = total_ticket_cost.ToString("C"); // output the information
        }

        private void btn_change_Click(object sender, EventArgs e)
        {
            decimal dec_change_back; //initialise the variable

            if (!decimal.TryParse(txt_amount_from.Text, out decimal dec_amount_from))
            {
                MessageBox.Show("Please enter a valid amount of money into textbox");
                // data validation for the money-from-customer field
            }

            else if (dec_amount_from < total_ticket_cost)
            {
                MessageBox.Show("Money from customer cannot be less than ticket cost");
                return;
                // money from customer cannot be less than ticket cost
            }

            dec_change_back = dec_amount_from - total_ticket_cost; // perform the arithmetic

            txt_change_back.Text = dec_change_back.ToString("c"); // output the information
        }
    }
}
