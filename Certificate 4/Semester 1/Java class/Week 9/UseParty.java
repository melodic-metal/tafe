// Date: 13/09/2018
// Programmer: Brendan Kelly
// Purpose:

import java.util.*;

public class UseParty {

   public static void main(String[] args) {
   
      int guests;
      Party aParty = new Party();
      Scanner keyboard = new Scanner(System.in);
      
      System.out.println("Enter number of guests");
      guests = keyboard.nextInt();
      aParty.setGuests(guests);
      System.out.println("The party has " + aParty.getGuests() + " guests");
      aParty.displayInvitation();
   
   }
   

}