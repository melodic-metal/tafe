/**
 *
 *
 * Calculations in JS
 *
 *
 * @File:       /Ep02/js/ex-04.js
 * @Project:    Classwork
 * @Author:     Brendan Kelly <j082400@tafe.wa.edu.au>
 * @Version:    1.0
 * @Copyright:  Brendan Kelly 2018
 *
 * History:
 * v1.0    2018-07-24
 *         Calculations and stuff
 *
 * v0.1    2018-07-24
 *         Template as provided by Adrian Gould
 *
 */
"use strict";

let area = 0;
let radius = "1.6";
const PI = 3.14159;

area = radius * PI;
alert(area);