﻿namespace BrendanKellyExercisesWeek01
{
    partial class Form2
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lblHero = new System.Windows.Forms.Label();
            this.lblVillain = new System.Windows.Forms.Label();
            this.txtHero = new System.Windows.Forms.TextBox();
            this.txtVillain = new System.Windows.Forms.TextBox();
            this.SuspendLayout();
            // 
            // lblHero
            // 
            this.lblHero.AutoSize = true;
            this.lblHero.Location = new System.Drawing.Point(34, 55);
            this.lblHero.Name = "lblHero";
            this.lblHero.Size = new System.Drawing.Size(30, 13);
            this.lblHero.TabIndex = 0;
            this.lblHero.Text = "Hero";
            // 
            // lblVillain
            // 
            this.lblVillain.AutoSize = true;
            this.lblVillain.Location = new System.Drawing.Point(37, 104);
            this.lblVillain.Name = "lblVillain";
            this.lblVillain.Size = new System.Drawing.Size(34, 13);
            this.lblVillain.TabIndex = 1;
            this.lblVillain.Text = "Villain";
            // 
            // txtHero
            // 
            this.txtHero.Location = new System.Drawing.Point(120, 47);
            this.txtHero.Name = "txtHero";
            this.txtHero.Size = new System.Drawing.Size(100, 20);
            this.txtHero.TabIndex = 2;
            this.txtHero.Text = "Batman";
            // 
            // txtVillain
            // 
            this.txtVillain.Location = new System.Drawing.Point(120, 96);
            this.txtVillain.Name = "txtVillain";
            this.txtVillain.Size = new System.Drawing.Size(100, 20);
            this.txtVillain.TabIndex = 3;
            this.txtVillain.Text = "Joker";
            // 
            // Form2
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(248, 166);
            this.Controls.Add(this.txtVillain);
            this.Controls.Add(this.txtHero);
            this.Controls.Add(this.lblVillain);
            this.Controls.Add(this.lblHero);
            this.Name = "Form2";
            this.Text = "Question2";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lblHero;
        private System.Windows.Forms.Label lblVillain;
        private System.Windows.Forms.TextBox txtHero;
        private System.Windows.Forms.TextBox txtVillain;
    }
}