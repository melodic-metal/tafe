import java.util.Scanner;

public class CarCareChoice2 {

   public static void main(String[] args) {
      
      double price = 0;
      boolean isValid = false;
      String serviceType = null;
      Scanner keyboard = new Scanner(System.in);
      System.out.println("Please enter a code to view prices of car servicing");
      System.out.println("Valid choices are: ");
      System.out.println("oil change, tyre rotation, battery check, or brake inspection");
      String userInput = keyboard.nextLine();
      
      
      
      
      switch(userInput) {
         case "oil":
            price = 25;
            isValid = true;
            serviceType = "Oil Change";
            break;
         case "tyr":
            price = 22;
            isValid = true;
            serviceType = "Tyre Rotation";
            break;
         case "bat":
            price = 15;
            isValid = true;
            serviceType = "Battery Check";
            break;
         case "bra":
            price = 5;
            isValid = true;
            serviceType = "brake inspection";
            break;
         default:
            isValid = false;
            break;   
            
         }
         
         if(isValid == false) {
            System.out.println("Invalid selection");   
         }
         else {
            System.out.println("The price for " + serviceType + " is $" + price);
         }

} 
}