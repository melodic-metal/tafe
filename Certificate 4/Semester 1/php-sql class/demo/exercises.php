<style>
    table {
        border-collapse: collapse;
        width: 100%;
    }

    th, td {
        text-align: left;
        padding: 8px;
    }

    tr:nth-child(even) {
        background-color: #f2f2f2
    }

    th {
        background-color: #4CAF50;
        color: white;
    }
</style>


<?php
/**
 * Created by PhpStorm.
 * User: kellyb
 * Date: 2/10/2018
 * Time: 3:01 PM
 */

Class Task
{
    function __construct($desc, $isCompleted = false, $priority = 1)
    {
        $this->desc = $desc;
        $this->isCompleted = $isCompleted;
        $this->priority = $priority;
    }


}

function displayPriorityDescs($priority)
{
    switch ($priority) {
        case 1:
            $prioDesc = "Very High";
            break;
        case 2:
            $prioDesc = "High";
            break;
        case 3:
            $prioDesc = "Medium";
            break;
        case 4:
            $prioDesc = "Low";
            break;
        case 5:
            $prioDesc = "Very Low";
            break;
        default:
            $prioDesc = "Not found";
            break;
    }
    return $prioDesc . " Priority";
}

$tasksArray = array(new Task("Dishes", true, 4), new Task("Wash Car", false, 3),
    new Task("Walk dog", true, 5), new Task("Do homework", true, 1), new Task("Make bed", false, 3),
    new Task("Pay Bills", true, 3), new Task("Mop floors"));

usort($tasksArray, "sortTasksByName");
echo "


<table>
<tr>
<th>Done?</th>
<th>Chore</th>
<th>Priority</th>
</tr>
";
foreach ($tasksArray as $task) {
    echo "<tr>";
    if ($task->isCompleted) {
        echo "
            <td><img src='Checkbox-Off@1x.png' /></td>

            ";
    } else {
        echo "<td><img src='Checkbox-On@1x.png' /></td>

            ";
    }
    echo "<td>$task->desc</td>
          <td";
    switch ($task->priority) {
        case 1: echo " bgcolor=\"#d31919\"";
                break;
        case 2: echo " bgcolor=\"#d26618\"";
                break;
        case 3: echo " bgcolor=\"#1013b5\"";
                break;
        case 4: echo " bgcolor=\"#1d9128\"";
                break;
        case 5: echo " bgcolor=\"#00c412\"";
                break;

    }


    echo ">" . displayPriorityDescs($task->priority) . "</td>";
    echo "</tr>";


}

echo "</table>";


function sortTasksByPriority($task1, $task2) {
    return strcmp($task2->priority, $task1->priority);
}

function sortTasksByCompletion($task1, $task2) {
    return strcmp($task2->isCompleted, $task1->isCompleted);
}

function sortTasksByName($task1, $task2) {
    return strcmp($task2->desc, $task1->desc);
}

function displayCertainTasks($priority) {

}


