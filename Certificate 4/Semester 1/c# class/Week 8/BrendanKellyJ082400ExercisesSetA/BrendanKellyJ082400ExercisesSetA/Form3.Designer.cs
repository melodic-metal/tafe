﻿namespace BrendanKellyJ082400ExercisesSetA
{
    partial class Form3
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lbl_name = new System.Windows.Forms.Label();
            this.lbl_age = new System.Windows.Forms.Label();
            this.txt_name = new System.Windows.Forms.TextBox();
            this.txt_age = new System.Windows.Forms.TextBox();
            this.btn_accept = new System.Windows.Forms.Button();
            this.btn_average = new System.Windows.Forms.Button();
            this.grp_input = new System.Windows.Forms.GroupBox();
            this.grp_outputs = new System.Windows.Forms.GroupBox();
            this.lbl_average = new System.Windows.Forms.Label();
            this.txt_average = new System.Windows.Forms.TextBox();
            this.txt_above_avg = new System.Windows.Forms.TextBox();
            this.lbl_above_average = new System.Windows.Forms.Label();
            this.grp_input.SuspendLayout();
            this.grp_outputs.SuspendLayout();
            this.SuspendLayout();
            // 
            // lbl_name
            // 
            this.lbl_name.AutoSize = true;
            this.lbl_name.Location = new System.Drawing.Point(42, 36);
            this.lbl_name.Name = "lbl_name";
            this.lbl_name.Size = new System.Drawing.Size(61, 13);
            this.lbl_name.TabIndex = 0;
            this.lbl_name.Text = "Enter name";
            // 
            // lbl_age
            // 
            this.lbl_age.AutoSize = true;
            this.lbl_age.Location = new System.Drawing.Point(42, 72);
            this.lbl_age.Name = "lbl_age";
            this.lbl_age.Size = new System.Drawing.Size(54, 13);
            this.lbl_age.TabIndex = 1;
            this.lbl_age.Text = "Enter Age";
            // 
            // txt_name
            // 
            this.txt_name.Location = new System.Drawing.Point(94, 21);
            this.txt_name.Name = "txt_name";
            this.txt_name.Size = new System.Drawing.Size(100, 20);
            this.txt_name.TabIndex = 2;
            // 
            // txt_age
            // 
            this.txt_age.Location = new System.Drawing.Point(94, 57);
            this.txt_age.Name = "txt_age";
            this.txt_age.Size = new System.Drawing.Size(100, 20);
            this.txt_age.TabIndex = 3;
            // 
            // btn_accept
            // 
            this.btn_accept.Location = new System.Drawing.Point(36, 226);
            this.btn_accept.Name = "btn_accept";
            this.btn_accept.Size = new System.Drawing.Size(75, 23);
            this.btn_accept.TabIndex = 4;
            this.btn_accept.Text = "Accept";
            this.btn_accept.UseVisualStyleBackColor = true;
            this.btn_accept.Click += new System.EventHandler(this.btn_accept_Click);
            // 
            // btn_average
            // 
            this.btn_average.Location = new System.Drawing.Point(138, 226);
            this.btn_average.Name = "btn_average";
            this.btn_average.Size = new System.Drawing.Size(75, 23);
            this.btn_average.TabIndex = 5;
            this.btn_average.Text = "Averages";
            this.btn_average.UseVisualStyleBackColor = true;
            this.btn_average.Click += new System.EventHandler(this.btn_average_Click);
            // 
            // grp_input
            // 
            this.grp_input.Controls.Add(this.txt_age);
            this.grp_input.Controls.Add(this.txt_name);
            this.grp_input.Location = new System.Drawing.Point(36, 12);
            this.grp_input.Name = "grp_input";
            this.grp_input.Size = new System.Drawing.Size(200, 100);
            this.grp_input.TabIndex = 6;
            this.grp_input.TabStop = false;
            this.grp_input.Text = "Inputs";
            // 
            // grp_outputs
            // 
            this.grp_outputs.Controls.Add(this.lbl_above_average);
            this.grp_outputs.Controls.Add(this.txt_above_avg);
            this.grp_outputs.Controls.Add(this.txt_average);
            this.grp_outputs.Controls.Add(this.lbl_average);
            this.grp_outputs.Location = new System.Drawing.Point(36, 120);
            this.grp_outputs.Name = "grp_outputs";
            this.grp_outputs.Size = new System.Drawing.Size(200, 100);
            this.grp_outputs.TabIndex = 7;
            this.grp_outputs.TabStop = false;
            this.grp_outputs.Text = "Outputs";
            // 
            // lbl_average
            // 
            this.lbl_average.AutoSize = true;
            this.lbl_average.Location = new System.Drawing.Point(6, 26);
            this.lbl_average.Name = "lbl_average";
            this.lbl_average.Size = new System.Drawing.Size(69, 13);
            this.lbl_average.TabIndex = 0;
            this.lbl_average.Text = "Average Age";
            // 
            // txt_average
            // 
            this.txt_average.Location = new System.Drawing.Point(94, 23);
            this.txt_average.Name = "txt_average";
            this.txt_average.ReadOnly = true;
            this.txt_average.Size = new System.Drawing.Size(100, 20);
            this.txt_average.TabIndex = 1;
            // 
            // txt_above_avg
            // 
            this.txt_above_avg.Location = new System.Drawing.Point(94, 60);
            this.txt_above_avg.Name = "txt_above_avg";
            this.txt_above_avg.ReadOnly = true;
            this.txt_above_avg.Size = new System.Drawing.Size(100, 20);
            this.txt_above_avg.TabIndex = 2;
            // 
            // lbl_above_average
            // 
            this.lbl_above_average.AutoSize = true;
            this.lbl_above_average.Location = new System.Drawing.Point(6, 63);
            this.lbl_above_average.Name = "lbl_above_average";
            this.lbl_above_average.Size = new System.Drawing.Size(78, 13);
            this.lbl_above_average.TabIndex = 3;
            this.lbl_above_average.Text = "No. above avg";
            // 
            // Form3
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(284, 261);
            this.Controls.Add(this.btn_average);
            this.Controls.Add(this.btn_accept);
            this.Controls.Add(this.lbl_age);
            this.Controls.Add(this.lbl_name);
            this.Controls.Add(this.grp_input);
            this.Controls.Add(this.grp_outputs);
            this.Name = "Form3";
            this.Text = "Form3";
            this.grp_input.ResumeLayout(false);
            this.grp_input.PerformLayout();
            this.grp_outputs.ResumeLayout(false);
            this.grp_outputs.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lbl_name;
        private System.Windows.Forms.Label lbl_age;
        private System.Windows.Forms.TextBox txt_name;
        private System.Windows.Forms.TextBox txt_age;
        private System.Windows.Forms.Button btn_accept;
        private System.Windows.Forms.Button btn_average;
        private System.Windows.Forms.GroupBox grp_input;
        private System.Windows.Forms.GroupBox grp_outputs;
        private System.Windows.Forms.Label lbl_above_average;
        private System.Windows.Forms.TextBox txt_above_avg;
        private System.Windows.Forms.TextBox txt_average;
        private System.Windows.Forms.Label lbl_average;
    }
}