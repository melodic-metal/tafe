import java.util.Scanner;

public class PurchaseArray {
 
    public static void main(String[] args)  {
        System.out.println("Creating invoices and setting sales amount");
        Scanner keyboard = new Scanner(System.in); // Set up scanner object
        final int ARRAY_SIZE = 6;
        Purchase purchaseArray[] = new Purchase[ARRAY_SIZE]; // instantiate an array of Purchase objects
        String input;
        int invoiceNumber = 0;
        double saleAmount = 0; // initialise all required variables
 
        for (int i = 0; i < ARRAY_SIZE; i++) {
            do {
                System.out.print("Please enter invoice number (between 1000 and 10000) for purchase " + (i + 1) + " >> ");
                invoiceNumber = keyboard.nextInt();
            }
            while (invoiceNumber < 1000 || invoiceNumber > 10000); // while invoiceNumber is between 1000 and 10000
 
            do {
                System.out.print("Please enter sales amount (non-negative) for purchase " + (i + 1) + " >> ");
                saleAmount = keyboard.nextDouble();
            }
            while (saleAmount < 0); // while saleAmount is not a negative number
 
            purchaseArray[i] = new Purchase(invoiceNumber, saleAmount);
            System.out.println();
        }
        // receive the information from the user
 
        System.out.println();
        System.out.println("Outputting information...");
        System.out.println();
        
 
        for (int j = 0; j < purchaseArray.length; j++) {
            purchaseArray[j].displayInformation();
            // output the information for the user
        }
 
    }
}