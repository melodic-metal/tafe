import java.util.Scanner;

public class CondoSales {

   public static void main(String[] args) {
   
      int condoPrice = 0, userInput = 0;
      String condoType = "";
      
      Scanner keyboard = new Scanner(System.in);
      System.out.println("Welcome to the condo sales system");
      System.out.println("Press 1 for a park view, 2 for golf course view, or 3 for a lake view");
      userInput = keyboard.nextInt();
      switch(userInput){
                  case 1:
                     condoPrice = 150000;
                     condoType = "Park";
                     break;
                  case 2:
                     condoPrice = 180000;
                     condoType = "Golf Course";
                     break;
                  case 3:
                     condoPrice = 210000;
                     condoType  = "Lake";
                     break;
                  default:
                     condoPrice = 0;
                     condoType = "Invalid";
                     break;
      }
      
      System.out.println("A condo of type \"" + condoType + "\" Will cost " + condoPrice);
   }

}