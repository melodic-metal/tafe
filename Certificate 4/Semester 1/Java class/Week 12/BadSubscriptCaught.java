import javax.swing.*;

public class BadSubscriptCaught {

   public static void main(String[] args) {
   
      String[] names = {"Jack", "John", "Jill", "Naomi", "Mark", "Emma", "Paul", "Maddie", "Rhys", "George" };
      String input;
      int userInput;
      
      try {
         input = JOptionPane.showInputDialog(null, "Input a number for a name");
         userInput = Integer.parseInt(input);
         JOptionPane.showMessageDialog(null, "That name is: " + names[userInput - 1]);
      }
      catch(ArrayIndexOutOfBoundsException e) {
         JOptionPane.showMessageDialog(null, "Not a valid selection");
      }
      
      
      
   
   }
   
}