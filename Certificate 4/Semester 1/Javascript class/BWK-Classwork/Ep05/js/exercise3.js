function convertFToC (temp) {
    let convertedTemp = 32 + temp * 9 / 5;
    return convertedTemp;
}

function writeToPage() {

    let pageBody = document.getElementById("pageBody");
    pageBody.innerHTML = "<p>Converted from C to F is: " +  convertFToC(-40) + "</p>";

}
