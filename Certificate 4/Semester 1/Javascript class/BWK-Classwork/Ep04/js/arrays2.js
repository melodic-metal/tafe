"use strict";
let fruit;

fruit = ["Banana", "Strawberry", "Lychee", "Mango",];

document.writeln("<p>Unpopped: " + fruit.toString() + "</p>");
fruit.pop();

document.writeln("<p>Popped:" + fruit.toString() + "</p>");

let result = fruit.push("Durian");
document.writeln("<p>Durian pushed: " + fruit.join(" ") + "</p>");
document.writeln("<p>Result:" + result + "</p>");

document.writeln("<br/>");

result = fruit.shift("apple1");
document.writeln("<p>apple shifted: " + fruit.join(" ") + "</p>");
document.writeln("<p>Result:" + result + "</p>");

document.writeln("<br/>");

result = fruit.shift("pear");
document.writeln("<p>pear shifted: " + fruit.join(" ") + "</p>");
document.writeln("<p>Result:" + result + "</p>");

document.writeln("<br/>");

result = fruit.unshift("apple2");
document.writeln("<p>apple unshifted: " + fruit.join(" ") + "</p>");
document.writeln("<p>Result:" + result + "</p>");

document.writeln("<br/>");

result = fruit.unshift("pear1");
document.writeln("<p>pear1 unshifted: " + fruit.join(" ") + "</p>");
document.writeln("<p>Result:" + result + "</p>");
//add new item to end of array
fruit[fruit.length] = "kiwi";

let cars = ["Ford Focus", "Aston Martin", "Holden Barina", "Bugatti Veyron"];

document.writeln("<p>" + cars.join(" ") + "</p>");
cars.splice(2, 0, "Hyundai Getz", "Honda Jazz");
document.write("<p>" + cars + "</p>");

cars.splice(4, 1, "Landrover", "Jeep Cherokee");
document.write("<p>" + cars + "</p>");

cars.splice(0, 2);
document.write("<p>" + cars + "</p>");

//merging arrays

let myGirls = ["Amy", "Bernadette", "Cheryl"];
let myBoys = ["Zac", "Vann", "Xavier"];
let myChildren = myGirls.concat(myBoys);
document.writeln("<p>" + myChildren.join(" ") + "</p>");

let moreChildren = ["Julian", "Kira", "Liam", "Mavis"];
let lotsOfChildren = myBoys.concat(myGirls, moreChildren);
document.writeln("<p>" + lotsOfChildren.join(" ") + "</p>");

//slicing arrays

fruit = ["Apple", "Banana", "Cherry", "Dragon Fruit", "Eggplant", "Fig"];
let slicedFruit = fruit.slice(1);
document.writeln("<p>" + slicedFruit + "</p>");
slicedFruit = fruit.slice(2, 4);
document.writeln("<p>" + slicedFruit + "</p>");
slicedFruit.reverse();
document.writeln("<p>" + slicedFruit + "</p>");


// numeric sort

let numbers = [8, 5, 1, 23, 45, 2, 3];
numbers.sort(
    function (a, b) {
        return a - b;
    }
)
document.writeln("<p>" + numbers + "</p>");

// shuffle an array

let cards = ["ace", 2, 3, 4, 5, 6, 7, 8, 9, "Jack", "Queen0", "King"];
document.writeln("<p>" + cards.sort(function (a, b) {
    return 0.5 - Math.random();
}) + "</p>");
