import java.util.Scanner;

public class CondoSales2 {

   public static void main(String[] args) {
   
      final int parkingPrice = 5000;
      int condoPrice = 0, userInputView = 0, userInputParking = 0;
      String condoType = "", parkingType;
      
      Scanner keyboard = new Scanner(System.in);
      System.out.println("Welcome to the condo sales system");
      System.out.println("Press 1 for a park view, 2 for golf course view, or 3 for a lake view");
      userInputView = keyboard.nextInt();

      switch(userInputView){
            case 1:
               condoPrice = 150000;
               condoType = "Park";
               break;
            case 2:
               condoPrice = 180000;
               condoType = "Golf Course";
               break;
            case 3:
               condoPrice = 210000;
               condoType  = "Lake";
               break;
            default:
               condoPrice = 0;
               condoType = "Invalid";
               break;
      }
      
      System.out.println("Please press 1 for a parking garage, or 2 for a parking spot");
      userInputParking = keyboard.nextInt();
            
      if (userInputParking == 1) {
            condoPrice += parkingPrice;
      }
      else if (userInputParking == 2) { }
     
      
      System.out.println("A condo of type \"" + condoType + "\" Will cost " + condoPrice);
   }

}