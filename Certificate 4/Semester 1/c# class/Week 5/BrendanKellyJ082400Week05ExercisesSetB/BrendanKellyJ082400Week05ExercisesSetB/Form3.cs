﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace BrendanKellyJ082400Week05ExercisesSetB
{
    public partial class Form3 : Form
    {
        public Form3()
        {
            InitializeComponent();
        }

        private void btn_calc_Click(object sender, EventArgs e)
        {
            lst_table.Items.Add("Number\t\tSquared");

            for (double i = 0.5; i <= 5; i = i + 0.5)
            {
                lst_table.Items.Add(i + "\t\t" + Math.Pow(i, 2));
            }

        }
    }
}
