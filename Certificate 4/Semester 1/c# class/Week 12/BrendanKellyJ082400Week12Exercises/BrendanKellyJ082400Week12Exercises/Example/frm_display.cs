﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace BrendanKellyJ082400Week12Exercises.Example
{
    public partial class frm_display : Form
    {
        public int int_age;
       
        public frm_display()
        {
            InitializeComponent();
            txt_family.ReadOnly = true;
        }

        private void btn_display_Click(object sender, EventArgs e)
        {
            txt_age.Text = int_age.ToString();
            txt_given.Text = frm_main.str_given_name;
            
        }

        private void btn_accept_Click(object sender, EventArgs e)
        {
            int int_new_age;
            if (!int.TryParse(txt_age.Text, out int_new_age))
            {
                MessageBox.Show("The entered age is not a number - Enter it again");
                txt_age.Clear();
                txt_age.Focus();
            }
            else
            {
                //  Change the this forms public variable value
                int_age = int_new_age;
                // change the frm_main's public static variable
                frm_main.str_given_name = txt_given.Text;
            }
        }

        private void btn_close_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
