/**
 *
 *
 * Calculations in JS
 *
 *
 * @File:       /Ep02/js/ex-08.js
 * @Project:    Classwork
 * @Author:     Brendan Kelly <j082400@tafe.wa.edu.au>
 * @Version:    1.0
 * @Copyright:  Brendan Kelly 2018
 *
 * History:
 * v1.0    2018-07-24
 *         Calculations and stuff
 *
 * v0.1    2018-07-24
 *         Template as provided by Adrian Gould
 *
 */
"use strict";
let number = 734;
let hours = 0;
let minutes = 0;
hours = number / 60;
minutes  = number % 60;
console.log("That is " + hours.toFixed() + " hours and " + minutes + " minutes");
