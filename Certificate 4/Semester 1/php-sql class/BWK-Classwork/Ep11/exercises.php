
<!doctype html>
<html lang="en">
<!--
ONE LINE TITLE FOR FILE

LONGER EXPLANATION OF THE FILE AND ITS PURPOSE
MAY BE MORE THAN ONE LINE IN LENGTH

@File:       /Template/index.html
@Project:    Classwork
@Author:     Brendan Kelly <082400@tafe.wa.edu.au>
@Version:    1.0
@Copyright:  YOUR NAME 2018

History:
v1.0    2018-07-24
        did stuff

v0.1    2018-07-24
        Template as provided by Adrian Gould
-->
<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <title>BWK - Classwork</title>

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="../assets/css/bootstrap.css">
    <!-- FontAwesome CSS -->
    <link rel="stylesheet" href="../assets/css/fontawesome-all.css">
    <!-- core first, then extra styles -->
    <link rel="stylesheet" href="../assets/css/app.css">

</head>
<body>


<nav class="navbar navbar-expand-lg navbar-dark bg-dark fixed-top">

    <a href="../" class="navbar-brand">BWK</a>

    <button class="navbar-toggler" type="button"
            data-toggle="collapse"
            data-target="#navbarContent"
            aria-controls="#navbarContent"
            aria-expanded="false" aria-label="toggle">
        <span class="navbar-toggler-icon"></span>
    </button>

    <div class="collapse navbar-collapse" id="navbarContent">
        <ul class="navbar-nav mr-auto">
            <li class="nav-item ">
                <a href="../index.php" class="nav-link">Home</a>
            </li>
            <li class="nav-item ">
                <a href="../Ep11/" class="nav-link">Ep11</a>
            </li>
            <li class="nav-item">
                <a href="../Ep12/" class="nav-link">Ep12</a>
            </li>
            <li class="nav-item">
                <a href="../Ep13/" class="nav-link">Ep13</a>
            </li>
            <li class="nav-item ">
                <a href="../Ep14/" class="nav-link">Ep14</a>
            </li>
            <li class="nav-item ">
                <a href="../Ep15/" class="nav-link">Ep15</a>
            </li>
            <li class="nav-item ">
                <a href="../Ep16/" class="nav-link">Ep16</a>
            </li>
            <li class="nav-item ">
                <a href="../Ep17/" class="nav-link">Ep17</a>
            </li>
            <li class="nav-item ">
                <a href="../Ep18/" class="nav-link">Ep18</a>
            </li>
            <li class="nav-item ">
                <a href="../Ep19/" class="nav-link">Ep19</a>
            </li>
        </ul>
    </div>
</nav>

<div class="container">

    <!-- Start Content goes here -->

<style>
    table {
        border-collapse: collapse;
        width: 100%;
    }

    th, td {
        text-align: left;
        padding: 8px;
    }

    tr:nth-child(even) {
        background-color: #f2f2f2
    }

    th {
        background-color: #4CAF50;
        color: white;
    }
</style>


<?php
/**
 * Created by PhpStorm.
 * User: kellyb
 * Date: 2/10/2018
 * Time: 3:01 PM
 */

Class Task
{
    function __construct($desc, $isCompleted = false, $priority = 1)
    {
        $this->desc = $desc;
        $this->isCompleted = $isCompleted;
        $this->priority = $priority;
    }


}

function displayPriorityDescs($priority)
{
    switch ($priority) {
        case 1:
            $prioDesc = "Very High";
            break;
        case 2:
            $prioDesc = "High";
            break;
        case 3:
            $prioDesc = "Medium";
            break;
        case 4:
            $prioDesc = "Low";
            break;
        case 5:
            $prioDesc = "Very Low";
            break;
        default:
            $prioDesc = "Not found";
            break;
    }
    return $prioDesc . " Priority";
}

$tasksArray = array(new Task("Dishes", true, 4), new Task("Wash Car", false, 3),
    new Task("Walk dog", true, 5), new Task("Do homework", true, 1), new Task("Make bed", false, 3),
    new Task("Pay Bills", true, 3), new Task("Mop floors"));

usort($tasksArray, "sortTasksByName");
echo "


<table>
<tr>
<th>Done?</th>
<th>Chore</th>
<th>Priority</th>
</tr>
";
foreach ($tasksArray as $task) {
    echo "<tr>";
    if ($task->isCompleted) {
        echo "
            <td><img src='Checkbox-Off@1x.png' /></td>

            ";
    } else {
        echo "<td><img src='Checkbox-On@1x.png' /></td>

            ";
    }
    echo "<td>$task->desc</td>
          <td";
    switch ($task->priority) {
        case 1: echo " bgcolor=\"#d31919\"";
                break;
        case 2: echo " bgcolor=\"#d26618\"";
                break;
        case 3: echo " bgcolor=\"#1013b5\"";
                break;
        case 4: echo " bgcolor=\"#1d9128\"";
                break;
        case 5: echo " bgcolor=\"#00c412\"";
                break;

    }


    echo ">" . displayPriorityDescs($task->priority) . "</td>";
    echo "</tr>";


}

echo "</table>";


function sortTasksByPriority($task1, $task2) {
    return strcmp($task2->priority, $task1->priority);
}

function sortTasksByCompletion($task1, $task2) {
    return strcmp($task2->isCompleted, $task1->isCompleted);
}

function sortTasksByName($task1, $task2) {
    return strcmp($task2->desc, $task1->desc);
}

function displayCertainTasks($priority) {

}


?>
    <!-- End Content Goes here-->
</div><!-- end container -->

<footer class="footer bg-dark text-light">
    <div class="container">
        <div class="row">
            <div class="col-4 small">
                <ul class="list-unstyled">
                    <li>&copy; Copyright 2018 Brendan Kelly</li>
                    <li><a href="#">Contact Us</a></li>
                    <li><a href="../template/index.html">Template</a></li>
                </ul>
            </div>

            <div class="col-4 small">
                <ul class="list-unstyled">
                    <li><a href="#">Site map</a></li>
                    <li><a href="#">Privacy policy</a></li>
                    <li><a href="#">Terms and Conditions</a></li>
                </ul>
            </div>

            <div class="col-4 small">
                <ul class="list-inline">
                    <li class="list-inline-item">
                        <a href="http://twitter.com">
                            <i class="fab fa-twitter"></i>
                            Twitter
                        </a>
                    </li>
                    <li class="list-inline-item">
                        <a href="http://facebook.com">
                            <i class="fab fa-facebook"></i>
                            Facebook
                        </a>
                    </li>
                    <li class="list-inline-item">
                        <a href="http://linkedin.com">
                            <i class="fab fa-linkedin"></i>
                            Linked in
                        </a>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</footer>


<script src="../assets/js/jquery-3.3.1.js"></script>
<script src="../assets/js/bootstrap.min.js"></script>
<script src="../assets/js/fontawesome-all.js"></script>

<!-- include page specific JS files -->
<script src="JS-FILENAME-GOES-HERE.JS"></script>
<!-- end of page specific JS files -->
</body>
</html>

