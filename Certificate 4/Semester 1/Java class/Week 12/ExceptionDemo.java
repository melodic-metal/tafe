import javax.swing.*;

public class ExceptionDemo {

   public static void main(String[] args) {
   
      int numerator = 0, denominator = 0, result;
      String inputString;
      
      try {
         inputString = JOptionPane.showInputDialog(null, "Enter number to be divided");
         numerator = Integer.parseInt(inputString);
         inputString = JOptionPane.showInputDialog(null, "enter a number to divide the first number");
         denominator = Integer.parseInt(inputString);
         result = numerator / denominator;
     }
     
     catch(ArithmeticException e) {
         JOptionPane.showMessageDialog(null, e.getMessage());
         result = 0;
     }
     JOptionPane.showMessageDialog(null, numerator + " / " + denominator  + "\nResult is " + result);
   }
}