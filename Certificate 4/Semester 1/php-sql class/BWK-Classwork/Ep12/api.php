<?php
/**
 * Created by PhpStorm.
 * User: kellyb
 * Date: 9/10/2018
 * Time: 2:02 PM
 */

class SoftDrink implements \JsonSerializable {

    public function __construct($name, $caffeine, $sugar) {
        $this->name = $name;
        $this->caffeine = $caffeine;
        $this->sugar = $sugar;
    }

    public function jsonSerialize()
    {
        // TODO: Implement jsonSerialize() method.
        return get_object_vars($this);
    }
}

$coke = new SoftDrink("coke", 10, 20);
$sprite = new SoftDrink("Sprite", 30, 40);

$softDrinks = array($coke, $sprite);


$chosenDrink = $_GET["drink"];



foreach ($softDrinks as $drink) {
    if (strcmp($chosenDrink, $drink->name) == 0) {
        echo json_encode($drink);
    }
}

