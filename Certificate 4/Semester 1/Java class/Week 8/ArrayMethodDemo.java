public class ArrayMethodDemo {

   static int[] numbers = { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10 };
   
   public static void main(String[] args) {
   
      displayAllIntegers();
      System.out.println();
      displayAllIntegersReversed();
      System.out.println();
      displaySum();
      System.out.println();
      displayLessThanArg(9);
      System.out.println();
      displayHigherThanAvg();
   
   }
   
   
   public static void displayAllIntegers() {
   
      for(int i = 0; i < numbers.length; i++) {
         System.out.print(numbers[i] + " ");
      }   
   }
   
   public static void displayAllIntegersReversed() {
      for (int i = numbers.length - 1; i >= 0; --i) {
         System.out.print(numbers[i] + " ");
      }
   }   
   public static void displaySum() {
     int total = 0;
     for (int i = 0; i < numbers.length -1; i++) {
         total += numbers[i];
      }
      System.out.print("Total is: " + total);     
   }   
   public static void displayLessThanArg(int number) {
      for (int i = 0; i < numbers.length -1; i++) {
         if(numbers[i] < number) {
            System.out.print(numbers[i] + " "); 
          }  
      }
      
   }
   public static void displayHigherThanAvg() {
      double average;
      int total = 0;
      for (int i = 0; i < numbers.length -1; i++) {
          total += numbers[i];
      }
      System.out.print("Numbers higher than average are: ");
      average = total / numbers.length;
      for (int i = 0; i < numbers.length; i++) {
         if (numbers[i] > average) {
            System.out.print( numbers[i] + " ");
      }   
        
   }

}
}