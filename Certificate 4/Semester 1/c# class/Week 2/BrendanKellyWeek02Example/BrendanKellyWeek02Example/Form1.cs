﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace BrendanKellyWeek02Example
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
     
        }

        private void txt_name_TextChanged(object sender, EventArgs e)
        {
            txt_GName.Text = txt_name.Text;
        }

        private void btnClick_Click(object sender, EventArgs e)
        {
            txt_name.Text = "12";
            txt_GName.BackColor = Color.Chartreuse;
            txt_name.Font = new Font("Arial", 12, FontStyle.Strikeout);
         
        }
    }
}
