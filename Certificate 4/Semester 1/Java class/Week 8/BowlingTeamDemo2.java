import java.util.*;

public class BowlingTeamDemo2 {

   public static void main(String[] args) {
   
      String name;
      final int NUM_TEAMS = 4;
      BowlingTeam[] teams = new BowlingTeam[NUM_TEAMS];
      BowlingTeam bowlTeam = new BowlingTeam();
      int x;
      final int NUM_TEAM_MEMBERS = 4;
      Scanner keyboard = new Scanner(System.in);
      for (int y = 0; y < NUM_TEAMS; y++) {
         teams[y] = new BowlingTeam();
         System.out.println("Set Team Name >> ");
         name = keyboard.nextLine();
         teams[y].setTeamName(name);
         for (x = 0; x < NUM_TEAM_MEMBERS; x++) { 
            System.out.println("Enter Team Member's Name >> ");
            name = keyboard.nextLine();
            bowlTeam.setMember(x, name);
            teams[y].setMember(x, name);
         }
      }
      
      for(int y = 0; y < NUM_TEAMS; y++) {
         System.out.println("\nMembers of team " +  teams[y].getTeamName());
      
         for(x = 0; x < NUM_TEAM_MEMBERS; x++) {
            System.out.print(teams[y].getMember(x) + " ");
            System.out.println();
         }
      }
      System.out.println();
   }
}