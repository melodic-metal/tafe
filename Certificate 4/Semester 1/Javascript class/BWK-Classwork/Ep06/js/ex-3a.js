"use strict";

document.getElementById("plus").addEventListener(
    "click", function (event) {

        calculate("plus");
    }
);

document.getElementById("subtract").addEventListener(
    "click", function (event) {

        calculate("subtract");
    }
);
document.getElementById("multiply").addEventListener(
    "click", function (event) {

        calculate("multiply");
    }
);

document.getElementById("divide").addEventListener(
    "click", function (event) {

        calculate("divide");
    }
);

stopDefault("plus");
stopDefault("subtract");
stopDefault("divide");
stopDefault("multiply");


function calculate(operator) {
    let num1 = parseInt(getValue("number1"));
    let num2 = parseInt(getValue("number2"));

    if (operator == "plus"){
        let result = num1 + num2;

        displayResult(result);
    }

    else if (operator == "divide"){
        let start = num1 / num2;
        let remainder = num1 % num2;
        let result = start + " r " + remainder;
        displayResult(result);
    }

    else if (operator == "multiply"){
        let result = num1 * num2;
        displayResult(result);
    }

    else if (operator == "subtract"){
        let result = num1 - num2;
        displayResult(result);
    }
}



function getValue(elementID) {

    let theControl = document.getElementById(elementID);
    return theControl.value;

}


function stopDefault(elementID) {
    document.getElementById(elementID).addEventListener("click", function (event) {
            event.preventDefault()
        }
    );
}

function displayResult(result){
    let resultsArea = document.getElementById("resultsArea");
    resultsArea.innerHTML = "<p>" + result.toString() + "</p>";
}





