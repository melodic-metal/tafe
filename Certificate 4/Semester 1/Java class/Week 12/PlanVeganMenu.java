import javax.swing.*;

public class PlanVeganMenu {

   public static void main(String[] args) {
   
      VeganMenu briefMenu = new VeganMenu();
      PickMenu entree = null;
      String guestChoice = new String();
      
      try {
            PickMenu selection = new PickMenu(briefMenu);
            entree = selection;
            guestChoice = entree.getGuestChoice();
      }
      catch(Exception error) {
         guestChoice = "an invalid vegan selection";
      }
      
      JOptionPane.showMessageDialog(null, "You chose " + guestChoice);
   }

}