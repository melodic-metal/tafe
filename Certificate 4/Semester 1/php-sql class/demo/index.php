<?php
/**
 * Created by PhpStorm.
 * User: kellyb
 * Date: 18/09/2018
 * Time: 2:03 PM
 */

// one line comment
/*
multi line comment
*/

/** php doc comments
 *
 */

echo "<h1>Hello world!</h1>";
print "<h2>Hello world 2!</h2>";

$name = "BWK";

define("WELCOME_MESSAGE", "Hello World");
echo "<p>This site belongs to {$name} </p>";
echo '<p>This site belongs to {$name} </p>';

echo '<p>'.WELCOME_MESSAGE.'</p>';

?>

<hr>
<h3>Hello <?=$name;?></h3>

<?php
 oddOrEven();
function oddOrEven($num = 22) {

    if ($num % 2 == 0) {
        echo "even";
    }
    else {
        echo "odd";
    }
}

class Car {
    public function __construct($make, $model, $numberOfWheels = 4) {
        $this->make = $make;
        $this->model = $model;
        $this->numberOfWheels = $numberOfWheels;


    }
    public function AWheelFellOff() {
            if ($this->numberOfWheels > 0) {
                $this->numberOfWheels--;
            }
    }
}
$mycar = new Car("Mitsubishi", "Pajero");
$mycar->AWheelFellOff();
echo $mycar->numberOfWheels;

$robertsCar= new Car("Honda", "Civic");

$cars = array($mycar, $robertsCar, new Car("Bleh", "Blehness",2));

foreach ($cars as $car) {
    echo $car->make . " " . $car->model . "<br>";
}

function compareCars($car1, $car2) {
    return strcmp($car1->make, $car2->make);
}

usort($cars, "compareCars");

foreach ($cars as $car) {
    echo $car->make . " " . $car->model . "<br>";
}