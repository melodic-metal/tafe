﻿// Date: 13/09/2018
// Programmer: Brendan Kelly
// Purpose: Display age in years from DOB

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace BrendanKellyWeek9ExerciseSetA
{
    public partial class Form12 : Form
    {
        public Form12()
        {
            InitializeComponent();
            txt_age.ReadOnly = true;
        }
        
        private void btn_calc_Click(object sender, EventArgs e)
        {
            DateTime dte_today = DateTime.Now;
            DateTime dte_dob = dte_date_of_birth.Value;
            int int_age;

            if(dte_dob > dte_today)
            {
                MessageBox.Show("Date of Birth cannot be after today's date");
            }

            else
            {
             int_age = dte_today.Year- dte_dob.Year;
                txt_age.Text = int_age.ToString();
            }
        }
    }
}
