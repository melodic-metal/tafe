﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace BrendanKellyJ082400ExerciseSetA
{
    public partial class Form2 : Form
    {
        String[] str_doggos = { "Corgi", "Boxer", "Husky", "Beagle", "Collie"};
        public Form2()
        {
            InitializeComponent();
        }

        private void btn_dogs_Click(object sender, EventArgs e)
        {
            lst_doggos.Visible = true;
            for (int i = 0; i < str_doggos.Length; i++)
            {
                lst_doggos.Items.Add(i+1 + "\t" + str_doggos[i]);
            }
        }
    }
}
