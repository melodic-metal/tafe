let outputArea = document.getElementById("output");


let rainfall = {
    "2018-02-18": 10.3,
    "2018-02-19": 16.8,
    "2018-02-20": 24.8,
    "2018-02-21": 629,
    "2018-02-22": 23.9,
    "2018-02-23": 3.6,
    "2018-02-24": 1.1,
    "2018-02-25": 1.2,
    "2018-02-26": 7,
    "2018-02-27": 3,
    "2018-02-28": 4,
    "2018-03-01": 65,

};

function getTotalRainfall() {
    let total = 0;
    for (let key in rainfall) {
        total += rainfall[key];
    }
    outputArea.innerHTML = "<p> Total is: " + total + "</p>";
}

function getMeanRainfall() {
    let mean = 0, total = 0;
    for (let key in rainfall) {
        total += rainfall[key];
        mean = total / getDays();
    }
    outputArea.innerHTML += "<p> mean is: " + mean + "</p>";

}
function getMinRainfall() {
    let min = Infinity;
    for (let key in rainfall) {
        if (rainfall[key] < min ) {
            min = rainfall[key];
        }

    }
    outputArea.innerHTML += "<p> min is: " + min + "</p>";

}

function getMaxRainfall() {
    let max = -Infinity;
    for (let key in rainfall) {
        if (rainfall[key] > max) {
            max = rainfall[key];
        }
    }
    outputArea.innerHTML += "<p> max is: " + max + "</p>";
}
//outputArea += "";
function getDaysWithRain() {
    let output = "";
    outputArea.innerHTML += "<table class='table'><tr><th>Date</th><th>Rainfall</th></tr>";
    for (let key in rainfall) {
        if (rainfall[key] > 0 ){
            output += "<tr><td> " + key + "</td><td> " + rainfall[key] + "</td></tr><br/>";
        }
    }
    outputArea.innerHTML += output + "</table>";
}

function getDays() {
    return Object.keys(rainfall).length;
}



getTotalRainfall();
getMeanRainfall();
getMinRainfall();
getMaxRainfall();
outputArea.innerHTML += "<p> number of days is: " + getDays() + "</p>";
getDaysWithRain();


