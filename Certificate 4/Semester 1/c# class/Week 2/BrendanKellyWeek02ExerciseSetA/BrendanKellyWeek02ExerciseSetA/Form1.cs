﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace BrendanKellyWeek02ExerciseSetA
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void btnChange_Click(object sender, EventArgs e)
        {
            txtFirstName.Text = "Enter Your Name Here";
            txtFirstName.Font = new Font("Times New Roman", 16, FontStyle.Italic);
            txtFirstName.ForeColor = Color.Blue;
            txtFirstName.BackColor = Color.Yellow;
            Form1.ActiveForm.Text = "Name Request";


        }

        private void btnShow_Click(object sender, EventArgs e)
        {
            txtFirstName.Visible = true;
        }

        private void btnHide_Click(object sender, EventArgs e)
        {
            txtFirstName.Visible = false;
        }

        private void numericUpDown1_ValueChanged(object sender, EventArgs e)
        {

        }
    }
}
