﻿// Programmer : Brendan Kelly 
// Date : August 2018
// Purpose : Application works out total cost of bill for a billing period

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace BrendanKellyJ082400Week03ExerciseSetC
{
    public partial class Form3 : Form
    {
        public Form3()
        {
            InitializeComponent();
        }

        private void btnCalculate_Click(object sender, EventArgs e)
        {
            double dblBillingPeriod = double.Parse(txtBillingPeriod.Text);
            double dblNumberOfCalls = double.Parse(txtNumberOfCalls.Text) * .15;
            double dblConnFee = 33 * dblBillingPeriod;
            double dblTotal = dblNumberOfCalls + dblConnFee;

            txtTotal.Text = dblTotal.ToString("C");
            
        }
    }
}
