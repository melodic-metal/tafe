﻿namespace BrendanKellyJ082400ExercisesSetA
{
    partial class Form5
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btn_price = new System.Windows.Forms.Button();
            this.txt_price = new System.Windows.Forms.TextBox();
            this.txt_name = new System.Windows.Forms.TextBox();
            this.btn_accept = new System.Windows.Forms.Button();
            this.lst_output = new System.Windows.Forms.ListBox();
            this.lbl_price = new System.Windows.Forms.Label();
            this.lbl_product = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // btn_price
            // 
            this.btn_price.Location = new System.Drawing.Point(162, 214);
            this.btn_price.Name = "btn_price";
            this.btn_price.Size = new System.Drawing.Size(75, 23);
            this.btn_price.TabIndex = 13;
            this.btn_price.Text = "Over $10";
            this.btn_price.UseVisualStyleBackColor = true;
            this.btn_price.Click += new System.EventHandler(this.btn_price_Click_1);
            // 
            // txt_price
            // 
            this.txt_price.Location = new System.Drawing.Point(128, 47);
            this.txt_price.Name = "txt_price";
            this.txt_price.Size = new System.Drawing.Size(100, 20);
            this.txt_price.TabIndex = 12;
            // 
            // txt_name
            // 
            this.txt_name.Location = new System.Drawing.Point(128, 15);
            this.txt_name.Name = "txt_name";
            this.txt_name.Size = new System.Drawing.Size(100, 20);
            this.txt_name.TabIndex = 11;
            // 
            // btn_accept
            // 
            this.btn_accept.Location = new System.Drawing.Point(63, 205);
            this.btn_accept.Name = "btn_accept";
            this.btn_accept.Size = new System.Drawing.Size(75, 41);
            this.btn_accept.TabIndex = 10;
            this.btn_accept.Text = "Accept Product";
            this.btn_accept.UseVisualStyleBackColor = true;
            this.btn_accept.Click += new System.EventHandler(this.btn_accept_Click_1);
            // 
            // lst_output
            // 
            this.lst_output.FormattingEnabled = true;
            this.lst_output.Location = new System.Drawing.Point(100, 93);
            this.lst_output.Name = "lst_output";
            this.lst_output.Size = new System.Drawing.Size(120, 95);
            this.lst_output.TabIndex = 9;
            // 
            // lbl_price
            // 
            this.lbl_price.AutoSize = true;
            this.lbl_price.Location = new System.Drawing.Point(91, 47);
            this.lbl_price.Name = "lbl_price";
            this.lbl_price.Size = new System.Drawing.Size(31, 13);
            this.lbl_price.TabIndex = 8;
            this.lbl_price.Text = "Price";
            // 
            // lbl_product
            // 
            this.lbl_product.AutoSize = true;
            this.lbl_product.Location = new System.Drawing.Point(47, 18);
            this.lbl_product.Name = "lbl_product";
            this.lbl_product.Size = new System.Drawing.Size(75, 13);
            this.lbl_product.TabIndex = 7;
            this.lbl_product.Text = "Product Name";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(284, 261);
            this.Controls.Add(this.btn_price);
            this.Controls.Add(this.txt_price);
            this.Controls.Add(this.txt_name);
            this.Controls.Add(this.btn_accept);
            this.Controls.Add(this.lst_output);
            this.Controls.Add(this.lbl_price);
            this.Controls.Add(this.lbl_product);
            this.Name = "Form1";
            this.Text = "Form1";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btn_price;
        private System.Windows.Forms.TextBox txt_price;
        private System.Windows.Forms.TextBox txt_name;
        private System.Windows.Forms.Button btn_accept;
        private System.Windows.Forms.ListBox lst_output;
        private System.Windows.Forms.Label lbl_price;
        private System.Windows.Forms.Label lbl_product;
    }
}

