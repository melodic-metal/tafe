﻿namespace BrendanKellyJ082400Assessment2
{
    partial class sales_summary_form
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lbl_total_num = new System.Windows.Forms.Label();
            this.lbl_total_val = new System.Windows.Forms.Label();
            this.lst_summary = new System.Windows.Forms.ListBox();
            this.txt_total_num = new System.Windows.Forms.TextBox();
            this.txt_total_val = new System.Windows.Forms.TextBox();
            this.SuspendLayout();
            // 
            // lbl_total_num
            // 
            this.lbl_total_num.AutoSize = true;
            this.lbl_total_num.Location = new System.Drawing.Point(12, 41);
            this.lbl_total_num.Name = "lbl_total_num";
            this.lbl_total_num.Size = new System.Drawing.Size(108, 13);
            this.lbl_total_num.TabIndex = 0;
            this.lbl_total_num.Text = "Total number of sales";
            // 
            // lbl_total_val
            // 
            this.lbl_total_val.AutoSize = true;
            this.lbl_total_val.Location = new System.Drawing.Point(60, 88);
            this.lbl_total_val.Name = "lbl_total_val";
            this.lbl_total_val.Size = new System.Drawing.Size(60, 13);
            this.lbl_total_val.TabIndex = 1;
            this.lbl_total_val.Text = "Total value";
            // 
            // lst_summary
            // 
            this.lst_summary.FormattingEnabled = true;
            this.lst_summary.Location = new System.Drawing.Point(37, 145);
            this.lst_summary.Name = "lst_summary";
            this.lst_summary.Size = new System.Drawing.Size(219, 95);
            this.lst_summary.TabIndex = 2;
            // 
            // txt_total_num
            // 
            this.txt_total_num.Location = new System.Drawing.Point(132, 38);
            this.txt_total_num.Name = "txt_total_num";
            this.txt_total_num.Size = new System.Drawing.Size(100, 20);
            this.txt_total_num.TabIndex = 3;
            // 
            // txt_total_val
            // 
            this.txt_total_val.Location = new System.Drawing.Point(132, 81);
            this.txt_total_val.Name = "txt_total_val";
            this.txt_total_val.Size = new System.Drawing.Size(100, 20);
            this.txt_total_val.TabIndex = 4;
            // 
            // sales_summary_form
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(284, 261);
            this.Controls.Add(this.txt_total_val);
            this.Controls.Add(this.txt_total_num);
            this.Controls.Add(this.lst_summary);
            this.Controls.Add(this.lbl_total_val);
            this.Controls.Add(this.lbl_total_num);
            this.Name = "sales_summary_form";
            this.Text = "sales_summary_form";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lbl_total_num;
        private System.Windows.Forms.Label lbl_total_val;
        private System.Windows.Forms.ListBox lst_summary;
        private System.Windows.Forms.TextBox txt_total_num;
        private System.Windows.Forms.TextBox txt_total_val;
    }
}