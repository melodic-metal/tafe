import java.util.Scanner;

public class CheckIfEven {

   public static void main(String[] args) {
      
      Scanner keyboard = new Scanner(System.in);
      System.out.println("Please enter a number >> ");
      int userInput = keyboard.nextInt();
         
         
      if (userInput % 2 == 1) {
      
         System.out.println("Number is odd");
      }
      else if (userInput % 2 == 0) {
      
         System.out.println("Number is even");
      } 
      
     
   }

}