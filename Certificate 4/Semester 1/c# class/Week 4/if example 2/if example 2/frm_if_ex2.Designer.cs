﻿namespace if_example_2
{
    partial class frm_if_ex2
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.txt_amount_to_pay = new System.Windows.Forms.TextBox();
            this.txt_discount = new System.Windows.Forms.TextBox();
            this.txt_amount_spent = new System.Windows.Forms.TextBox();
            this.btn_calc_discount = new System.Windows.Forms.Button();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // txt_amount_to_pay
            // 
            this.txt_amount_to_pay.Location = new System.Drawing.Point(148, 209);
            this.txt_amount_to_pay.Name = "txt_amount_to_pay";
            this.txt_amount_to_pay.Size = new System.Drawing.Size(100, 20);
            this.txt_amount_to_pay.TabIndex = 13;
            // 
            // txt_discount
            // 
            this.txt_discount.Location = new System.Drawing.Point(148, 152);
            this.txt_discount.Name = "txt_discount";
            this.txt_discount.Size = new System.Drawing.Size(100, 20);
            this.txt_discount.TabIndex = 12;
            // 
            // txt_amount_spent
            // 
            this.txt_amount_spent.Location = new System.Drawing.Point(148, 32);
            this.txt_amount_spent.Name = "txt_amount_spent";
            this.txt_amount_spent.Size = new System.Drawing.Size(100, 20);
            this.txt_amount_spent.TabIndex = 11;
            // 
            // btn_calc_discount
            // 
            this.btn_calc_discount.Location = new System.Drawing.Point(97, 94);
            this.btn_calc_discount.Name = "btn_calc_discount";
            this.btn_calc_discount.Size = new System.Drawing.Size(122, 23);
            this.btn_calc_discount.TabIndex = 10;
            this.btn_calc_discount.Text = "Calculate Discount";
            this.btn_calc_discount.UseVisualStyleBackColor = true;
            this.btn_calc_discount.Click += new System.EventHandler(this.btn_calc_discount_Click);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(51, 212);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(76, 13);
            this.label3.TabIndex = 9;
            this.label3.Text = "Amount to Pay";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(51, 155);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(49, 13);
            this.label2.TabIndex = 8;
            this.label2.Text = "Discount";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(36, 35);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(74, 13);
            this.label1.TabIndex = 7;
            this.label1.Text = "Amount Spent";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(284, 261);
            this.Controls.Add(this.txt_amount_to_pay);
            this.Controls.Add(this.txt_discount);
            this.Controls.Add(this.txt_amount_spent);
            this.Controls.Add(this.btn_calc_discount);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Name = "Form1";
            this.Text = "Discount calculator";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox txt_amount_to_pay;
        private System.Windows.Forms.TextBox txt_discount;
        private System.Windows.Forms.TextBox txt_amount_spent;
        private System.Windows.Forms.Button btn_calc_discount;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
    }
}

