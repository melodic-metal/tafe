﻿namespace Multi_Form_Example
{
    partial class frm_main
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.txt_age = new System.Windows.Forms.TextBox();
            this.Label2 = new System.Windows.Forms.Label();
            this.Label1 = new System.Windows.Forms.Label();
            this.btn_Display_form = new System.Windows.Forms.Button();
            this.txt_family_name = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.txt_given_name = new System.Windows.Forms.TextBox();
            this.SuspendLayout();
            // 
            // txt_age
            // 
            this.txt_age.Location = new System.Drawing.Point(154, 108);
            this.txt_age.Name = "txt_age";
            this.txt_age.Size = new System.Drawing.Size(100, 20);
            this.txt_age.TabIndex = 9;
            // 
            // Label2
            // 
            this.Label2.AutoSize = true;
            this.Label2.Location = new System.Drawing.Point(30, 108);
            this.Label2.Name = "Label2";
            this.Label2.Size = new System.Drawing.Size(79, 13);
            this.Label2.TabIndex = 8;
            this.Label2.Text = "Enter Your Age";
            // 
            // Label1
            // 
            this.Label1.AutoSize = true;
            this.Label1.Location = new System.Drawing.Point(30, 50);
            this.Label1.Name = "Label1";
            this.Label1.Size = new System.Drawing.Size(120, 13);
            this.Label1.TabIndex = 7;
            this.Label1.Text = "Enter Your Family Name";
            // 
            // btn_Display_form
            // 
            this.btn_Display_form.Location = new System.Drawing.Point(107, 171);
            this.btn_Display_form.Name = "btn_Display_form";
            this.btn_Display_form.Size = new System.Drawing.Size(110, 42);
            this.btn_Display_form.TabIndex = 6;
            this.btn_Display_form.Text = "Open Second Form and pass data";
            this.btn_Display_form.UseVisualStyleBackColor = true;
            this.btn_Display_form.Click += new System.EventHandler(this.btn_Display_form_Click);
            // 
            // txt_family_name
            // 
            this.txt_family_name.Location = new System.Drawing.Point(154, 47);
            this.txt_family_name.Name = "txt_family_name";
            this.txt_family_name.Size = new System.Drawing.Size(100, 20);
            this.txt_family_name.TabIndex = 5;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(30, 78);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(119, 13);
            this.label3.TabIndex = 10;
            this.label3.Text = "Enter Your Given Name";
            // 
            // txt_given_name
            // 
            this.txt_given_name.Location = new System.Drawing.Point(154, 78);
            this.txt_given_name.Name = "txt_given_name";
            this.txt_given_name.Size = new System.Drawing.Size(100, 20);
            this.txt_given_name.TabIndex = 11;
            // 
            // frm_main
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(284, 261);
            this.Controls.Add(this.txt_given_name);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.txt_age);
            this.Controls.Add(this.Label2);
            this.Controls.Add(this.Label1);
            this.Controls.Add(this.btn_Display_form);
            this.Controls.Add(this.txt_family_name);
            this.Name = "frm_main";
            this.Text = "Opening Form";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        internal System.Windows.Forms.TextBox txt_age;
        internal System.Windows.Forms.Label Label2;
        internal System.Windows.Forms.Label Label1;
        internal System.Windows.Forms.Button btn_Display_form;
        internal System.Windows.Forms.TextBox txt_family_name;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox txt_given_name;
    }
}

