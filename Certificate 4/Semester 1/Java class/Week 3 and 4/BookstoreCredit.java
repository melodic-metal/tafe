import java.util.Scanner;

public class BookstoreCredit {

   public static void main(String[] args) {
      String name  = "";
      double gpa = 0;
      Scanner keyboard = new Scanner(System.in);
      System.out.println("Please enter your name >>");
      name = keyboard.nextLine();
      System.out.println("Please enter your GPA >>");
      gpa = keyboard.nextDouble();
      
      System.out.println("Hello " + name + ". You get a credit of " + calculateCredit(gpa));
   
   }
   
   public static double calculateCredit(double a) {
      double gpa = 0;
      gpa =  a * 10;
      return gpa;
         
   }

}