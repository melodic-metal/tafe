﻿namespace BrendanKellyJ082400Week12Exercises.Question3
{
    partial class start_form
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btn_small = new System.Windows.Forms.Button();
            this.btn_med = new System.Windows.Forms.Button();
            this.btn_large = new System.Windows.Forms.Button();
            this.lbl_output = new System.Windows.Forms.Label();
            this.txt_total = new System.Windows.Forms.TextBox();
            this.SuspendLayout();
            // 
            // btn_small
            // 
            this.btn_small.Location = new System.Drawing.Point(11, 37);
            this.btn_small.Name = "btn_small";
            this.btn_small.Size = new System.Drawing.Size(75, 23);
            this.btn_small.TabIndex = 0;
            this.btn_small.Text = "Small Car";
            this.btn_small.UseVisualStyleBackColor = true;
            this.btn_small.Click += new System.EventHandler(this.btn_small_Click);
            // 
            // btn_med
            // 
            this.btn_med.Location = new System.Drawing.Point(105, 37);
            this.btn_med.Name = "btn_med";
            this.btn_med.Size = new System.Drawing.Size(75, 23);
            this.btn_med.TabIndex = 1;
            this.btn_med.Text = "Medium Car";
            this.btn_med.UseVisualStyleBackColor = true;
            this.btn_med.Click += new System.EventHandler(this.btn_med_Click);
            // 
            // btn_large
            // 
            this.btn_large.Location = new System.Drawing.Point(197, 37);
            this.btn_large.Name = "btn_large";
            this.btn_large.Size = new System.Drawing.Size(75, 23);
            this.btn_large.TabIndex = 2;
            this.btn_large.Text = "Large Car";
            this.btn_large.UseVisualStyleBackColor = true;
            this.btn_large.Click += new System.EventHandler(this.btn_large_Click);
            // 
            // lbl_output
            // 
            this.lbl_output.AutoSize = true;
            this.lbl_output.Location = new System.Drawing.Point(53, 139);
            this.lbl_output.Name = "lbl_output";
            this.lbl_output.Size = new System.Drawing.Size(66, 13);
            this.lbl_output.TabIndex = 4;
            this.lbl_output.Text = "Total Cost Is";
            // 
            // txt_total
            // 
            this.txt_total.Location = new System.Drawing.Point(126, 135);
            this.txt_total.Name = "txt_total";
            this.txt_total.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.txt_total.Size = new System.Drawing.Size(100, 20);
            this.txt_total.TabIndex = 5;
            // 
            // StartForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(284, 261);
            this.Controls.Add(this.txt_total);
            this.Controls.Add(this.lbl_output);
            this.Controls.Add(this.btn_large);
            this.Controls.Add(this.btn_med);
            this.Controls.Add(this.btn_small);
            this.Name = "StartForm";
            this.Text = "StartForm";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btn_small;
        private System.Windows.Forms.Button btn_med;
        private System.Windows.Forms.Button btn_large;
        private System.Windows.Forms.Label lbl_output;
        private System.Windows.Forms.TextBox txt_total;
    }
}