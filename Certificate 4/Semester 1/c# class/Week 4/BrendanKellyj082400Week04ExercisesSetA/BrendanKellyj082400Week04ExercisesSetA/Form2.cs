﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace BrendanKellyj082400Week04ExercisesSetA
{
    public partial class Form2 : Form
    {
        public Form2()
        {
            InitializeComponent();
        }

        private void btn_calc_Click(object sender, EventArgs e)
        {
            int int_start_reading = 0; // initialise and set start reading to 0
            int int_end_reading = 0; // initialise and set end reading to 0
            decimal dec_cost = .35M; // initialise and set the cost of water to the default of 35 cents
            int int_amount_of_water_used = 0; // // initialise and set amount of water used to default of 0 KL

            if (!int.TryParse(txt_start.Text, out int_start_reading ) || !int.TryParse(txt_end.Text, out int_end_reading)) // Try to parse input into int
            {
                MessageBox.Show("Please enter valid numbers");
                txt_start.Clear();
                txt_end.Clear(); // clear the two text boxes
                txt_start.Focus(); // focus into the first input so user can start again
                return;
            }
            int_amount_of_water_used = int_end_reading - int_start_reading;  // calculate amount of water used

            if (int_amount_of_water_used <= 100)
            {
                dec_cost = .35M; // set price to 35 cents, as it's the first 100 KL
            }
            else
            {
                int_amount_of_water_used = int_amount_of_water_used - 100; // minus 100 KL as the first 100 KL is 
                dec_cost = dec_cost + .55M;
            }
            dec_cost = int_amount_of_water_used * dec_cost;
            txt_cost.Text = dec_cost.ToString("C");
        }
    }
}
