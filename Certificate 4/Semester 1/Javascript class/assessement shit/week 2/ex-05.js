/**
 *
 *
 * Calculations in JS
 *
 *
 * @File:       /Ep02/js/ex-06.js
 * @Project:    Classwork
 * @Author:     Brendan Kelly <j082400@tafe.wa.edu.au>
 * @Version:    1.0
 * @Copyright:  Brendan Kelly 2018
 *
 * History:
 * v1.0    2018-07-24
 *         Calculations and stuff
 *
 * v0.1    2018-07-24
 *         Template as provided by Adrian Gould
 *
 */
"use strict";

let tempCelsius = 0;
let tempFahrenheit = 140;
alert(5*(tempFahrenheit-32)/9);