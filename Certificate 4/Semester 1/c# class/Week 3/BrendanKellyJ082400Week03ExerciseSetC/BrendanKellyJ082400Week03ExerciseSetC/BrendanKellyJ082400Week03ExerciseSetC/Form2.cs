﻿// Programmer : Brendan Kelly 
// Date : August 2018
// Purpose : Application takes in input in the form of hours worked, plus cost of materials, the calculates total cost

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace BrendanKellyJ082400Week03ExerciseSetC
{
    public partial class Form2 : Form
    {
        public Form2()
        {
            InitializeComponent();
        }

        private void btnCalculate_Click(object sender, EventArgs e)
        {
            double dblHours = double.Parse(txtHours.Text);
            double dblMats = double.Parse(txtMats.Text);
            double dblTotal = 60 + (dblHours * 90) + dblMats;

            txtTotal.Text = dblTotal.ToString("c");
        }
    }
}
