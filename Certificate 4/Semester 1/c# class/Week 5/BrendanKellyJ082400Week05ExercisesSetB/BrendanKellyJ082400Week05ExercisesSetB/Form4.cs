﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace BrendanKellyJ082400Week05ExercisesSetB
{
    public partial class Form4 : Form
    {
        public Form4()
        {
            InitializeComponent();
        }

        private void btn_calculate_Click(object sender, EventArgs e)
        {
            lst_table.Items.Clear();
            if (!int.TryParse(txt_input.Text, out int int_user_input))
            {
                MessageBox.Show("Please enter a valid integer");
            }



            for (int i = 1; i < int_user_input; i++)
            {
                if (int_user_input % i   == 0) {
                    lst_table.Items.Add(i);
                }
            }

        }
    }
}
