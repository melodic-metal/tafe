﻿namespace BrendanKellyJ082400Week07ExerciseSetA
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lbL_header = new System.Windows.Forms.Label();
            this.lbl_passengers = new System.Windows.Forms.Label();
            this.txt_passengers = new System.Windows.Forms.TextBox();
            this.lst_method = new System.Windows.Forms.ListBox();
            this.lbl_method = new System.Windows.Forms.Label();
            this.lbl_cost = new System.Windows.Forms.Label();
            this.txt_cost = new System.Windows.Forms.TextBox();
            this.btn_calc = new System.Windows.Forms.Button();
            this.btn_reset = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // lbL_header
            // 
            this.lbL_header.AutoSize = true;
            this.lbL_header.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbL_header.Location = new System.Drawing.Point(76, 13);
            this.lbL_header.Name = "lbL_header";
            this.lbL_header.Size = new System.Drawing.Size(136, 20);
            this.lbL_header.TabIndex = 0;
            this.lbL_header.Text = "Perth to Sydney";
            // 
            // lbl_passengers
            // 
            this.lbl_passengers.AutoSize = true;
            this.lbl_passengers.Location = new System.Drawing.Point(15, 58);
            this.lbl_passengers.Name = "lbl_passengers";
            this.lbl_passengers.Size = new System.Drawing.Size(94, 13);
            this.lbl_passengers.TabIndex = 1;
            this.lbl_passengers.Text = "No. of Passengers";
            // 
            // txt_passengers
            // 
            this.txt_passengers.Location = new System.Drawing.Point(115, 55);
            this.txt_passengers.Name = "txt_passengers";
            this.txt_passengers.Size = new System.Drawing.Size(100, 20);
            this.txt_passengers.TabIndex = 2;
            // 
            // lst_method
            // 
            this.lst_method.FormattingEnabled = true;
            this.lst_method.Items.AddRange(new object[] {
            "Bus",
            "Train",
            "Plane"});
            this.lst_method.Location = new System.Drawing.Point(115, 131);
            this.lst_method.Name = "lst_method";
            this.lst_method.Size = new System.Drawing.Size(118, 43);
            this.lst_method.TabIndex = 3;
            // 
            // lbl_method
            // 
            this.lbl_method.AutoSize = true;
            this.lbl_method.Location = new System.Drawing.Point(112, 115);
            this.lbl_method.Name = "lbl_method";
            this.lbl_method.Size = new System.Drawing.Size(121, 13);
            this.lbl_method.TabIndex = 4;
            this.lbl_method.Text = "Select Method of Travel";
            // 
            // lbl_cost
            // 
            this.lbl_cost.AutoSize = true;
            this.lbl_cost.Location = new System.Drawing.Point(40, 222);
            this.lbl_cost.Name = "lbl_cost";
            this.lbl_cost.Size = new System.Drawing.Size(61, 13);
            this.lbl_cost.TabIndex = 5;
            this.lbl_cost.Text = "Travel Cost";
            // 
            // txt_cost
            // 
            this.txt_cost.Location = new System.Drawing.Point(115, 219);
            this.txt_cost.Name = "txt_cost";
            this.txt_cost.Size = new System.Drawing.Size(100, 20);
            this.txt_cost.TabIndex = 6;
            // 
            // btn_calc
            // 
            this.btn_calc.Location = new System.Drawing.Point(12, 115);
            this.btn_calc.Name = "btn_calc";
            this.btn_calc.Size = new System.Drawing.Size(75, 23);
            this.btn_calc.TabIndex = 7;
            this.btn_calc.Text = "Calculate";
            this.btn_calc.UseVisualStyleBackColor = true;
            this.btn_calc.Click += new System.EventHandler(this.btn_calc_Click);
            // 
            // btn_reset
            // 
            this.btn_reset.Location = new System.Drawing.Point(12, 151);
            this.btn_reset.Name = "btn_reset";
            this.btn_reset.Size = new System.Drawing.Size(75, 23);
            this.btn_reset.TabIndex = 8;
            this.btn_reset.Text = "Reset";
            this.btn_reset.UseVisualStyleBackColor = true;
            this.btn_reset.Click += new System.EventHandler(this.btn_reset_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(284, 261);
            this.Controls.Add(this.btn_reset);
            this.Controls.Add(this.btn_calc);
            this.Controls.Add(this.txt_cost);
            this.Controls.Add(this.lbl_cost);
            this.Controls.Add(this.lbl_method);
            this.Controls.Add(this.lst_method);
            this.Controls.Add(this.txt_passengers);
            this.Controls.Add(this.lbl_passengers);
            this.Controls.Add(this.lbL_header);
            this.Name = "Form1";
            this.Text = "Fare Calculator";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lbL_header;
        private System.Windows.Forms.Label lbl_passengers;
        private System.Windows.Forms.TextBox txt_passengers;
        private System.Windows.Forms.ListBox lst_method;
        private System.Windows.Forms.Label lbl_method;
        private System.Windows.Forms.Label lbl_cost;
        private System.Windows.Forms.TextBox txt_cost;
        private System.Windows.Forms.Button btn_calc;
        private System.Windows.Forms.Button btn_reset;
    }
}

