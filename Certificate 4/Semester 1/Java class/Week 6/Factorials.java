// Date: 25/08/2018
// Programmer: Brendan Kelly J082400
// Purpose: Find factorials of a number

public class Factorials {


   public static void main(String[] args) {

       int factorial = 1;
       for (int i = 1; i <= 10; i++) {
           factorial = factorial * i;
       }
       System.out.println("The factorial of 10 is " + factorial);
   }
}

