﻿// Date: 6/09/2018
// Programmer: Brendan Kelly (J082400)
// Purpose: put product names and prices into a listbox

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace BrendanKellyJ082400ExercisesSetA
{
    public partial class Form5 : Form
    {
        public struct products
        {
            public string product_name;
            public double product_price;
        };

        List<products> product_list = new List<products>();
        products product;

        public Form5()
        {
            InitializeComponent();
        }

        private void btn_accept_Click_1(object sender, EventArgs e)
        {
            if (!double.TryParse(txt_price.Text, out double dbl_prices_temp))
            {
                MessageBox.Show("Please enter a valid number");
            }
            product.product_name = txt_name.Text;
            product.product_price = dbl_prices_temp;
            product_list.Add(product);

        }

        private void btn_price_Click_1(object sender, EventArgs e)
        {
            lst_output.Items.Clear();
            
            for(int i = 0; i < product_list.Count; i++)
            {
                if (product.product_price > 10)
                {
                    lst_output.Items.Add(product_list[i].product_name + " " + product_list[i].product_price);
                }
            }

         }



     }
 }
    

