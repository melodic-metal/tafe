const pictureDelay = 5000;
let pictures = [
    {
        "filename": "koala.jpg",
        "name": "Koala"
    },
    {
        "filename": "sugar-glider.png",
        "name": "Sugar Glider"
    },
    {
        "filename": "wombat.jpg",
        "name": "Wombat"
    },
    {
        "filename": "kangaroo.jpg",
        "name": "Kangaroo"
    },
    {
        "filename": "numbat.jpg",
        "name": "Numbat"
    },
    {
        "filename": "quokka.jpg",
        "name": "Quokka"
    },
    {
        "filename": "quoll.jpg",
        "name": "Quoll"
    }
];

function randomPicture() {
    let numPictures = pictures.length - 1;
    let randomPicture = Math.floor(Math.random() * numPictures);
    changeCaption(randomPicture);
    return pictures[randomPicture].filename;
}

function createXHR() {
    let xhr;
    if (window.XMLHttpRequest || window.ActiveXObject) {
        if (window.XMLHttpRequest) {
             xhr = new XMLHttpRequest();

        }
        else {
            try {
                xhr = new ActiveXObject("Msxml2.XMLHTTP");

            }
            catch (exception) {
                xhr = new ActiveXObject("Microsoft.XMLHTTP");

            }
        }
    }
    else {
        return false;
    }

    xhr.open("GET", "media/" + randomPicture(), true);   // Make sure file is in same server
    xhr.overrideMimeType('text/plain; charset=x-user-defined');
    xhr.send(null);

    xhr.onreadystatechange = function () {
        if (xhr.readyState == 4) {
            if ((xhr.status == 200) || (xhr.status == 0)) {
                let image = document.getElementById("photo");
                image.src = "data:image/jpg;base64," + encode64(xhr.responseText);
            } else {
                alert("Something misconfiguration : " +
                    "\nError Code : " + xhr.status +
                    "\nError Message : " + xhr.responseText);
            }
        }
    };
    return xhr;

}

function encode64(inputStr){
    var b64 = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/=";
    var outputStr = "";
    var i = 0;

    while (i<inputStr.length){
        var byte1 = inputStr.charCodeAt(i++) & 0xff;
        var byte2 = inputStr.charCodeAt(i++) & 0xff;
        var byte3 = inputStr.charCodeAt(i++) & 0xff;

        var enc1 = byte1 >> 2;
        var enc2 = ((byte1 & 3) << 4) | (byte2 >> 4);

        var enc3, enc4;
        if (isNaN(byte2)){
            enc3 = enc4 = 64;
        } else{
            enc3 = ((byte2 & 15) << 2) | (byte3 >> 6);
            if (isNaN(byte3)){
                enc4 = 64;
            } else {
                enc4 = byte3 & 63;
            }
        }
        outputStr +=  b64.charAt(enc1) + b64.charAt(enc2) + b64.charAt(enc3) + b64.charAt(enc4);
    }
    return outputStr;
}

function slideShowStart() {
    let theTimer = setTimeout(function request() {
        slideShow();

        theTimer = setTimeout(request, pictureDelay)
    }, pictureDelay)
}

function slideShow() {
    let requestObject = new createXHR();

    if (requestObject == false) {
        outputZone.innerHTML = "<p class='alert alert-danger'>Request Status - 404 NOT FOUND</p>"
    }


}

function changeCaption(number) {
    let captionZone = document.getElementById("photo-caption");

    captionZone.innerText = "A cute " + pictures[number].name + " doing his thing";


}

function updateDots() {
          
}

slideShowStart();