﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace BrendanKellyJ082400Week05ExerciseSetA
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void btn_calc_Click(object sender, EventArgs e)
        {
            int i = 0;

            lst_tables.Items.Add("Number\t\tX9");
            while (i <= 12)
            {
                lst_tables.Items.Add(i + "X9 = \t\t" + (i * 9).ToString());
                    
                i++;
            }

        }
    }
}
