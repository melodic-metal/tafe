// Date: 13/09/2018
// Programmer: Brendan Kelly 
// Purpose: set up fields for a haiku

public class Haiku extends Poem {
  
   private static String poemName;
   public Haiku(String name) {
      super(name, 3);
      poemName = name;
   }

      public static String getName() {
      return poemName;
   }
}