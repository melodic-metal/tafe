﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace BrendanKellyJ082400ExerciseSetA
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        String[] str_states = { "WA", "NSW", "SA", "VIC", "QLD", "TAS" };
        const int ARRAY_LENGTH = 6;

        private void btn_normal_order_Click(object sender, EventArgs e)
        {

            lst_states.Items.Clear();
            for (int i = 0; i < ARRAY_LENGTH; i++)
            {
                lst_states.Items.Add(str_states[i]);
            }
        }

        private void btn_reverse_order_Click(object sender, EventArgs e)
        {
            lst_states.Items.Clear();
            for (int i = ARRAY_LENGTH - 1; i >= 0; i--)
            {
                lst_states.Items.Add(str_states[i]);
            }
        }
    }


}
