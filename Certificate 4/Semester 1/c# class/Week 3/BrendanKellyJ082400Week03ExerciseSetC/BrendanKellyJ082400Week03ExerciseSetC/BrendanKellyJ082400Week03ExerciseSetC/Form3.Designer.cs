﻿namespace BrendanKellyJ082400Week03ExerciseSetC
{
    partial class Form3
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lblBillingPeriod = new System.Windows.Forms.Label();
            this.lblNumberOfCalls = new System.Windows.Forms.Label();
            this.txtBillingPeriod = new System.Windows.Forms.TextBox();
            this.txtNumberOfCalls = new System.Windows.Forms.TextBox();
            this.btnCalculate = new System.Windows.Forms.Button();
            this.lblTotal = new System.Windows.Forms.Label();
            this.txtTotal = new System.Windows.Forms.TextBox();
            this.SuspendLayout();
            // 
            // lblBillingPeriod
            // 
            this.lblBillingPeriod.AutoSize = true;
            this.lblBillingPeriod.Location = new System.Drawing.Point(38, 62);
            this.lblBillingPeriod.Name = "lblBillingPeriod";
            this.lblBillingPeriod.Size = new System.Drawing.Size(67, 13);
            this.lblBillingPeriod.TabIndex = 0;
            this.lblBillingPeriod.Text = "Billing Period";
            // 
            // lblNumberOfCalls
            // 
            this.lblNumberOfCalls.AutoSize = true;
            this.lblNumberOfCalls.Location = new System.Drawing.Point(38, 107);
            this.lblNumberOfCalls.Name = "lblNumberOfCalls";
            this.lblNumberOfCalls.Size = new System.Drawing.Size(81, 13);
            this.lblNumberOfCalls.TabIndex = 1;
            this.lblNumberOfCalls.Text = "Number of Calls";
            // 
            // txtBillingPeriod
            // 
            this.txtBillingPeriod.Location = new System.Drawing.Point(139, 62);
            this.txtBillingPeriod.Name = "txtBillingPeriod";
            this.txtBillingPeriod.Size = new System.Drawing.Size(100, 20);
            this.txtBillingPeriod.TabIndex = 2;
            // 
            // txtNumberOfCalls
            // 
            this.txtNumberOfCalls.Location = new System.Drawing.Point(139, 100);
            this.txtNumberOfCalls.Name = "txtNumberOfCalls";
            this.txtNumberOfCalls.Size = new System.Drawing.Size(100, 20);
            this.txtNumberOfCalls.TabIndex = 3;
            // 
            // btnCalculate
            // 
            this.btnCalculate.Location = new System.Drawing.Point(108, 182);
            this.btnCalculate.Name = "btnCalculate";
            this.btnCalculate.Size = new System.Drawing.Size(75, 23);
            this.btnCalculate.TabIndex = 4;
            this.btnCalculate.Text = "Calculate";
            this.btnCalculate.UseVisualStyleBackColor = true;
            this.btnCalculate.Click += new System.EventHandler(this.btnCalculate_Click);
            // 
            // lblTotal
            // 
            this.lblTotal.AutoSize = true;
            this.lblTotal.Location = new System.Drawing.Point(74, 143);
            this.lblTotal.Name = "lblTotal";
            this.lblTotal.Size = new System.Drawing.Size(31, 13);
            this.lblTotal.TabIndex = 5;
            this.lblTotal.Text = "Total";
            // 
            // txtTotal
            // 
            this.txtTotal.Location = new System.Drawing.Point(139, 143);
            this.txtTotal.Name = "txtTotal";
            this.txtTotal.Size = new System.Drawing.Size(100, 20);
            this.txtTotal.TabIndex = 6;
            // 
            // Form3
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(284, 261);
            this.Controls.Add(this.txtTotal);
            this.Controls.Add(this.lblTotal);
            this.Controls.Add(this.btnCalculate);
            this.Controls.Add(this.txtNumberOfCalls);
            this.Controls.Add(this.txtBillingPeriod);
            this.Controls.Add(this.lblNumberOfCalls);
            this.Controls.Add(this.lblBillingPeriod);
            this.Name = "Form3";
            this.Text = "Form3";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lblBillingPeriod;
        private System.Windows.Forms.Label lblNumberOfCalls;
        private System.Windows.Forms.TextBox txtBillingPeriod;
        private System.Windows.Forms.TextBox txtNumberOfCalls;
        private System.Windows.Forms.Button btnCalculate;
        private System.Windows.Forms.Label lblTotal;
        private System.Windows.Forms.TextBox txtTotal;
    }
}