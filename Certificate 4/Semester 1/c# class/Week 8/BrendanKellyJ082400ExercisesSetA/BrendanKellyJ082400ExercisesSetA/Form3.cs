﻿// Date: 6/09/2018
// Programmer: Brendan Kelly (J082400)
// Purpose: input people's names and ages, and get averages out

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace BrendanKellyJ082400ExercisesSetA
{
    public partial class Form3 : Form
    {
        struct person_details
        {
            public string person_name;
            public int person_age;
        }

        List<person_details> people = new List<person_details>();
        person_details person;
        public Form3()
        {
            InitializeComponent();
        }

        private void btn_accept_Click(object sender, EventArgs e)
        {
            if(!int.TryParse(txt_age.Text, out int int_age))
            {
                MessageBox.Show("Please enter a valid age in age textbox");
            }
            person.person_name = txt_name.Text;
            person.person_age = int_age;
            people.Add(person);
            txt_age.Clear();
            txt_name.Clear();

        }

        private void btn_average_Click(object sender, EventArgs e)
        {
            int int_total_ages = 0,  int_count_above_avg = 0;
            double int_average_age;
            for (int i = 0; i < people.Count; i++)
            {
                int_total_ages += people[i].person_age;
            }
            int_average_age = int_total_ages / people.Count;
            txt_average.Text = int_average_age.ToString();
            for (int i = 0; i < people.Count; i++)
            {
                if(people[i].person_age > int_average_age )
                {
                    int_count_above_avg += 1;
                }
            }
            txt_above_avg.Text = int_count_above_avg.ToString();
            
        }
    }
}
