﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;

namespace BrendanKellyJ082400Assessment2
{
    public partial class main_form : Form
    {
        public struct Products
        {
            public string strCode;
            public string strName;
            public int intStock;
            public double dblPrice;
            public string strImage;
        }
        List<string> lst_products = new List<string>();
        List<double> lst_price = new List<Double>();
        List<int> lst_stock = new List<int>();
        public main_form()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            txt_price.ReadOnly = true;
            txt_cost.ReadOnly = true;

            StreamReader inFile;
            inFile = File.OpenText("products1.txt");
            string strRead;
            while ((strRead = inFile.ReadLine()) != null)
            {
                Products PrdObj;
                string[] strTempArr = new string[5];
                strTempArr = strRead.Split(',');
                PrdObj.strCode = strTempArr[0];
                PrdObj.strName = strTempArr[1];
                PrdObj.intStock = int.Parse(strTempArr[2]);
                PrdObj.dblPrice = double.Parse(strTempArr[3]);
                PrdObj.strImage = strTempArr[4];
                lst_products.Add(string.Join("\r\n", PrdObj));
            }
            foreach (Products PrdObj in lst_products)
            {
                lst_products.Add(PrdObj.strName);
                lst_price.Add(PrdObj.dblPrice);
                lst_stock.Add(PrdObj.intStock);
            }
            inFile.Close();

            txt_quantity.SelectAll();
            txt_quantity.Focus();
            btn_confirm.Visible = false;
            btn_cancel.Visible = false;

            txt_quantity.Text = "0";


            this.pic_pic.SizeMode = PictureBoxSizeMode.Zoom;



            txt_quantity.SelectAll();
            txt_quantity.Focus();
            btn_confirm.Visible = false;
            btn_cancel.Visible = false;

            txt_quantity.Text = "0";


            this.pic_pic.SizeMode = PictureBoxSizeMode.Zoom;

        }



        private void btn_sell_Click(object sender, EventArgs e)
        {
            if (!int.TryParse(txt_quantity.Text, out int int_quantity))
            {
                MessageBox.Show("Please enter a valid quantity");
                txt_quantity.Clear();
                txt_quantity.Focus();

            }
        }

        private void btn_summary_Click(object sender, EventArgs e)
        {
            sales_summary_form sales_summary = new sales_summary_form();
            sales_summary.ShowDialog();
        }

        private void btn_cancel_Click(object sender, EventArgs e)
        {
            txt_quantity.Clear();

        }

    }
}
