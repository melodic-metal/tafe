﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace BrendanKellyWeek02ExerciseSetB
{
    public partial class Form3 : Form
    {
        public Form3()
        {
            InitializeComponent();
        }

        private void txtFirstName_Enter(object sender, EventArgs e)
        {
            txtFamilyName.Clear();
            txtFirstName.Clear();
            txtFullName.Clear();
        }

        private void txtFullName_Enter(object sender, EventArgs e)
        {
            txtFullName.Text = txtFirstName.Text + " " + txtFamilyName.Text;
        }
    }
}
