﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace BrendanKellyJ082400ExerciseSetA
{
    public partial class Form4 : Form
    {
        String[] days_of_week = { "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday", "Sunday" }; 
        public Form4()
        {
            InitializeComponent();
        }

        private void btn_calc_Click(object sender, EventArgs e)
        {
            
            if(!int.TryParse(txt_input.Text, out int int_output))
            {
                MessageBox.Show("Please enter a valid number");
            }
            else if (int_output < 1 || int_output > 7 )
            {
                MessageBox.Show("Please enter a number between 1 and 7");
            }

            for (int i = 0; i < days_of_week.Length; i++)
            {
                if (int_output == i)
                {
                    txt_output.Text = days_of_week[i - 1];
                }

            }
            
        }
    }
}
