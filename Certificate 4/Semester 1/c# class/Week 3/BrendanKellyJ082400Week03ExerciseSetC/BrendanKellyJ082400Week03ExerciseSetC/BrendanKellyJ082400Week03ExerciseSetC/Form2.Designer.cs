﻿namespace BrendanKellyJ082400Week03ExerciseSetC
{
    partial class Form2
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lblHours = new System.Windows.Forms.Label();
            this.lblMats = new System.Windows.Forms.Label();
            this.lblTotal = new System.Windows.Forms.Label();
            this.txtHours = new System.Windows.Forms.TextBox();
            this.txtMats = new System.Windows.Forms.TextBox();
            this.txtTotal = new System.Windows.Forms.TextBox();
            this.btnCalculate = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // lblHours
            // 
            this.lblHours.AutoSize = true;
            this.lblHours.Location = new System.Drawing.Point(42, 65);
            this.lblHours.Name = "lblHours";
            this.lblHours.Size = new System.Drawing.Size(73, 13);
            this.lblHours.TabIndex = 0;
            this.lblHours.Text = "Hours worked";
            // 
            // lblMats
            // 
            this.lblMats.AutoSize = true;
            this.lblMats.Location = new System.Drawing.Point(42, 100);
            this.lblMats.Name = "lblMats";
            this.lblMats.Size = new System.Drawing.Size(68, 13);
            this.lblMats.TabIndex = 1;
            this.lblMats.Text = "Material Cost";
            // 
            // lblTotal
            // 
            this.lblTotal.AutoSize = true;
            this.lblTotal.Location = new System.Drawing.Point(42, 146);
            this.lblTotal.Name = "lblTotal";
            this.lblTotal.Size = new System.Drawing.Size(55, 13);
            this.lblTotal.TabIndex = 2;
            this.lblTotal.Text = "Total Cost";
            // 
            // txtHours
            // 
            this.txtHours.Location = new System.Drawing.Point(122, 57);
            this.txtHours.Name = "txtHours";
            this.txtHours.Size = new System.Drawing.Size(100, 20);
            this.txtHours.TabIndex = 4;
            // 
            // txtMats
            // 
            this.txtMats.Location = new System.Drawing.Point(122, 100);
            this.txtMats.Name = "txtMats";
            this.txtMats.Size = new System.Drawing.Size(100, 20);
            this.txtMats.TabIndex = 5;
            // 
            // txtTotal
            // 
            this.txtTotal.Location = new System.Drawing.Point(122, 138);
            this.txtTotal.Name = "txtTotal";
            this.txtTotal.Size = new System.Drawing.Size(100, 20);
            this.txtTotal.TabIndex = 6;
            // 
            // btnCalculate
            // 
            this.btnCalculate.Location = new System.Drawing.Point(95, 199);
            this.btnCalculate.Name = "btnCalculate";
            this.btnCalculate.Size = new System.Drawing.Size(75, 23);
            this.btnCalculate.TabIndex = 7;
            this.btnCalculate.Text = "Calculate";
            this.btnCalculate.UseVisualStyleBackColor = true;
            this.btnCalculate.Click += new System.EventHandler(this.btnCalculate_Click);
            // 
            // Form2
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(284, 261);
            this.Controls.Add(this.btnCalculate);
            this.Controls.Add(this.txtTotal);
            this.Controls.Add(this.txtMats);
            this.Controls.Add(this.txtHours);
            this.Controls.Add(this.lblTotal);
            this.Controls.Add(this.lblMats);
            this.Controls.Add(this.lblHours);
            this.Name = "Form2";
            this.Text = "Form2";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lblHours;
        private System.Windows.Forms.Label lblMats;
        private System.Windows.Forms.Label lblTotal;
        private System.Windows.Forms.TextBox txtHours;
        private System.Windows.Forms.TextBox txtMats;
        private System.Windows.Forms.TextBox txtTotal;
        private System.Windows.Forms.Button btnCalculate;
    }
}