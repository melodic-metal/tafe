<?php
/**
 * Created by PhpStorm.
 * User: kellyb
 * Date: 9/10/2018
 * Time: 2:51 PM
 */

class Car implements \JsonSerializable
{

    public function __construct($make, $model, $colour, $year, $firstName, $lastName)
    {
        $this->make = $make;
        $this->model = $model;
        $this->colour = $colour;
        $this->year = $year;
        $this->firstName = $firstName;
        $this->lastName = $lastName;

    }

    public function jsonSerialize()
    {
        return get_object_vars($this);
    }


}

$car1 = new Car("Ford", "Focus", "red", 1993, "Mark", "Butterworth");
$car2 = new Car("Honda", "Civic", "blue", 2003, "Jill", "Jaloney");
$car3 = new Car("Porsche", "911", "pink", 1994, "Emily", "Blunt");
$car4 = new Car("Ferrari", "Testarossa", "black", 2012, "Mark", "Smith");


$searchby = $_GET["searchby"];

$cars = array($car1, $car2, $car3, $car4);

if ($searchby == "person") {

    if(isset($_GET['firstname']) && !empty($_GET['firstname'])){

        $first = $_GET["firstname"];
    } else { echo "not set"; }
    $last = $_GET["lastname"];
    foreach ($cars as $car) {
        if (strcmp($first, $car->firstName) == 0) {
            echo json_encode($car);
        }

    }
}
elseif ($searchby == "car") {
    $make = $_GET["make"];
    $model = $_GET["model"];
    $colour = $_GET["colour"];
    $year = $_GET["year"];
    foreach ($cars as $car) {
        if (strcmp($model, $car->model) == 0) {
            echo json_encode($car);
        }
    }

}



