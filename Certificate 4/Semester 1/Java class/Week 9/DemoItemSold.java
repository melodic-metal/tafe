// Date: 13/09/2018
// Programmer: Brendan Kelly
// Purpose: Demo item/pet sold classes

public class DemoItemSold {

   public static void main(String[] args) {
   
      ItemSold itemSold = new ItemSold();
      PetSold petSold = new PetSold();
      
      itemSold.setInvoiceNumber(123456);
      itemSold.setDescription("Did some stuff");
      itemSold.setPrice(45.99);
      
      petSold.setHouseBroken(true);
      petSold.setNeutered(false);
      petSold.setVaccinated(true);
      
      System.out.println("Demo PetSold/ItemSold classes");
      
      //ItemSold class stuff
      System.out.println("Invoice Number: " + itemSold.getInvoiceNumber() + "\nFor Service: " + itemSold.getDescription() + "\nwhich cost " + itemSold.getPrice());
      
      //PetSold class stuff
      System.out.println("Vaccinated: " + petSold.getVaccinated() + "\tNeutered: " + petSold.getNeutered() + "\tHouse Broken: " + petSold.getHouseBroken());
   
   }

}