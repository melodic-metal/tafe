import java.util.Scanner;

public class CarCareChoice {

   public static void main(String[] args) {
      
      double price = 0;
      boolean isValid = false;
      Scanner keyboard = new Scanner(System.in);
      System.out.println("Please enter a code to view prices of car servicing");
      System.out.println("Valid choices are: ");
      System.out.println("oil change, tyre rotation, battery check, or brake inspection");
      String userInput = keyboard.nextLine();
      
      switch(userInput) {
         case "oil change":
            price = 25;
            isValid = true;
            break;
         case "tyre rotation":
            price = 22;
            isValid = true;
            break;
         case "battery check":
            price = 15;
            isValid = true;
            break;
         case "brake inspection":
            price = 5;
            isValid = true;
            break;
         default:
            isValid = false;
            break;   
            
         }
         if(isValid == false) {
            System.out.println("Invalid selection");   
         }
         else {
            System.out.println("The price for " + userInput + " is $" + price);
         }

} 
}