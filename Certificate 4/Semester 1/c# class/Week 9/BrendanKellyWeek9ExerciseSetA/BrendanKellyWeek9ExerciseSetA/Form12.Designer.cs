﻿namespace BrendanKellyWeek9ExerciseSetA
{
    partial class Form12
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.txt_age = new System.Windows.Forms.TextBox();
            this.lbl_select = new System.Windows.Forms.Label();
            this.dte_date_of_birth = new System.Windows.Forms.DateTimePicker();
            this.lbl_age = new System.Windows.Forms.Label();
            this.btn_calc = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // txt_age
            // 
            this.txt_age.Location = new System.Drawing.Point(124, 71);
            this.txt_age.Name = "txt_age";
            this.txt_age.Size = new System.Drawing.Size(200, 20);
            this.txt_age.TabIndex = 0;
            // 
            // lbl_select
            // 
            this.lbl_select.AutoSize = true;
            this.lbl_select.Location = new System.Drawing.Point(5, 51);
            this.lbl_select.Name = "lbl_select";
            this.lbl_select.Size = new System.Drawing.Size(101, 13);
            this.lbl_select.TabIndex = 2;
            this.lbl_select.Text = "Select Date Of Birth";
            // 
            // dte_date_of_birth
            // 
            this.dte_date_of_birth.Location = new System.Drawing.Point(124, 45);
            this.dte_date_of_birth.Name = "dte_date_of_birth";
            this.dte_date_of_birth.Size = new System.Drawing.Size(200, 20);
            this.dte_date_of_birth.TabIndex = 3;
            // 
            // lbl_age
            // 
            this.lbl_age.AutoSize = true;
            this.lbl_age.Location = new System.Drawing.Point(41, 71);
            this.lbl_age.Name = "lbl_age";
            this.lbl_age.Size = new System.Drawing.Size(65, 13);
            this.lbl_age.TabIndex = 4;
            this.lbl_age.Text = "Age in years";
            // 
            // btn_calc
            // 
            this.btn_calc.Location = new System.Drawing.Point(124, 123);
            this.btn_calc.Name = "btn_calc";
            this.btn_calc.Size = new System.Drawing.Size(75, 23);
            this.btn_calc.TabIndex = 5;
            this.btn_calc.Text = "Calculate";
            this.btn_calc.UseVisualStyleBackColor = true;
            this.btn_calc.Click += new System.EventHandler(this.btn_calc_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(347, 158);
            this.Controls.Add(this.btn_calc);
            this.Controls.Add(this.lbl_age);
            this.Controls.Add(this.dte_date_of_birth);
            this.Controls.Add(this.lbl_select);
            this.Controls.Add(this.txt_age);
            this.Name = "Form1";
            this.Text = "Form1";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox txt_age;
        private System.Windows.Forms.Label lbl_select;
        private System.Windows.Forms.DateTimePicker dte_date_of_birth;
        private System.Windows.Forms.Label lbl_age;
        private System.Windows.Forms.Button btn_calc;
    }
}

