﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace BrendanKellyJ082400Week05ExerciseSetA
{
    public partial class Form8 : Form
    {
        public Form8()
        {
            InitializeComponent();
        }

        private void btn_calc_Click(object sender, EventArgs e)
        {
            int int_years = 0;
            decimal dec_amount_of_money = 100000;
            const decimal dec_interest_rate = 0.05M;

            while (dec_amount_of_money < 1000000)
            {
                dec_amount_of_money = dec_amount_of_money * (1 + dec_interest_rate);
                int_years++;
            }
            txt_output.Text = "It would take " + int_years + " years to become a millionaire";
        }
    }
}
