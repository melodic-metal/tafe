public class QuartsToGallons {

   public static void main(String[] args) {
   
      final int QUARTS_IN_GALLONS = 4;
      int neededPaint = 63;
      int quarts = 0;
      int gallons = 0;

      quarts = neededPaint / QUARTS_IN_GALLONS;      
      gallons = neededPaint % QUARTS_IN_GALLONS;

      System.out.print("A job that requires " + neededPaint + " quarts, needs " + quarts + " quarts and " + gallons + " gallons");
   
   }

}