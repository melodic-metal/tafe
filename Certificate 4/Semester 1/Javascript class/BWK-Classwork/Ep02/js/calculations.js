/**
 *
 *
 * Calculations in JS
 *
 *
 * @File:       /Ep02/js/calculations.js
 * @Project:    Classwork
 * @Author:     Brendan Kelly <j082400@tafe.wa.edu.au>
 * @Version:    1.0
 * @Copyright:  Brendan Kelly 2018
 *
 * History:
 * v1.0    2018-07-24
 *         Calculations and stuff
 *
 * v0.1    2018-07-24
 *         Template as provided by Adrian Gould
 *
 */
let grossPay = 0;
let hoursWorked = 12.5;
const PAY_RATE = 18.75;

grossPay = hoursWorked * PAY_RATE;

alert(grossPay);

let intNum1 = 23;
let intNum2 = 5;
let result = intNum1 / intNum2;
let remainder = intNum1 % intNum2;
console.log("num 1 / num 2 = " + result);
console.log("num1 % num 2 = " + remainder);
