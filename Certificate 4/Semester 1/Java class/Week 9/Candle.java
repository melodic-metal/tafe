// Date: 13/09/2018
// Programmer: Brendan Kelly
// Purpose: Class to hold data about candles

public class Candle {

   private static String colour;
   private static int height;
   private static double price;
   
   public static void setColour(String col) {
      colour = col;
   }
   
   public static String getColour() {
      return colour;
   }
   
   public static void setHeight(int hei) {
      height = hei;
   }  
   
   public static int getHeight() {
      return height;
   }
   

}