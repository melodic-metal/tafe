// Date: 13/09/2018
// Programmer: Brendan Kelly
// Purpose: Demo the horse classes

public class DemoHorses {

   public static void main(String[] args) {
   
      String name, colour;
      int year, races;
      RaceHorse aHorse = new RaceHorse();
      System.out.println("Setting horse stuff");
      aHorse.setName("Blackie");
      aHorse.setColour("Black");
      aHorse.setYearOfBirth(1934);
      aHorse.setRacesCompeted(4);
      name = aHorse.getName();
      colour = aHorse.getColour();
      year = aHorse.getYearOfBirth();
      races = aHorse.getRacesCompeted();
      System.out.println("The " + colour + " horse " + name + ", born in " + year + ", has competed in " + races + " races");
      
      
   }

}