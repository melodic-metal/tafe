/**
 *
 *
 * Variables in JS
 *
 *
 * @File:       /js/variables.js
 * @Project:    Classwork
 * @Author:     Brendan Kelly <j082400@tafe.wa.edu.au>
 * @Version:    1.0
 * @Copyright:  Brendan Kelly 2018
 *
 * History:
 * v1.0    2018-07-24
 *         Variables and stuff
 *
 * v0.1    2018-07-24
 *         Template as provided by Adrian Gould
 *
 */


let hoursWorked = 38;
let _test;

const TAX_RATE = 0.1;
const PAY_RATE = 12.67;
const COMPANY_NAME = "NO U INCORPORATED";

alert("Pay rate:      $" + PAY_RATE);
alert("Hours worked:  " + hoursWorked);

/*
 Assignment statements
 */
let variable = 12345;
let stringVariable = "tweet!";
let boolean = true;

console.log("variable is: " + typeof(variable))
console.log("stringVariable is: " + typeof(stringVariable))
console.log("boolean is: " + typeof(boolean))