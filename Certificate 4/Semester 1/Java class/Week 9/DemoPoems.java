// Date: 13/09/2018
// Programmer: Brendan Kelly 
// Purpose: instantiate some different types of poems, then show some information 

public class DemoPoems {

   public static void main(String[] args) {
   
      Poem poem = new Poem("Some poem", 123);
      Haiku haiku = new Haiku("a haiku");
      Couplet couplet = new Couplet("a couplet");
      Limerick limerick = new Limerick("a limerick");
      
      System.out.println("Demo poems");
      System.out.println("Limerick name: " + limerick.getName());
      System.out.println("Haiku name: " + haiku.getName());
      System.out.println("Couplet name: " + couplet.getName());
      
   }

}