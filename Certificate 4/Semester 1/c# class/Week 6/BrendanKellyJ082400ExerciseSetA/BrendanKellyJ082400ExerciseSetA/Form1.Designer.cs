﻿namespace BrendanKellyJ082400ExerciseSetA
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lst_states = new System.Windows.Forms.ListBox();
            this.btn_normal_order = new System.Windows.Forms.Button();
            this.btn_reverse_order = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // lst_states
            // 
            this.lst_states.FormattingEnabled = true;
            this.lst_states.Location = new System.Drawing.Point(80, 59);
            this.lst_states.Name = "lst_states";
            this.lst_states.Size = new System.Drawing.Size(120, 95);
            this.lst_states.TabIndex = 0;
            // 
            // btn_normal_order
            // 
            this.btn_normal_order.Location = new System.Drawing.Point(44, 193);
            this.btn_normal_order.Name = "btn_normal_order";
            this.btn_normal_order.Size = new System.Drawing.Size(75, 38);
            this.btn_normal_order.TabIndex = 1;
            this.btn_normal_order.Text = "Normal Order";
            this.btn_normal_order.UseVisualStyleBackColor = true;
            this.btn_normal_order.Click += new System.EventHandler(this.btn_normal_order_Click);
            // 
            // btn_reverse_order
            // 
            this.btn_reverse_order.Location = new System.Drawing.Point(145, 193);
            this.btn_reverse_order.Name = "btn_reverse_order";
            this.btn_reverse_order.Size = new System.Drawing.Size(75, 38);
            this.btn_reverse_order.TabIndex = 2;
            this.btn_reverse_order.Text = "Reverse Order";
            this.btn_reverse_order.UseVisualStyleBackColor = true;
            this.btn_reverse_order.Click += new System.EventHandler(this.btn_reverse_order_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(284, 261);
            this.Controls.Add(this.btn_reverse_order);
            this.Controls.Add(this.btn_normal_order);
            this.Controls.Add(this.lst_states);
            this.Name = "Form1";
            this.Text = "Form1";
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.ListBox lst_states;
        private System.Windows.Forms.Button btn_normal_order;
        private System.Windows.Forms.Button btn_reverse_order;
    }
}

