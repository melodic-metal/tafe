public class NumbersDemo {

   public static void main(String[] args) {
   
      int a = 5;
      int b = 10;
      
      
      displayNumberSquared(a, b);
      System.out.println();
      displayNumberPlusFive(a, b);
      System.out.println();
      displayTwiceTheNumber(a, b);
   }
   
   public static void displayNumberSquared(int num1, int num2) {
   
      System.out.println("Numbers squared");
      System.out.println("Number 1: " + num1 * num1 + " . Number 2: " + num2 * num2);
   
   }
   
   public static void displayNumberPlusFive(int num1, int num2) {
   
      System.out.println("Numbers plus 5");
      System.out.println("Number 1: " + (num1 + 5) + " . Number 2: " + (num2 + 5));   
   }
   
   public static void displayTwiceTheNumber(int num1, int num2) {

      System.out.println("Twice the numbers");
      System.out.println("Number 1: " + num1 * 2 + " . Number 2: " + num2 * 2);   
   }

}