﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace BrendanKellyJ082400Week12Exercises.Example
{
    public partial class frm_main : Form
    {
        public static string str_family_name;
        public static string str_given_name;
        public static int int_age;
        public frm_main()
        {
            InitializeComponent();
        }

        private void btn_display_Click(object sender, EventArgs e)
        {


            if (!int.TryParse(txt_age.Text, out int_age))
            {
                MessageBox.Show("The age entered is not a number. Please try again");
                txt_age.Clear();
                txt_age.Focus();
            }
            else
            {
                frm_display frm_show_data = new frm_display();


                // copy family name from main form to display form's name textbox
                frm_show_data.txt_family.Text = txt_family.Text;

                // place the age in the display form's public variable
                frm_show_data.int_age = int_age;

                // Display the form
                frm_show_data.ShowDialog();

                // use the Display form's changed public variable value to update the age (the variable for this form still exists)
                txt_age.Text = frm_show_data.int_age.ToString();
                // use the changed public static variable to update the given name
                txt_given.Text = str_given_name;
            }

            }
    }
}
