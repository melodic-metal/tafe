// Date: 1/09/2018
// Programmer: Brendan Kelly
// Purpose: program that instantiates a EmployeesTaxReturn object and gets the required information from the user. Lastly, it outputs the information if all information is valid.

import java.util.Scanner;

public class PrepareTax {

   public static void main(String[] args) {
      int tfn = 0, income = 0; 
      int reason = 0; // 1 for tfn, 2 for income
      String firstName ="", lastName ="", address =""; // initialise all variables
      Scanner kb = new Scanner(System.in); // set up new scanner object to receive keyboard input
      
      while(true) { 
            int isValid = 0; // set initial counter to 0
            
            System.out.print("Please enter the TFN >> ");
            tfn = kb.nextInt();
            kb.nextLine(); 
            if (Integer.toString(tfn).length() == 9) { //check that TFN is within acceptable ranges
               isValid++;
               reason = 1; 
            }
            else {
               System.out.println("TFN Not valid");
               break;
            }
            
            System.out.print("Please enter first name >> ");
            firstName = kb.nextLine();
                  
            System.out.print("Please enter last name >> ");
            lastName = kb.nextLine();
                        
            System.out.print("Please enter address >> ");
            address = kb.nextLine();
            
            System.out.print("Please enter income >> ");
            income = kb.nextInt();
            kb.nextLine();
            // ^ grab all the required information
      
            if (income >= 0) { //check that income is equal to or above 0, as someone can't have a negative income
               isValid++;
               reason = 2;
            }
            else {
               System.out.println("Income is below 0. Please check input");
               break;
            }
            
            if (isValid == 2) { // if isValid == 2, both conditions must be correct
               EmployeesTaxReturn taxReturn = new EmployeesTaxReturn(tfn, lastName, firstName, address, income); // set up object with all required information
               taxReturn.displayInformation(); // call method to display information
               break;
               }
            else {
               System.out.print("Invalid Data: ");

               
              // give a a reason why data is invalid

            }
      } 
    
   }
}
