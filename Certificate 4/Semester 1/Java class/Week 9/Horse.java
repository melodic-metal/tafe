// Date: 13/09/2018
// Programmer: Brendan Kelly
// Purpose: main horse class, with getters and setters

public class Horse {

   private static String horseName;
   private static String horseColour;
   private static int yearOfBirth;
   
   public static void setName(String name) {
      horseName = name;  
   }   
   
   public static String getName() {
      return horseName;
   }
   
   public static void setColour(String colour) {
      horseColour = colour;
   }
   
   public static String getColour() {
      return horseColour;
   }
   
   public static void setYearOfBirth(int year) {
      yearOfBirth = year;
   }
   
   public static int getYearOfBirth() {
      return yearOfBirth;
   }   
}