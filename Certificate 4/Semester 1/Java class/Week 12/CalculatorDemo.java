import java.util.Scanner;
import java.io.IOException;

public class CalculatorDemo {

   public static void main(String[] args)  throws IOException {
   
      Scanner input = new Scanner(System.in);
      Process proc = Runtime.getRuntime().exec("cmd /c c:\\Windows\\System32\\calc.exe");
      
      double num1 = 2.1;
      double num2 = 5.1;
      double answer = num1 + num2;
      double usersAnswer;
      
      System.out.println("What is the sum of " + num1 + " and " + num2 + "? >> " + answer);
      usersAnswer = input.nextDouble();
      if (usersAnswer == answer) System.out.println("Correct");
      else System.out.println("Incorrect");
      
      }

}