/*
 * AJAX3.JS
 *
 * Initialise the XML HTTP request
 *
 */

let xhr = null;
let outputZone = document.getElementById("outputZone");
let responseZone = document.getElementById("responseZone");

if (window.XMLHttpRequest || window.ActiveXObject) {
    if (window.XMLHttpRequest) {
        xhr = new XMLHttpRequest();
    }
    else {
        try {
            xhr = new ActiveXObject("Msxml2.XMLHTTP");
        }
        catch (exception) {
            xhr = new ActiveXObject("Microsoft.XMLHTTP");
        }
    }

    doAjaxRequest();
}

else {
    alert("no ajax 4 u");
}

function doAjaxRequest() {
    //xhr.open("GET", "FILE/RESOURCE", true);
    xhr.open("GET", "files/welcome.txt", true);
    xhr.send();


    xhr.onreadystatechange = function () {

        if (xhr.readyState == 0) {
            responseZone.innerHTML = "ready state 0 - not sent";
        } else if (xhr.readyState == 1) {
            responseZone.innerHTML = "ready state 1 - opened";
        } else if (xhr.readyState == 2) {
            responseZone.innerHTML = "ready state 2 - headers received";
        } else if (xhr.readyState == 3) {
            responseZone.innerHTML = "ready state 3 - loading ";
        } else {
            responseZone.innerHTML = "ready state 4 - done";
            if(xhr.status == 200) {
                responseZone.innerHTML += "<p>request status - 200 okay</p>";
                outputZone.innerText = xhr.responseText;
            }

            else if (xhr.status == 404) {
                responseZone.innerHTML += "<p>request status - 404 not found";
                outputZone.innerText = xhr.responseText;
            }
        }
    }
}