﻿// Date: 30/08/2018
// Programmer: Brendan Kelly
// Purpose: Add products to a "database" and then find products over $10

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace BrendanKellyJ082400Week07ExerciseSetB
{
    public partial class Form1 : Form
    {
        List<string> str_products = new List<string>();
        List<double> dbl_prices = new List<double>();
        public Form1()
        {
            InitializeComponent();
        }

        private void btn_accept_Click(object sender, EventArgs e)
        {
            if (!double.TryParse(txt_price.Text, out double dbl_prices_temp))
            {
                MessageBox.Show("Please enter a valid number");
            }
            else
            {
                str_products.Add(txt_name.Text);
                dbl_prices.Add(dbl_prices_temp);
            }
        }

        private void btn_price_Click(object sender, EventArgs e)
        {
            lst_output.Items.Clear();
            for(int i = 0; i < str_products.Count; i++)
            {
                if(dbl_prices[i] > 10) {
                    lst_output.Items.Add(str_products[i] + "\t" + dbl_prices[i].ToString());
                } 
                

            
            }
        }
    }
}
