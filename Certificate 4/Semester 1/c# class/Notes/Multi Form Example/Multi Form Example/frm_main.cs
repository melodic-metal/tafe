﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

// programmer   : Brian
// date         : April 2014
// purpose      : to demonstrate multi form applications aand passing data between froms


namespace Multi_Form_Example
{
    public partial class frm_main : Form
    {
        public static string str_given_name;

        public frm_main()
        {
            InitializeComponent();
        }

        private void btn_Display_form_Click(object sender, EventArgs e)
        {
            int int_in_age;

            // copy the given name into the public static variable
            str_given_name = txt_given_name.Text;

            // if an integer is not entered into the age text box
            if (!int.TryParse(txt_age.Text, out int_in_age))
            {
                // display an error message
                MessageBox.Show("The entered age is not a number - Enter it again");
                txt_age.Clear();
                txt_age.Focus();
            }
            else // create child form and transfer data
            {          
                // Declare the new display form variable and create the form object
                frm_display frm_show_data = new frm_display();

                // copy family name from main form to display form's name textbox
                frm_show_data.txt_family_name_F2.Text = txt_family_name.Text;
                
                // place the age in the display form's public variable
                frm_show_data.int_age = int_in_age;   
                
                // Display the form
                frm_show_data.ShowDialog();
                   
                // use the Display form's changed public variable value to update the age (the variable for this form still exists)
                 txt_age.Text = frm_show_data.int_age.ToString();
                // use the changed public static variable to update the given name
                 txt_given_name.Text = str_given_name;
                // Dispose of the Display form
                frm_show_data.Dispose();  // this statement is not needed as the form variable will be disposed of when the function ends after this statement
            }
        }
    }
}
