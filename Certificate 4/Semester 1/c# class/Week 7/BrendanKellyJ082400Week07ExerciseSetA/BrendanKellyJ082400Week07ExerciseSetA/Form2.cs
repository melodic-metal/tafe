﻿// Date: 30/08/2018
// Programmer: Brendan Kelly
// Purpose: Calculate Fares from Perth to Sydney via various means of transport, without a calculate button

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace BrendanKellyJ082400Week07ExerciseSetA
{
    public partial class Form2 : Form
    {
        public Form2()
        {
            InitializeComponent();
        }

        private void lst_method_SelectedIndexChanged(object sender, EventArgs e)
        {

            int int_product_index = 0;
            double dbl_cost, dbl_product_cost;
            double[] dbl_prices = { 250, 900, 400 };

            if (lst_method.SelectedIndex < 0)
            {
                MessageBox.Show("Please select a transport method", "Error");
            }
            else if (!int.TryParse(txt_passengers.Text, out int int_passengers))
            {
                MessageBox.Show("Passengers is not a valid number", "Error");
            }

            else
            {
                int_product_index = lst_method.SelectedIndex;
                dbl_product_cost = dbl_prices[int_product_index];
                dbl_cost = dbl_product_cost * int_passengers;
                txt_cost.Text = dbl_cost.ToString("C");

            }
        }

        private void btn_reset_Click(object sender, EventArgs e)
        {
            txt_cost.Clear();
            txt_passengers.Clear();
            lst_method.SelectedIndex = -1;
            txt_passengers.Focus();
        }
    }
}
