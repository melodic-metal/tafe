﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace BrendanKellyJ082400Week05ExerciseSetA
{
    public partial class Form9 : Form
    {
        public Form9()
        {
            InitializeComponent();
        }

        private void btn_calc_Click(object sender, EventArgs e)
        {
            int int_years = 0;
            float.TryParse(txt_interest.Text, out float flt_interest);
            double.TryParse(txt_investment.Text, out double dbl_investment);

            while (dbl_investment < 1000000)
            {
                dbl_investment = dbl_investment * (1 + flt_interest);
                int_years++;
            }
            txt_output.Text = "It would take " + int_years + " years to become a millionaire";
        }
    }
}
