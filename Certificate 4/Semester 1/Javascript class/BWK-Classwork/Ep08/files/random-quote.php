<?php
/**
 * Quick and Dirty Random Quote Generator
 *
 * @File:       /files/random-quote.php
 * @Project:    AJG-Classwork
 * @Author:     Adrian.Gould<adrian.gould@nmtafe.wa.edu.au>
 * @Version:    1.0
 * @Copyright:  Adrian Gould 2018
 *
 * History:
 *
 *  v1.1    DD/MM/YYYY
 *          Add version details here
 *
 *  v1.0    04/09/2018
 *          Version details
 *
 *  v0.0    2018-05-08
 *          Template as provided by Adrian Gould
 */

$quotes =
    [
        [
            "quote" => "Every man is born as many men and dies as a single one. ",
            "quoteBy" => "Martin Heidegger"
        ],
        [
            "quote" => "Language is the house of the truth of Being. ",
            "quoteBy" => "Martin Heidegger"
        ],
        [
            "quote" => "Man acts as though he were the shaper and master of language,\nwhile in fact language remains the master of man. ",
            "quoteBy" => "Martin Heidegger"
        ],
        [
            "quote" => "The most thought-provoking thing in our thought-provoking time is that we are still not thinking. ",
            "quoteBy" => "Martin Heidegger"
        ],
        [
            "quote" => "The possible ranks higher than the actual. ",
            "quoteBy" => "Martin Heidegger"
        ],
        [
            "quote" => "Unless you change how you are,\nyou will always have what you've got. ",
            "quoteBy" => "Jim Rohn"
        ],
        [
            "quote" => "A stumble may prevent a fall. ",
            "quoteBy" => "English Proverb"
        ],
        [
            "quote" => "There's no limit to what a man can achieve,\nif he doesn't care who gets the credit. ",
            "quoteBy" => "Laing Burns, Jr."
        ],
        [
            "quote" => "Don't waste yourself in rejection, \nnor bark against the bad, \nbut chant the beauty of the good.",
            "quoteBy" => "Ralph Waldo Emerson"
        ],
        [
            "quote" => "The most practical, beautiful, workable philosophy in the world won't work - if you won't. ",
            "quoteBy" => "Zig Ziglar"
        ],
        [
            "quote" => "I believe the greater the handicap, \nthe greater the triumph. ",
            "quoteBy" => "John H. Johnson"
        ],
        [
            "quote" => "Life shrinks or expands in proportion to one's courage. ",
            "quoteBy" => "Anais Nin"
        ],
        [
            "quote" => "We don't see things as they are we see them as we are. ",
            "quoteBy" => "Anais Nin"
        ],
        [
            "quote" => "You can't build a reputation on what you are going to do. ",
            "quoteBy" => "Henry Ford"
        ],
        [
            "quote" => "The greatest form of maturity is at harvest time.\nThat is when we must learn how to reap without complaint if the amounts are small and how to to reap without apology if the amounts are big. ",
            "quoteBy" => "Jim Rohn"
        ],
        [
            "quote" => "People seem not to see that their opinion of the world is also a confession of character. ",
            "quoteBy" => "Ralph Waldo Emerson"
        ],
        [
            "quote" => "The most successful people are those who are good at plan B. ",
            "quoteBy" => "James Yorke"
        ],
        [
            "quote" => "Opportunity is missed by most because it is dressed in overalls and looks like work. ",
            "quoteBy" => "Thomas Alva Edison"
        ],
        [
            "quote" => "The universe is full of magical things, patiently waiting for our wits to grow sharper. ",
            "quoteBy" => "Eden Phillpotts"
        ],
        [
            "quote" => "Experience is not what happens to a man, it is what a man does with what happens to him. ",
            "quoteBy" => "Aldous Huxley"
        ],
        [
            "quote" => "Imagination rules the world. ",
            "quoteBy" => "Napoleon Bonaparte"
        ],
        [
            "quote" => "Adversity has the effect of eliciting talents which, in prosperous circumstances, would have lain dormant. ",
            "quoteBy" => "Horace"
        ],
        [
            "quote" => "It isn't that they can't see the solution, it's that they can't see the problem. ",
            "quoteBy" => "G.K. Chesterton"
        ],
        [
            "quote" => "Facts are stubborn, but statistics are more pliable. ",
            "quoteBy" => "Mark Twain"
        ],
        [
            "quote" => "All truth goes through three steps:  First, it is ridiculed.\nSecond, it is violently opposed.\nFinally, it is accepted as self-evident. ",
            "quoteBy" => "Arthur Schopenhauer"
        ],
        [
            "quote" => "An invasion of armies can be resisted; an invasion of ideas cannot be resisted. ",
            "quoteBy" => "Victor Hugo"
        ],
        [
            "quote" => "Pain is inevitable but misery is optional. ",
            "quoteBy" => "Barbara Johnson"
        ],
        [
            "quote" => "Beware of defining as intelligent only those who share your opinions. ",
            "quoteBy" => "Ugo Ojetti"
        ],
        [
            "quote" => "If we knew what it was we were doing, it would not be called research, would it? ",
            "quoteBy" => "Albert Einstein"
        ],
        [
            "quote" => "To believe a thing is impossible is to make it so. ",
            "quoteBy" => "French proverb"
        ],
        [
            "quote" => "Simplicity is the ultimate sophistication. ",
            "quoteBy" => "Leonardo da Vinci"
        ],
        [
            "quote" => "To be simple is to be great. ",
            "quoteBy" => "Ralph Waldo Emerson"
        ],
        [
            "quote" => "The trouble about man is twofold. He cannot learn truths which are too complicated; he forgets truths which are too simple. ",
            "quoteBy" => "Dame Rebecca West"
        ],
        [
            "quote" => "Everything should be as simple as it is, but not simpler. ",
            "quoteBy" => "Albert Einstein"
        ],
        [
            "quote" => "That you may retain your self-respect, it is better to displease the people by doing what you know is right, than to temporarily please them by doing what you know is wrong. ",
            "quoteBy" => "William J. H.\nBoetcker"
        ],
        [
            "quote" => "Many of life's failures are people who did not realize how close they were to success when they gave up. ",
            "quoteBy" => "Thomas Edison"
        ],
        [
            "quote" => "Hitch your wagon to a star. ",
            "quoteBy" => "Ralph Waldo Emerson"
        ],
        [
            "quote" => "If you knew how much work went into it, you wouldn't call it genius. ",
            "quoteBy" => "Michelangelo"
        ],
        [
            "quote" => "I know God will not give me anything I can't handle.\nI just wish that He didn't trust me so much. ",
            "quoteBy" => "Mother Teresa"
        ],
        [
            "quote" => "If we did the things we are capable of, we would astound ourselves. ",
            "quoteBy" => "Thomas Edison"
        ],

        [
            "quote" => "...I was bown in a cwossfiwe huwwicane",
            "quoteBy" => "Jumpin Jack Fudd"
        ],
        [
            "quote" => "\". evewy giwls cwazy 'bout a baldheaded man . .\"",
            "quoteBy" => "ZZ Fudd"
        ],
        [
            "quote" => "\"Ah'm just a singew in a wock and woll band\"",
            "quoteBy" => "Moody Fudds"
        ],
        [
            "quote" => "\"Jesus just weft Chicago, and he's bound for New Oweans\"",
            "quoteBy" => "ZZ Fudd"
        ],
        [
            "quote" => "\"Wide, Captain, Wide, on youw mystewy ship....\"",
            "quoteBy" => "E.Fudd"
        ],
        [
            "quote" => "\"... aww my wowdy fwiends have settwed down ...\"",
            "quoteBy" => "Hank Fudd, Jr."
        ],
        [
            "quote" => "\"Entew night, entew wight, off to nevew, nevew wand\"",
            "quoteBy" => " Fuddalica"
        ],
        [
            "quote" => "\" wunnin', *wunnin' with the devil*, cause I'm wunnin' \"",
            "quoteBy" => "Van Fudd"
        ],
        [
            "quote" => "\"My memowy has been sowd, my angew is a centewfowd!\"",
            "quoteBy" => "J.\nFudd Band"
        ],
        [
            "quote" => "\"...She don't wide, she don't wide.......cocaine.\"",
            "quoteBy" => " Elmer Clapton"
        ],
        [
            "quote" => "\"dweam weavew, I bewieve you can get me thwough .\"",
            "quoteBy" => "Elmew Wwight"
        ],
        [
            "quote" => "\"Swells wike teen spiwit\"",
            "quoteBy" => "Fuddwana"
        ],
        [
            "quote" => "\"Widdle miss widdle miss caaan't be wong...\"",
            "quoteBy" => "Fudd Doctors"
        ],
        [
            "quote" => "\"I just want to fowwwget you\"",
            "quoteBy" => "Kudd"
        ],
        [
            "quote" => "Exit light, entew night, howdn my hand, off to nevew-nevew wand",
            "quoteBy" => "Fuddalica"
        ],
        [
            "quote" => "Thank God Almighty, I'm fwee at wast! ",
            "quoteBy" => " M. L.\nFudd Jr."
        ],
        [
            "quote" => "\"... *hot*, *bwuuuuuue*, and *wiiiiiighteous* ...\"",
            "quoteBy" => "ZZ Fudd"
        ],
        [
            "quote" => "\"Be vewy, vewy kwiet.\nI'm hunting twagwines!\"",
            "quoteBy" => "Elmer Fudd"
        ],
        [
            "quote" => "\"I didn't shoot Pwezident Kennedy!\"  Fudd Harvey Oswald"
        ],
        [
            "quote" => "\"... the vetewan cosmic rockew\"",
            "quoteBy" => " Moody Fudds"
        ],
        [
            "quote" => "\"Knights in white satin, nevew weaching the end...\"",
            "quoteBy" => " Moody Fudds"
        ],
        [
            "quote" => "\"... tip-toe thwough the tuwips ...\"",
            "quoteBy" => "Tiny Fudd"
        ],
        [
            "quote" => "(c) Copywight 1992 Elmer Fudd .\nAll wights wesewved . "
        ],
        [
            "quote" => "\"Wet it woll . down the highway!  Woll! .\"",
            "quoteBy" => "Bachman-Fudd Overdrive"
        ],
        [
            "quote" => "\"To Bwe or not to bwe, thwat wis the qwestion\"",
            "quoteBy" => "\"Fuddlet"
        ],
        [
            "quote" => "\"Joween\"",
            "quoteBy" => " Dowwy Fudd"
        ],
        [
            "quote" => "\"I'w nevew be youw beast of buwden....\"",
            "quoteBy" => "Rolling Fudds"
        ],
        [
            "quote" => "\"A weal Amewican Hewo, GI Joe is thewe...\"",
            "quoteBy" => " G.I. Elmer"
        ],
        [
            "quote" => "I Woke up this mownin, and I got myself a beew\"\"",
            "quoteBy" => "The Fudds"
        ],
        [
            "quote" => "\"I weaw my sungwasses at night...\"",
            "quoteBy" => " Cory Fudd"
        ],
        [
            "quote" => "I'm a wocket mannnn ... ",
            "quoteBy" => "Elton Fudd"
        ],
        [
            "quote" => "...Wollin', Wollin', Wollin' on a wivew.......",
            "quoteBy" => "Tina Fudd"
        ],
        [
            "quote" => "... thewe's a bad moon on de wise ...\"",
            "quoteBy" => "Elmer Fudd Revival"
        ],
        [
            "quote" => "\"in the mowning to the awawm cwocks wawning\"",
            "quoteBy" => "Bachman-Fudd Overdrive"
        ],
        [
            "quote" => "\"Youw Ten Miwwion dowwar check couwd be in de maiw!\"",
            "quoteBy" => "Elmer McMahon"
        ],
        [
            "quote" => "Come out wittle bunny wabbit (Blam) Gun controwl dat!",
            "quoteBy" => "Elton Fudd"
        ],
        [
            "quote" => "\"Wolling, wolling, wolling, keep them doggies wolling\" Frankie Fudd"
        ],
        [
            "quote" => "\"I know it's only wock and woll...\"",
            "quoteBy" => "Wolling Fudds"
        ],
        [
            "quote" => "\"Wemonaid, that kwool wefweshing dwink\"",
            "quoteBy" => "Eddie Murphy/Elvis Fudd"
        ],
        [
            "quote" => "\"Aww we aww is dust in the wind...\"",
            "quoteBy" => "Fuddsas"
        ],
        [
            "quote" => "Twansfowmews - wobots in disguise! \"",
            "quoteBy" => "Optimus Fudd"
        ],
        [
            "quote" => "Way, waydy, Way, acwoss my big bwass bed.\"",
            "quoteBy" => "Bob \"FUD\" Dywan"
        ],
        [
            "quote" => "\"I know it's only wock and woll...\"",
            "quoteBy" => "Wolling Fudds"
        ],
        [
            "quote" => "\"It don't mattew if you're bwack or white\"",
            "quoteBy" => "Fudd Jackson"
        ],
        [
            "quote" => "\"...and the colowed giwl goes dotodoo todo do\"",
            "quoteBy" => "Lou Fudd"
        ],
        [
            "quote" => "\"Wock and Woll Hootchie-coo\"",
            "quoteBy" => "Wick Fuddingew"
        ],
        [
            "quote" => "\"Wockin' Wobin, tweet tweet, Wockin Wobin..\"",
            "quoteBy" => "Fuddson 5"
        ],
        [
            "quote" => "\"... come on and take a fwee wide ...\"",
            "quoteBy" => "Elmer Winters Group"
        ],
        [
            "quote" => "\"She's got a thing dat's cawwed . .\nWadaw wove.\"",
            "quoteBy" => "Golden Fuddring"
        ],
        [
            "quote" => "\"We got a thing that's cawwed wadar wove...\"!White Fudd",
            "quoteBy" => "Elton Fudd"
        ],
        [
            "quote" => "\"We'we tiny, we'we toony, we'we aww a wittwl woony...\"",
            "quoteBy" => "Tiny Fudds"
        ],
        [
            "quote" => "...just a sweet twasnvestite fwom Twansexual, Twansywvania\"",
            "quoteBy" => "Fuddnfurter"
        ],
        [
            "quote" => "\"... the vetewan cosmic rockew\"",
            "quoteBy" => "Moody Fudds"
        ],
        [
            "quote" => "\"don't teww my heawt, my achy-bweaky heawt .\"",
            "quoteBy" => "Billy Ray Fudd"
        ],
        [
            "quote" => "\"stowmtwoopew's coming and you bettew get pwepawed\"",
            "quoteBy" => "Elmer Nugent"
        ],
        [
            "quote" => "\"I'ww be a foow fow youw loving no mowe\"",
            "quoteBy" => " Whitefudd"
        ],
        [
            "quote" => "\"Secwet Agent Man...they've given you a numbew...\"",
            "quoteBy" => "Johnny Fudders"
        ],
        [
            "quote" => "\"And He shaww weign fowevew and evew.\"",
            "quoteBy" => "Georg Fudderick Handel"
        ],
        [
            "quote" => "\"Spwing Song\"",
            "quoteBy" => "Fuddix Mendelssohn-Bartholdy"
        ],
        [
            "quote" => "\"Jingwe Bewws\"",
            "quoteBy" => "J.\nFudd Pierpont"
        ],
        [
            "quote" => "\"Hawk, the Hewawd Angews Sing\"",
            "quoteBy" => "Fuddix Mendelssohn-Bartholdy"
        ],
        [
            "quote" => "\"The Hebwides (Fingaw's Cave)\"",
            "quoteBy" => "Fuddix Mendelssohn-Bartholdy"
        ],
        [
            "quote" => "\"I Wove Thee\"",
            "quoteBy" => "EdFudd Grieg"
        ],
        [
            "quote" => "\"In the Hawwl Of the Mountain King\"",
            "quoteBy" => "EdFudd Grieg"
        ],
        [
            "quote" => "\"Finwandia\"",
            "quoteBy" => "Fudd Sibelius"
        ],
        [
            "quote" => "\"Piwgwims' Chowus\"",
            "quoteBy" => "RichFudd Wagner"
        ],
        [
            "quote" => "\"My Heawt Is Evew Faithfuw\"",
            "quoteBy" => "JoFudd Sebastian Bach"
        ],
        [
            "quote" => "\"Wuwwaby\"",
            "quoteBy" => "JoFuddes Brahms"
        ],
        [
            "quote" => "\"Sewenade\"",
            "quoteBy" => "Fudd Schubert"
        ],
        [
            "quote" => "\"The Mawwiage Of Figawo\"",
            "quoteBy" => "WolfFudd Amadeus Mozart"
        ],
        [
            "quote" => "\"By the Beautifuw Bwue Danube\"",
            "quoteBy" => "JoFudd Strauss, Jr."
        ],
        [
            "quote" => "\"Wove Fow Thwee Owanges\"",
            "quoteBy" => "SerFudd Prokofiev"
        ],
        [
            "quote" => "\"Vocawise\"",
            "quoteBy" => "SerFudd Rachmaninov"
        ],
        [
            "quote" => "Tuwkish Mawch\"",
            "quoteBy" => "Ludwig Fudd Beethoven"
        ],
        [
            "quote" => "\" ... dis is just what de doctow owdewed!\"",
            "quoteBy" => "Elmer Nugent"
        ],
        [
            "quote" => "\"... onwy the stwong suwvive ...\"",
            "quoteBy" => "REO Fuddwagon"
        ],
        [
            "quote" => "\"...Wock de boat, don't wock de boat, baby ...\"",
            "quoteBy" => "The Fudd Corp."
        ],
        [
            "quote" => "\"Tuwn, Tuwn, Tuwn\"...",
            "quoteBy" => "Bachman-Fudd Overdrive"
        ],
        [
            "quote" => "I need a lovew that won't dwive me cwazy ",
            "quoteBy" => "Elmer Cougar"
        ],
        [
            "quote" => "...Dweam On, Dweam until youw dweams come twue!",
            "quoteBy" => "AewoFudd"
        ],
        [
            "quote" => "Stop dwaggin' my heawt awound... ",
            "quoteBy" => "Stevie Fudds/Elmer Petty"
        ],
        [
            "quote" => "I need a lovew that won't dwive me cwazy",
            "quoteBy" => " Fudd Benataw"
        ],
        [
            "quote" => "...Wike pigs on de wing ...\"",
            "quoteBy" => "Pink Fudd"
        ],
        [
            "quote" => "\" Bown fweeeee.... as fwee as the wind bwows \"",
            "quoteBy" => " Fuddy Williams þ"
        ],
        [
            "quote" => "Woxanne! you don't cawe if it's wwong ow it's wight.",
            "quoteBy" => "Wox"
        ],
        [
            "quote" => "... don't you know dat you awe a shooting staw ...\" Fudd Company"
        ],
        [
            "quote" => "Geowgia, Geowgia, you'we awways on my mind ...\"",
            "quoteBy" => "Ray Fudd"
        ],
        [
            "quote" => "It bweaks my heawt to see those staws....",
            "quoteBy" => "Elmer Hyatt"
        ],
        [
            "quote" => "... cest wa vie, when youw weaves aww tuwn to bwown",
            "quoteBy" => "Emerson, Fudd & Palmer"
        ],
        [
            "quote" => "...Smashing a pewfectwy goooood guitaw! ",
            "quoteBy" => "Elmer Hyatt"
        ],
        [
            "quote" => "Stop dwaggin' my heawt awound... ",
            "quoteBy" => "Stevie Fudds/Elmer Petty"
        ],
        [
            "quote" => "Shhh! be vewy quiet. I'm hunting womulans.",
            "quoteBy" => "Elton Fudd"
        ],


        [
            "quote" => "Life isn't about getting and having, it's about giving and being.",
            "quoteBy" => "Kevin Kruse"
        ],
        [
            "quote" => "Whatever the mind of man can conceive and believe, it can achieve.",
            "quoteBy" => "Napoleon Hill"
        ],
        [
            "quote" => "Strive not to be a success, but rather to be of value.",
            "quoteBy" => "Albert Einstein"
        ],
        [
            "quote" => "Two roads diverged in a wood, and I—I took the one less traveled by, And that has made all the difference.",
            "quoteBy" => "Robert Frost"
        ],
        [
            "quote" => "I attribute my success to this: I never gave or took any excuse.",
            "quoteBy" => "Florence Nightingale"
        ],
        [
            "quote" => "You miss 100% of the shots you don't take.",
            "quoteBy" => "Wayne Gretzky"
        ],
        [
            "quote" => "I've missed more than 9000 shots in my career.\nI've lost almost 300 games.\n26 times I've been trusted to take the game winning shot and missed.\nI've failed over and over and over again in my life.\nAnd that is why I succeed.",
            "quoteBy" => "Michael Jordan"
        ],
        [
            "quote" => "The most difficult thing is the decision to act, the rest is merely tenacity.",
            "quoteBy" => "Amelia Earhart"
        ],
        [
            "quote" => "Every strike brings me closer to the next home run.",
            "quoteBy" => "Babe Ruth"
        ],
        [
            "quote" => "Definiteness of purpose is the starting point of all achievement.",
            "quoteBy" => "W. Clement Stone"
        ],
        [
            "quote" => "We must balance conspicuous consumption with conscious capitalism.",
            "quoteBy" => "Kevin Kruse"
        ],
        [
            "quote" => "Life is what happens to you while you're busy making other plans.",
            "quoteBy" => "John Lennon"
        ],
        [
            "quote" => "We become what we think about.",
            "quoteBy" => "Earl Nightingale"
        ],
        [
            "quote" => "14. Twenty years from now you will be more disappointed by the things that you didn't do than by the ones you did do, so throw off the bowlines, sail away from safe harbor, catch the trade winds in your sails. Explore, Dream, Discover.",
            "quoteBy" => "Mark Twain"
        ],
        [
            "quote" => "15. Life is 10% what happens to me and 90% of how I react to it.",
            "quoteBy" => "Charles Swindoll"
        ],
        [
            "quote" => "The most common way people give up their power is by thinking they don't have any.",
            "quoteBy" => "Alice Walker"
        ],
        [
            "quote" => "The mind is everything.\nWhat you think you become.",
            "quoteBy" => "Buddha"
        ],
        [
            "quote" => "The best time to plant a tree was 20 years ago.\nThe second best time is now.",
            "quoteBy" => "Chinese Proverb"
        ],
        [
            "quote" => "An unexamined life is not worth living.",
            "quoteBy" => "Socrates"
        ],
        [
            "quote" => "Eighty percent of success is showing up.",
            "quoteBy" => "Woody Allen"
        ],
        [
            "quote" => "Your time is limited, so don't waste it living someone else's life.",
            "quoteBy" => "Steve Jobs"
        ],
        [
            "quote" => "Winning isn't everything, but wanting to win is.",
            "quoteBy" => "Vince Lombardi"
        ],
        [
            "quote" => "I am not a product of my circumstances.\nI am a product of my decisions.",
            "quoteBy" => "Stephen Covey"
        ],
        [
            "quote" => "Every child is an artist.\nThe problem is how to remain an artist once he grows up.",
            "quoteBy" => "Pablo Picasso"
        ],
        [
            "quote" => "You can never cross the ocean until you have the courage to lose sight of the shore.",
            "quoteBy" => "Christopher Columbus"
        ],
        [
            "quote" => "I've learned that people will forget what you said, people will forget what you did, but people will never forget how you made them feel.",
            "quoteBy" => "Maya Angelou"
        ],
        [
            "quote" => "Either you run the day, or the day runs you.",
            "quoteBy" => "Jim Rohn"
        ],
        [
            "quote" => "Whether you think you can or you think you can't, you're right.",
            "quoteBy" => "Henry Ford"
        ],
        [
            "quote" => "The two most important days in your life are the day you are born and the day you find out why.",
            "quoteBy" => "Mark Twain"
        ],
        [
            "quote" => "Whatever you can do, or dream you can, begin it.\nBoldness has genius, power and magic in it.",
            "quoteBy" => "Johann Wolfgang von Goethe"
        ],
        [
            "quote" => "The best revenge is massive success.",
            "quoteBy" => "Frank Sinatra"
        ],
        [
            "quote" => "People often say that motivation doesn't last.\nWell, neither does bathing.\nThat's why we recommend it daily.",
            "quoteBy" => "Zig Ziglar"
        ],
        [
            "quote" => "Life shrinks or expands in proportion to one's courage.",
            "quoteBy" => "Anais Nin"
        ],
        [
            "quote" => "If you hear a voice within you say “you cannot paint,” then by all means paint and that voice will be silenced.",
            "quoteBy" => "Vincent Van Gogh"
        ],
        [
            "quote" => "There is only one way to avoid criticism: do nothing, say nothing, and be nothing.",
            "quoteBy" => "Aristotle"
        ],
        [
            "quote" => "Ask and it will be given to you; search, and you will find; knock and the door will be opened for you.",
            "quoteBy" => "Jesus"
        ],
        [
            "quote" => "The only person you are destined to become is the person you decide to be.",
            "quoteBy" => "Ralph Waldo Emerson"
        ],
        [
            "quote" => "Go confidently in the direction of your dreams. Live the life you have imagined.",
            "quoteBy" => "Henry David Thoreau"
        ],
        [
            "quote" => "When I stand before God at the end of my life, I would hope that I would not have a single bit of talent left and could say, I used everything you gave me.",
            "quoteBy" => "Erma Bombeck"
        ],
        [
            "quote" => "Few things can help an individual more than to place responsibility on him, and to let him know that you trust him.",
            "quoteBy" => "Booker T.\nWashington"
        ],
        [
            "quote" => "Certain things catch your eye, but pursue only those that capture the heart.",
            "quoteBy" => " Ancient Indian Proverb"
        ],
        [
            "quote" => "Believe you can and you're halfway there.",
            "quoteBy" => "Theodore Roosevelt"
        ],
        [
            "quote" => "Everything you've ever wanted is on the other side of fear.",
            "quoteBy" => "George Addair"
        ],
        [
            "quote" => "We can easily forgive a child who is afraid of the dark; the real tragedy of life is when men are afraid of the light.",
            "quoteBy" => "Plato"
        ],
        [
            "quote" => "Teach thy tongue to say, “I do not know,” and thous shalt progress.",
            "quoteBy" => "Maimonides"
        ],
        [
            "quote" => "Start where you are. Use what you have. Do what you can.",
            "quoteBy" => "Arthur Ashe"
        ],
        [
            "quote" => "When I was 5 years old, my mother always told me that happiness was the key to life. When I went to school, they asked me what I wanted to be when I grew up. I wrote down 'happy'.\nThey told me I didn't understand the assignment, and I told them they didn't understand life.",
            "quoteBy" => "John Lennon"
        ],
        [
            "quote" => "Fall seven times and stand up eight.",
            "quoteBy" => "Japanese Proverb"
        ],
        [
            "quote" => "When one door of happiness closes, another opens, but often we look so long at the closed door that we do not see the one that has been opened for us.",
            "quoteBy" => "Helen Keller"
        ],
        [
            "quote" => "Everything has beauty, but not everyone can see.",
            "quoteBy" => "Confucius"
        ],
        [
            "quote" => "How wonderful it is that nobody need wait a single moment before starting to improve the world.",
            "quoteBy" => "Anne Frank"
        ],
        [
            "quote" => "When I let go of what I am, I become what I might be.",
            "quoteBy" => "Lao Tzu"
        ],
        [
            "quote" => "Life is not measured by the number of breaths we take, but by the moments that take our breath away.",
            "quoteBy" => "Maya Angelou"
        ],
        [
            "quote" => "Happiness is not something readymade. It comes from your own actions.",
            "quoteBy" => "Dalai Lama"
        ],
        [
            "quote" => "If you're offered a seat on a rocket ship, don't ask what seat! Just get on.",
            "quoteBy" => "Sheryl Sandberg"
        ],
        [
            "quote" => "First, have a definite, clear practical ideal; a goal, an objective.\nSecond, have the necessary means to achieve your ends; wisdom, money, materials, and methods.\nThird, adjust all your means to that end.",
            "quoteBy" => "Aristotle"
        ],
        [
            "quote" => "If the wind will not serve, take to the oars.",
            "quoteBy" => "Latin Proverb"
        ],
        [
            "quote" => "You can't fall if you don't climb.\nBut there's no joy in living your whole life on the ground.",
            "quoteBy" => "Unknown"
        ],
        [
            "quote" => "We must believe that we are gifted for something, and that this thing, at whatever cost, must be attained.",
            "quoteBy" => "Marie Curie"
        ],
        [
            "quote" => "Too many of us are not living our dreams because we are living our fears.",
            "quoteBy" => "Les Brown"
        ],
        [
            "quote" => "Challenges are what make life interesting and overcoming them is what makes life meaningful.",
            "quoteBy" => "Joshua J.\nMarine"
        ],
        [
            "quote" => "If you want to lift yourself up, lift up someone else.",
            "quoteBy" => "Booker T.\nWashington"
        ],
        [
            "quote" => "I have been impressed with the urgency of doing.\nKnowing is not enough; we must apply.\nBeing willing is not enough; we must do.",
            "quoteBy" => "Leonardo da Vinci"
        ],
        [
            "quote" => "Limitations live only in our minds.\nBut if we use our imaginations, our possibilities become limitless.",
            "quoteBy" => "Jamie Paolinetti"
        ],
        [
            "quote" => "You take your life in your own hands, and what happens? A terrible thing, no one to blame.",
            "quoteBy" => "Erica Jong"
        ],
        [
            "quote" => "What's money? A man is a success if he gets up in the morning and goes to bed at night and in between does what he wants to do.",
            "quoteBy" => "Bob Dylan"
        ],
        [
            "quote" => "I didn't fail the test.\nI just found 100 ways to do it wrong.",
            "quoteBy" => "Benjamin Franklin"
        ],
        [
            "quote" => "In order to succeed, your desire for success should be greater than your fear of failure.",
            "quoteBy" => "Bill Cosby"
        ],
        [
            "quote" => "A person who never made a mistake never tried anything new.",
            "quoteBy" => " Albert Einstein"
        ],
        [
            "quote" => "The person who says it cannot be done should not interrupt the person who is doing it.",
            "quoteBy" => "Chinese Proverb"
        ],
        [
            "quote" => "There are no traffic jams along the extra mile.",
            "quoteBy" => "Roger Staubach"
        ],
        [
            "quote" => "It is never too late to be what you might have been.",
            "quoteBy" => "George Eliot"
        ],
        [
            "quote" => "You become what you believe.",
            "quoteBy" => "Oprah Winfrey"
        ],
        [
            "quote" => "I would rather die of passion than of boredom.",
            "quoteBy" => "Vincent van Gogh"
        ],
        [
            "quote" => "A truly rich man is one whose children run into his arms when his hands are empty.",
            "quoteBy" => "Unknown"
        ],
        [
            "quote" => "It is not what you do for your children, but what you have taught them to do for themselves, that will make them successful human beings.",
            "quoteBy" => "Ann Landers"
        ],
        [
            "quote" => "If you want your children to turn out well, spend twice as much time with them, and half as much money.",
            "quoteBy" => "Abigail Van Buren"
        ],
        [
            "quote" => "Build your own dreams, or someone else will hire you to build theirs.",
            "quoteBy" => "Farrah Gray"
        ],
        [
            "quote" => "The battles that count aren't the ones for gold medals.\nThe struggles within yourself–the invisible battles inside all of us–that's where it's at.",
            "quoteBy" => "Jesse Owens"
        ],
        [
            "quote" => "Education costs money.\nBut then so does ignorance.",
            "quoteBy" => "Sir Claus Moser"
        ],
        [
            "quote" => "I have learned over the years that when one's mind is made up, this diminishes fear.",
            "quoteBy" => "Rosa Parks"
        ],
        [
            "quote" => "It does not matter how slowly you go as long as you do not stop.",
            "quoteBy" => "Confucius"
        ],
        [
            "quote" => "If you look at what you have in life, you'll always have more.\nIf you look at what you don't have in life, you'll never have enough.",
            "quoteBy" => "Oprah Winfrey"
        ],
        [
            "quote" => "Remember that not getting what you want is sometimes a wonderful stroke of luck.",
            "quoteBy" => "Dalai Lama"
        ],
        [
            "quote" => "You can't use up creativity.\nThe more you use, the more you have.",
            "quoteBy" => "Maya Angelou"
        ],
        [
            "quote" => "Dream big and dare to fail.",
            "quoteBy" => "Norman Vaughan"
        ],
        [
            "quote" => "Our lives begin to end the day we become silent about things that matter.",
            "quoteBy" => "Martin Luther King Jr."
        ],
        [
            "quote" => "Do what you can, where you are, with what you have.",
            "quoteBy" => "Teddy Roosevelt"
        ],
        [
            "quote" => "If you do what you've always done, you'll get what you've always gotten.",
            "quoteBy" => "Tony Robbins"
        ],
        [
            "quote" => "Dreaming, after all, is a form of planning.",
            "quoteBy" => "Gloria Steinem"
        ],
        [
            "quote" => "It's your place in the world; it's your life. Go on and do all you can with it, and make it the life you want to live.",
            "quoteBy" => "Mae Jemison"
        ],
        [
            "quote" => "You may be disappointed if you fail, but you are doomed if you don't try.",
            "quoteBy" => "Beverly Sills"
        ],
        [
            "quote" => "Remember no one can make you feel inferior without your consent.",
            "quoteBy" => "Eleanor Roosevelt"
        ],
        [
            "quote" => "Life is what we make it, always has been, always will be.",
            "quoteBy" => "Grandma Moses"
        ],
        [
            "quote" => "The question isn't who is going to let me; it's who is going to stop me.",
            "quoteBy" => "Ayn Rand"
        ],
        [
            "quote" => "When everything seems to be going against you, remember that the airplane takes off against the wind, not with it.",
            "quoteBy" => "Henry Ford"
        ],
        [
            "quote" => "It's not the years in your life that count.\nIt's the life in your years.",
            "quoteBy" => "Abraham Lincoln"
        ],
        [
            "quote" => "Change your thoughts and you change your world.",
            "quoteBy" => "Norman Vincent Peale"
        ],
        [
            "quote" => "Either write something worth reading or do something worth writing.",
            "quoteBy" => "Benjamin Franklin"
        ],
        [
            "quote" => "Nothing is impossible, the word itself says, “I'm possible!”",
            "quoteBy" => "–Audrey Hepburn"
        ],
        [
            "quote" => "The only way to do great work is to love what you do.",
            "quoteBy" => "Steve Jobs"
        ],
        [
            "quote" => "If you can dream it, you can achieve it.",
            "quoteBy" => "Zig Ziglar"
        ],

        [
            "quote" => "The only people who never fail are those who never try.",
            "quoteBy" => "Ilka Chase"
        ],
        [
            "quote" => "Failure is just another way to learn how to do something right.",
            "quoteBy" => "Marian Wright Edelman"
        ],
        [
            "quote" => "I failed my way to success.",
            "quoteBy" => "Thomas Edison"
        ],
        [
            "quote" => "Every failure brings with it the seed of an equivalent success.",
            "quoteBy" => "Napoleon Hill"
        ],
        [
            "quote" => "Only those who dare to fail greatly can ever achieve greatly.",
            "quoteBy" => "John F.\nKennedy"
        ],
        [
            "quote" => "It is hard to fail, but it is worse never to have tried to succeed.",
            "quoteBy" => "Theodore Roosevelt"
        ],
        [
            "quote" => "Imagination is more important than knowledge.",
            "quoteBy" => "Albert Einstein"
        ],
        [
            "quote" => "Hold fast to dreams, for if dreams die, life is a broken winged bird that cannot fly.",
            "quoteBy" => "Langston Hughes"
        ],
        [
            "quote" => "The future belongs to those who believe in the beauty of their dreams.",
            "quoteBy" => "Eleanor Roosevelt"
        ],
        [
            "quote" => "Go confidently in the direction of your dreams. Live the life you have imagined.",
            "quoteBy" => "Henry David Thoreau"
        ],
        [
            "quote" => "You cannot depend on your eyes when your imagination is out of focus.",
            "quoteBy" => "Mark Twain"
        ],
        [
            "quote" => "Commitment leads to action.\nAction brings your dream closer.",
            "quoteBy" => "Marcia Wieder"
        ],
        [
            "quote" => "I never think of the future",
            "quoteBy" => "it comes soon enough.\""
        ],
        [
            "quote" => "Don't waste your life in doubts and fears: Spend yourself on the work before you, well assured that the right performance of this hour's duties will be the best preparation for the hours or ages that follow it.",
            "quoteBy" => "Ralph Waldo Emerson"
        ],
        [
            "quote" => "Do not anticipate trouble or worry about what may never happen.\nKeep in the sunlight.",
            "quoteBy" => "Benjamin Franklin"
        ],
        [
            "quote" => "Take time to deliberate; but when the time for action arrives, stop thinking and go in.",
            "quoteBy" => "Andrew Jackson"
        ],
        [
            "quote" => "The mind that is anxious about future events is miserable.",
            "quoteBy" => "Seneca"
        ],
        [
            "quote" => "Live in each season as it passes; breathe the air, drink the drink, taste the fruit, and resign yourself to the influences of each.",
            "quoteBy" => "Henry David Thoreau"
        ],
        [
            "quote" => "The art of leadership is saying no, not yes.\nIt is very easy to say yes.",
            "quoteBy" => "Tony Blair"
        ],
        [
            "quote" => "A leader is a dealer in hope.",
            "quoteBy" => "Napoleon Bonaparte"
        ],
        [
            "quote" => "While a good leader sustains momentum, a great leader increases it.",
            "quoteBy" => "John C.\nMaxwell"
        ],
        [
            "quote" => "A general is just as good or just as bad as the troops under his command make him.",
            "quoteBy" => "General Douglas MacArthur"
        ],
        [
            "quote" => "To do great things is difficult; but to command great things is more difficult.",
            "quoteBy" => "Friedrich Nietzsche"
        ],
        [
            "quote" => "Leadership does not always wear the harness of compromise.",
            "quoteBy" => "Woodrow Wilson"
        ],
        [
            "quote" => "Business opportunities are like buses",
            "quoteBy" => "there's always another one coming.\""
        ],
        [
            "quote" => "I avoid looking forward or backward, and try to keep looking upward.",
            "quoteBy" => "Charlotte Bronte"
        ],
        [
            "quote" => "The more difficulties one has to encounter, within and without, the more significant and the higher in inspiration his life will be.",
            "quoteBy" => "Horace Bushnell"
        ],
        [
            "quote" => "Every artist was first an amateur.",
            "quoteBy" => "Ralph Waldo Emerson"
        ],
        [
            "quote" => "I was always looking outside myself for strength and confidence, but it comes from within.\nIt is there all of the time.",
            "quoteBy" => "Anna Freud"
        ],
        [
            "quote" => "We can do anything we want to do if we stick to it long enough.",
            "quoteBy" => "Helen Keller"
        ],
        [
            "quote" => "Our business in life is not to get ahead of others, but to get ahead of ourselves.",
            "quoteBy" => "E. Joseph Cossman"
        ],
        [
            "quote" => "Insist on yourself. Never imitate.",
            "quoteBy" => "Ralph Waldo Emerson"
        ],
        [
            "quote" => "Who looks outside, dreams.\nWho looks inside, awakes.",
            "quoteBy" => "Carl Jung"
        ],
        [
            "quote" => "It is not easy to find happiness in ourselves, and it is not possible to find it elsewhere.",
            "quoteBy" => "Agnes Repplier"
        ],
        [
            "quote" => "The only journey is the one within.",
            "quoteBy" => "Rainer Maria Rilke"
        ],
        [
            "quote" => "Follow your honest convictions, and stay strong.",
            "quoteBy" => "William Thackeray"
        ],
        [
            "quote" => "The happiness of your life depends upon the quality of your thoughts; therefore guard accordingly.",
            "quoteBy" => "Marcus Aurelius"
        ],
        [
            "quote" => "Action may not always bring happiness; but there is no happiness without action.",
            "quoteBy" => "Benjamin Disraeli"
        ],
        [
            "quote" => "Happiness depends more on the inward disposition of mind than on outward circumstances.",
            "quoteBy" => "Benjamin Franklin"
        ],
        [
            "quote" => "There is no happiness except in the realization that we have accomplished something.",
            "quoteBy" => "Henry Ford"
        ],
        [
            "quote" => "Happiness is not a goal, but a by-product.",
            "quoteBy" => "Eleanor Roosevelt"
        ],
        [
            "quote" => "Happiness is not a state to arrive at, but a manner of traveling.",
            "quoteBy" => "Margaret Lee Runbeck"
        ],
        [
            "quote" => "Purpose is what gives life a meaning.",
            "quoteBy" => "C. H. Parkhurst"
        ],
        [
            "quote" => "The significance of a man is not in what he attains but in what he longs to attain.",
            "quoteBy" => "Kahlil Gibran"
        ],
        [
            "quote" => "In all things that you do, consider the end.",
            "quoteBy" => "Solon"
        ],
        [
            "quote" => "Life can be pulled by goals just as surely as it can be pushed by drives.",
            "quoteBy" => "Viktor Frankl"
        ],
        [
            "quote" => "The virtue lies in the struggle, not in the prize.",
            "quoteBy" => "Richard Monckton Milnes"
        ],
        [
            "quote" => "To reach a port, we must sail",
            "quoteBy" => "sail, not tie at anchor"
        ],
        [
            "quote" => "Success is the child of audacity.",
            "quoteBy" => "Benjamin Disraeli"
        ],
        [
            "quote" => "The difference between a successful person and others is not a lack of strength, not a lack of knowledge, but rather a lack in will.",
            "quoteBy" => "Vince Lombardi"
        ],
        [
            "quote" => "The secret of success is to know something nobody else knows.",
            "quoteBy" => "Aristotle Onassis"
        ],
        [
            "quote" => "The surest way not to fail is to determine to succeed.",
            "quoteBy" => "Richard Brinsley Sheridan"
        ],
        [
            "quote" => "I cannot give you the formula for success, but I can give you the formula for failure",
            "quoteBy" => "which is: Try to please everybody.\""
        ],
        [
            "quote" => "Careful thinking and hard work will solve nearly all your problems.\nTry and see for yourself.",
            "quoteBy" => "Ullery"
        ],
        [
            "quote" => "Years teach us more than books.",
            "quoteBy" => "Berthold Auerbach"
        ],
        [
            "quote" => "The only medicine for suffering, crime, and all the other woes of mankind, is wisdom.",
            "quoteBy" => "Thomas Huxley"
        ],
        [
            "quote" => "The art of being wise is knowing what to overlook.",
            "quoteBy" => "William James"
        ],
        [
            "quote" => "The great lesson is that the sacred is in the ordinary, that it is to be found in one's daily life, in one's neighbors, friends and family, in one's backyard.",
            "quoteBy" => "Abraham Maslow"
        ],
        [
            "quote" => "A wise man learns by the mistakes of others, a fool by his own.",
            "quoteBy" => "Latin proverb"
        ],
        [
            "quote" => "No man was ever wise by chance.",
            "quoteBy" => "Seneca"
        ],
        [
            "quote" => "In everything the ends well defined are the secret of durable success.",
            "quoteBy" => "Victor Cousins"
        ],
        [
            "quote" => "Arriving at one goal is the starting point to another.",
            "quoteBy" => "John Dewey"
        ],
        [
            "quote" => "A goal is a dream with a deadline.",
            "quoteBy" => "Napoleon Hill"
        ],
        [
            "quote" => "Most 'impossible' goals can be met simply by breaking them down into bite-size chunks, writing them down, believing them, and then going full speed ahead as if they were routine.",
            "quoteBy" => "Don Lancaster"
        ],
        [
            "quote" => "Goals are the fuel in the furnace of achievement.",
            "quoteBy" => "Brian Tracy"
        ],
        [
            "quote" => "We are what we repeatedly do. Excellence, therefore, is not an act but a habit.",
            "quoteBy" => "Aristotle"
        ],
        [
            "quote" => "Take risks and you'll get the payoffs. Learn from your mistakes until you succeed.\nIt's that simple.",
            "quoteBy" => "Bobby Flay"
        ],
        [
            "quote" => "The best way out is always through.",
            "quoteBy" => "Robert Frost"
        ],
        [
            "quote" => "You miss 100 percent of the shots you don't take.",
            "quoteBy" => "Wayne Gretzky"
        ],
        [
            "quote" => "Nothing will ever be attempted if all possible objections must first be overcome.",
            "quoteBy" => "Samuel Johnson"
        ],
        [
            "quote" => "Don't bunt.\nAim out of the ballpark.",
            "quoteBy" => "David Ogilvy"
        ],
        [
            "quote" => "The critical ingredient is getting off your butt and doing something.\nIt's as simple as that.\nA lot of people have ideas, but there are few who decide to do something about them now. Not tomorrow. Not next week.\nBut today.\nThe true entrepreneur is a doer, not a dreamer.”",
            "quoteBy" => "Nolan Bushnell, entrepreneur."
        ],
        [
            "quote" => "Never give in",
            "quoteBy" => "never, never, never, never, in nothing great or small, large or petty, never give in except to convictions of honour and good sense. Never yield to force; never yield to the apparently overwhelming might of the enemy.” - Winston Churchill, British Prime Minister."
        ],
        [
            "quote" => "Your most unhappy customers are your greatest source of learning.",
            "quoteBy" => "Bill Gates, co-founder of Microsoft."
        ],
        [
            "quote" => "I have not failed.\nI've just found 10,000 ways that won't work.",
            "quoteBy" => "Thomas Edison, inventor."
        ],
        [
            "quote" => "Entrepreneurship is neither a science nor an art.\nIt is a practice.",
            "quoteBy" => "Peter Drucker, management consultant, educator, and author."
        ],
        [
            "quote" => "In the modern world of business, it is useless to be a creative, original thinker unless you can also sell what you create.",
            "quoteBy" => "David Ogilvy, co-founder of Ogilvy & Mather."
        ],
        [
            "quote" => "Success is how high you bounce after you hit bottom.",
            "quoteBy" => "General George Patton."
        ],
        [
            "quote" => "If you're not embarrassed by the first version of your product, you've launched too late.”",
            "quoteBy" => "Reid Hoffman, co-founder of LinkedIn."
        ],
        [
            "quote" => "Positive thinking will let you do everything better than negative thinking will.",
            "quoteBy" => "Zig Ziglar, author, salesman, and motivational speaker."
        ],
        [
            "quote" => "I'm not afraid of dying, I'm afraid of not trying.”",
            "quoteBy" => "Jay Z, musician."
        ],
        [
            "quote" => "Whatever the mind can conceive and believe, the mind can achieve.",
            "quoteBy" => "Dr. Napoleon Hill, author of Think and Grow Rich."
        ],
        [
            "quote" => "The longer you're not taking action the more money you're losing",
            "quoteBy" => "Carrie Wilkerson"
        ],
        [
            "quote" => "If you live for weekends or vacations, your shit is broken",
            "quoteBy" => "Gary Vaynerchuk"
        ],
        [
            "quote" => "Go Big, or Go Home",
            "quoteBy" => "Eliza Dushku"
        ],
        [
            "quote" => "Most great people have attained their greatest success just one step beyond their greatest failure",
            "quoteBy" => "Napoleon Hill"
        ],
        [
            "quote" => "Opportunity is missed by most people because it is dressed in overalls and looks like work",
            "quoteBy" => "Thomas Edison"
        ],
        [
            "quote" => "Have the end in mind and every day make sure your working towards it",
            "quoteBy" => "Ryan Allis"
        ],
        [
            "quote" => "He who begins many things finishes but few",
            "quoteBy" => "German Proverb"
        ],
        [
            "quote" => "The best use of life is to spend it for something that outlasts it",
            "quoteBy" => "William James"
        ],
        [
            "quote" => "If you think education is expensive, try ignorance",
            "quoteBy" => "Derek Bok"
        ],
        [
            "quote" => "Entrepreneurship is living a few years of your life like most people wont so you can spend the rest of your life like most people can't",
            "quoteBy" => "A student in Warren G.\nTracy's class"
        ],
        [
            "quote" => "Lend your friend $20, if he doesn't pay you back then he's not your friend.\nMoney well spent",
            "quoteBy" => "Ted Nicolas"
        ],
        [
            "quote" => "Be nice to geek's, you'll probably end up working for one",
            "quoteBy" => "Bill Gates"
        ],
        [
            "quote" => "To never forget that the most important thing in life is the quality of life we lead",
            "quoteBy" => "Quoted by Tony Hsieh on Retireat21"
        ],
        [
            "quote" => "Its better to own the racecourse then the race horse",
            "quoteBy" => "Unknown"
        ],
        [
            "quote" => "When you go to buy, don't show your silver",
            "quoteBy" => "Chinese Proverb"
        ],
        [
            "quote" => "It's easier to ask forgiveness than it is to get permission",
            "quoteBy" => "Grace Hopper"
        ],
        [
            "quote" => "To win without risk is to triumph without glory",
            "quoteBy" => "Corneille"
        ],
        [
            "quote" => "Example is not the main thing in influencing other people; it's the only thing",
            "quoteBy" => "Abraham Lincoln"
        ],
        [
            "quote" => "Associate yourself with people of good quality, for it is better to be alone than in bad company",
            "quoteBy" => "Booker T.\nWashington"
        ],
        [
            "quote" => "Keep away from people who try to belittle your ambitions.\nSmall people always do that, but the really great make you feel that you, too, can become great",
            "quoteBy" => "Mark Twain"
        ],
        [
            "quote" => "There is only one success",
            "quoteBy" => "to be able to spend your life in your own way"
        ],
        [
            "quote" => "You don't buy a nice car and get rich you get rich and buy a nice car",
            "quoteBy" => "Unknown"
        ],
        [
            "quote" => "Fall seven times, stand up eight",
            "quoteBy" => "Japanese Proverb"
        ],
        [
            "quote" => "One day your life will flash before your eyes.\nMake sure it is worth watching",
            "quoteBy" => "Mooie"
        ],
        [
            "quote" => "Whatever the mind can conceive and believe, the mind can achieve",
            "quoteBy" => "Dr. Napoleon Hill"
        ],
        [
            "quote" => "I have not failed.\nI've just found 10,000 ways that won't work",
            "quoteBy" => "Thomas Alva Edison"
        ],
        [
            "quote" => "If you ain't making waves, you ain't kickin' hard enough",
            "quoteBy" => "Unknown"
        ],
        [
            "quote" => "What is not started will never get finished",
            "quoteBy" => "Johann Wolfgang von Goethe"
        ],
        [
            "quote" => "Do not wait to strike until the iron is hot; but make it hot by striking",
            "quoteBy" => "William B.\nSprague"
        ],
        [
            "quote" => "When you cease to dream you cease to live",
            "quoteBy" => "Malcolm Forbes"
        ],
        [
            "quote" => "There are two rules for success. 1) Never tell everything you know.",
            "quoteBy" => "Roger H. Lincoln"
        ],
        [
            "quote" => "The only place where success comes before work is in the dictionary.",
            "quoteBy" => "Vidal Sassoon"
        ],
        [
            "quote" => "Every single person I know who is successful at what they do is successful because they love doing it.",
            "quoteBy" => "Joe Penna"
        ],
        [
            "quote" => "Being realistic is the most commonly traveled road to mediocrity.",
            "quoteBy" => "Will Smith"
        ],
        [
            "quote" => "Whenever an individual or a business decides that success has been attained, progress stops.",
            "quoteBy" => "Thomas J.\nWatson"
        ],
        [
            "quote" => "To be successful, you have to have your heart in your business, and your business in your heart.",
            "quoteBy" => "Thomas J.\nWatson"
        ],
        [
            "quote" => "If hard work is the key to success, most people would rather pick the lock.",
            "quoteBy" => "Claude McDonald"
        ],
        [
            "quote" => "Success is simply a matter of luck.\nAsk any failure.",
            "quoteBy" => "Earl Wilson"
        ],
        [
            "quote" => "The road to success is always under construction.",
            "quoteBy" => "Arnold Palmer"
        ],
        [
            "quote" => "Anything the mind can conceive and believe, it can achieve.",
            "quoteBy" => "Napoleon Hill"
        ],
        [
            "quote" => "Most great people have attained their greatest success just one step beyond their greatest failure.",
            "quoteBy" => "Napoleon Hill"
        ],
        [
            "quote" => "Whether you think you can or you can't, you're right.",
            "quoteBy" => "Henry Ford"
        ],
        [
            "quote" => "Failure defeats losers, failure inspires winners.",
            "quoteBy" => "Robert T.\nKiyosaki"
        ],
        [
            "quote" => "I have not failed.\nI've just found 10,000 ways that won't work.",
            "quoteBy" => "Thomas Edison"
        ],
        [
            "quote" => "The biggest failure you can have in life is not trying at all.",
            "quoteBy" => "Emil Motycka"
        ],
        [
            "quote" => "I honestly think it is better to be a failure at something you love than to be a success at something you hate.",
            "quoteBy" => "George Burns"
        ],
        [
            "quote" => "Leaders don't force people to follow, they invite them on a journey.",
            "quoteBy" => "Charles S. Lauer"
        ],
        [
            "quote" => "Example is not the main thing in influencing other people; it's the only thing.",
            "quoteBy" => "Abraham Lincoln"
        ],
        [
            "quote" => "Leadership is doing what is right when no one is watching.",
            "quoteBy" => "George Van Valkenburg"
        ],
        [
            "quote" => "Leadership is the art of getting someone else to do something you want done because he wants to do it.",
            "quoteBy" => "Dwight D. Eisenhower"
        ],
        [
            "quote" => "The difference between a boss and a leader: a boss says, 'Go!' -a leader says, 'Let's go!'.",
            "quoteBy" => "E.\nM.\nKelly"
        ],
        [
            "quote" => "I am more afraid of an army of one hundred sheep led by a lion than an army of one hundred lions led by a sheep.",
            "quoteBy" => "Charles Maurice"
        ],
        [
            "quote" => "The whole problem with the world is that fools and fanatics are always so certain of themselves, but wiser people so full of doubts.",
            "quoteBy" => "Bertrand Russell"
        ],
        [
            "quote" => "We are what we repeatedly do. Excellence, then, is not an act, but a habit.",
            "quoteBy" => "Aristotle"
        ],
        [
            "quote" => "Cannibals prefer those who have no spines.",
            "quoteBy" => "Stanislaw Lem"
        ],
        [
            "quote" => "A ship in harbor is safe.\nBut that's now what ships are built for.",
            "quoteBy" => "William Shedd"
        ],
        [
            "quote" => "If one does not know to which port one is sailing, no wind is favorable.",
            "quoteBy" => "Lucius Annaeus Seneca"
        ],
        [
            "quote" => "You miss 100% of the shots you don't take.",
            "quoteBy" => "Wayne Gretzky"
        ],
        [
            "quote" => "I'm not a businessman.\nI'm a business, man.",
            "quoteBy" => "Jay-Z"
        ],
        [
            "quote" => "The vision must be followed by the venture.\nIt is not enough to stare up the steps – we must step up the stairs.",
            "quoteBy" => "Vance Hayner"
        ],
        [
            "quote" => "Do not wait to strike until the iron is hot; but make it hot by striking.",
            "quoteBy" => "William B.\nSprague"
        ],
        [
            "quote" => "It's easier to ask forgiveness than it is to get permission.",
            "quoteBy" => "Grace Hopper"
        ],
        [
            "quote" => "Twenty years from now you will be more disappointed by the things that you didn't do than by the ones you did do.",
            "quoteBy" => "Mark Twain"
        ],
        [
            "quote" => "One day your life will flash before your eyes.\nMake sure it is worth watching.",
            "quoteBy" => "Mooie"
        ],
        [
            "quote" => "I think it's wrong that only one company makes the game Monopoly.",
            "quoteBy" => "Steven Wright"
        ],
        [
            "quote" => "Ever notice how it's a penny for your thoughts, yet you put in your two-cents? Someone is making a penny on the deal.",
            "quoteBy" => "Steven Wright"
        ],
        [
            "quote" => "Catch a man a fish, and you can sell it to him.\nTeach a man to fish, and you ruin a wonderful business opportunity.",
            "quoteBy" => "Karl (maybe Groucho) Marx"
        ],
        [
            "quote" => "I used to sell furniture for a living.\nThe trouble was, it was my own.",
            "quoteBy" => "Les Dawson"
        ],
        [
            "quote" => "Marking dynamos for repair $10,000.00—2 hours labor $10.00; knowing where to mark $9,990.00.",
            "quoteBy" => "Invoice from Charles Steinmetz"
        ],
        [
            "quote" => "By working faithfully eight hours a day you may eventually get to be boss and work twelve hours a day.",
            "quoteBy" => "Robert Frost"
        ],
        [
            "quote" => "My son is now an 'entrepreneur'.\nThat's what you're called when you don't have a job.",
            "quoteBy" => "Ted Turner"
        ],
        [
            "quote" => "I didn't go to college, but if I did, I would've taken all my tests at a restaurant, 'cause 'The customer is always right.'",
            "quoteBy" => "Mitch Hedberg"
        ],
        [
            "quote" => "Formal education will make you a living.\nSelf education will make you a fortune.",
            "quoteBy" => "Jim Rohn"
        ],
        [
            "quote" => "The greatest reward in becoming a millionaire is not the amount of money that you earn.\nIt is the kind of person that you have to become to become a millionaire in the first place.",
            "quoteBy" => "Jim Rohn"
        ],
        [
            "quote" => "If you're not learning while you're earning, you're cheating yourself out of the better portion of your compensation.",
            "quoteBy" => "Napoleon Hill"
        ],
        [
            "quote" => "A business that makes nothing but money is a poor business.",
            "quoteBy" => "Henry Ford"
        ],
        [
            "quote" => "After a certain point, money is meaningless.\nIt ceases to be the goal.\nThe game is what counts.",
            "quoteBy" => "Aristotle"
        ],
        [
            "quote" => "I treat business a bit like a computer game.\nI count money as points.\nI'm doing really well: making lots of money and lots of points.",
            "quoteBy" => "Michael Dunlop"
        ],
        [
            "quote" => "All of my friends were doing babysitting jobs.\nI wanted money without the job.",
            "quoteBy" => "Adam Horwitz"
        ],
        [
            "quote" => "I don't pay good wages because I have a lot of money; I have a lot of money because I pay good wages.",
            "quoteBy" => "Robert Bosch"
        ],
        [
            "quote" => "Lend your friend $20.\nIf he doesn't pay you back then he's not your friend.\nMoney well spent.",
            "quoteBy" => "Ted Nicolas"
        ],
        [
            "quote" => "Money and success don't change people; they merely amplify what is already there.",
            "quoteBy" => "Will Smith"
        ],
        [
            "quote" => "The secret of getting ahead is getting started.",
            "quoteBy" => "Agatha Christie"
        ],
        [
            "quote" => "Hire character.\nTrain skill.",
            "quoteBy" => "Peter Schultz"
        ],
        [
            "quote" => "In preparing for battle I have always found that plans are useless, but planning is indispensable.",
            "quoteBy" => "Dwight D. Eisenhower"
        ],
        [
            "quote" => "You've got to stop doing all the things that people have tried, tested, and found out don't work.",
            "quoteBy" => "Michael Dunlop"
        ],
        [
            "quote" => "I never perfected an invention that I did not think about in terms of the service it might give others… I find out what the world needs, then I proceed to invent.",
            "quoteBy" => "Thomas Edison"
        ],
        [
            "quote" => "If you're not making mistakes, then you're not making decisions.",
            "quoteBy" => "Catherine Cook"
        ],
        [
            "quote" => "Your most unhappy customers are your greatest source of learning.",
            "quoteBy" => "Bill Gates"
        ],
        [
            "quote" => "One can get anything if he is willing to help enough others get what they want.",
            "quoteBy" => "Zig Ziglar"
        ],
        [
            "quote" => "An entrepreneur tends to bite off a little more than he can chew hoping he'll quickly learn how to chew it.",
            "quoteBy" => "Roy Ash"
        ],
        [
            "quote" => "The true entrepreneur is a doer, not a dreamer.",
            "quoteBy" => "Nolan Bushnell"
        ],
        [
            "quote" => "Whenever you are asked if you can do a job, tell 'em, 'Certainly, I can!' Then get busy and find out how to do it.",
            "quoteBy" => "Theodore Roosevelt"
        ],
        [
            "quote" => "Everything started as nothing.",
            "quoteBy" => "Ben Weissenstein"
        ],
        [
            "quote" => "If you start with nothing and end up with nothing, there's nothing lost.",
            "quoteBy" => "Michael Dunlop"
        ],
        [
            "quote" => "Start today, not tomorrow.\nIf anything, you should have started yesterday.",
            "quoteBy" => "Emil Motycka"
        ],
        [
            "quote" => "Make it happen now, not tomorrow.\nTomorrow is a loser's excuse.",
            "quoteBy" => "Andrew Fashion"
        ],
        [
            "quote" => "Every day I get up and look through the Forbes list of the richest people in America.\nIf I'm not there, I go to work.",
            "quoteBy" => "Robert Orben"
        ],
        [
            "quote" => "Entrepreneurship is living a few years of your life like most people won't, so that you can spend the rest of your life like most people can't.",
            "quoteBy" => "Anonymous"
        ],
        [
            "quote" => "Yesterday's home runs don't win today's games.",
            "quoteBy" => "Babe Ruth"
        ],
        [
            "quote" => "You should always stay hungry.\nStay hungry, so you can eat.",
            "quoteBy" => "Syed Balkhi"
        ],
        [
            "quote" => "If you're not living life on the edge, you're taking up too much space.",
            "quoteBy" => "Anonymous"
        ],
        [
            "quote" => "I wasn't satisfied just to earn a good living.\nI was looking to make a statement.",
            "quoteBy" => "Donald Trump"
        ],
        [
            "quote" => "Opportunity is missed by most people because it is dressed in overalls and looks like work.",
            "quoteBy" => "Thomas Edison"
        ],
        [
            "quote" => "You must either modify your dreams or magnify your skills.",
            "quoteBy" => "Jim Rohn"
        ],
        [
            "quote" => "Keep away from people who try to belittle your ambitions.\nSmall people always do that, but the really great make you feel that you, too, can become great.",
            "quoteBy" => "Mark Twain"
        ],
        [
            "quote" => "Not a single person whose name is worth remembering lived a life of ease.",
            "quoteBy" => "Ryan P.\nAllis"
        ],
        [
            "quote" => "If you think that you are going to love something, give it a try.\nYou're going to kick yourself in the butt for the rest of your life if you don't.",
            "quoteBy" => "Joe Penna"
        ],
        [
            "quote" => "The best time to plant a tree is twenty years ago.\nThe second best time is now.",
            "quoteBy" => "Chinese Proverb"
        ],
        [
            "quote" => "If you want one year of prosperity, grow grain.\nIf you want ten years of prosperity, grow trees.\nIf you want one hundred years of prosperity, grow people.",
            "quoteBy" => "Chinese Proverb"
        ],
        [
            "quote" => "Vision without action is a daydream.\nAction without vision is a nightmare.",
            "quoteBy" => "Japanese Proverb"
        ],
        [
            "quote" => "Sow a thought, reap an action; sow an action, reap a habit; sow a habit, reap a character; sow a character, reap a destiny.",
            "quoteBy" => "Chinese Proverb"
        ],
        [
            "quote" => "A bad workman blames his tools.",
            "quoteBy" => "Chinese Proverb"
        ],
        [
            "quote" => "A fall into a ditch makes you wiser.",
            "quoteBy" => "Chinese Proverb"
        ],
        [
            "quote" => "Defeat isn't bitter if you don't swallow it.",
            "quoteBy" => "Chinese Proverb"
        ],
        [
            "quote" => "The diamond cannot be polished without friction, nor the man perfected without trials.",
            "quoteBy" => "Chinese Proverb"
        ],
        [
            "quote" => "A jade stone is useless before it is processed; a man is good-for-nothing until he is educated.",
            "quoteBy" => "Chinese Proverb"
        ],
        [
            "quote" => "A journey of a thousand miles begins with a single step.",
            "quoteBy" => "Chinese Proverb"
        ],
        [
            "quote" => "The loftiest towers rise from the ground.",
            "quoteBy" => "Chinese Proverb"
        ],
        [
            "quote" => "Building a castle is difficult. Defending and maintaining it is harder still.",
            "quoteBy" => "Asian Proverb"
        ],
        [
            "quote" => "A person who says it cannot be done should not interrupt the man doing it.",
            "quoteBy" => "Chinese Proverb"
        ],
        [
            "quote" => "All cats love fish but fear to wet their paws.",
            "quoteBy" => "Chinese Proverb"
        ],
        [
            "quote" => "Don't stand by the water and long for fish; go home and weave a net.",
            "quoteBy" => "Chinese Proverb"
        ],
        [
            "quote" => "Everyone should carefully observe which way his heart draws him, and then choose that way with all his strength.",
            "quoteBy" => "Hasidic Proverb"
        ],
        [
            "quote" => "Failing to plan is planning to fail.",
            "quoteBy" => "Chinese Proverb"
        ],
        [
            "quote" => "If you pay peanuts, you get monkeys.",
            "quoteBy" => "Chinese Proverb"
        ],
        [
            "quote" => "Make happy those who are near, and those who are far will come.",
            "quoteBy" => "Chinese Proverb"
        ],
        [
            "quote" => "Teachers open the door.\nYou enter by yourself.",
            "quoteBy" => "Chinese Proverb"
        ],
        [
            "quote" => "Find a job you love and you'll never work a day in your life.",
            "quoteBy" => "Confucius"
        ],
        [
            "quote" => "The entrepreneur builds an enterprise; the technician builds a job.",
            "quoteBy" => "Michael Gerber"
        ],
        [
            "quote" => "If you want to be inventive",
            "quoteBy" => "you have to be willing to fail.”"
        ],
        [
            "quote" => "The challenge is not just to build a company that can endure; but to build one that is worthy of enduring.",
            "quoteBy" => "Jim Collins"
        ],
        [
            "quote" => "Success is a lousy teacher.\nIt seduces smart people into thinking they can't lose.",
            "quoteBy" => "Bill Gates"
        ],
        [
            "quote" => "Don't treat your customers like a bunch of purses and wallets.",
            "quoteBy" => "Chris Brogan"
        ],
        [
            "quote" => "Nine out of ten businesses fail; so I came up with a foolproof plan",
            "quoteBy" => "create ten businesses.”"
        ],
        [
            "quote" => "If plan 'A' fails",
            "quoteBy" => "remember you have 25 letters left.”"
        ],
        [
            "quote" => "We make a living by what we get.\nBut we make a life by what we give.",
            "quoteBy" => "Winston Churchhill"
        ],
        [
            "quote" => "Believe that you will succeed, and you will.",
            "quoteBy" => "unknown"
        ],
        [
            "quote" => "Victory comes only after many struggles and countless defeats.",
            "quoteBy" => "Og Mandino"
        ],
        [
            "quote" => "As long as you're going to be thinking anyway, think big.",
            "quoteBy" => "Donald Trump"
        ],
        [
            "quote" => "Success is how high you bounce after you hit bottom",
            "quoteBy" => "General George Patton"
        ],
        [
            "quote" => "Remembering you are going to die is the best way I know to avoid the trap of thinking you have something to lose.",
            "quoteBy" => "Steve Jobs"
        ],
        [
            "quote" => "It's not about how to get started; its about how to get noticed.",
            "quoteBy" => "Steve Case"
        ],
        [
            "quote" => "A ship in harbor is safe, but that's not what ships are for.",
            "quoteBy" => "John Shedd"
        ],
        [
            "quote" => "If I find 10,000 ways something won't work, I haven't failed.\nI am not discouraged, because every wrong attempt discarded is often a step forward.",
            "quoteBy" => "Thomas Edison"
        ],
        [
            "quote" => "Business opportunities are like buses, there's always another one coming.",
            "quoteBy" => "Richard Branson"
        ],
        [
            "quote" => "If we don't allow ourselves to make mistakes, we will never invest in things that are radical.",
            "quoteBy" => "Jeff Clavier"
        ],
        [
            "quote" => "The critical ingredient is getting off your butt and doing something.",
            "quoteBy" => "Nolan Bushnell"
        ],
        [
            "quote" => "Nothing will work unless you do.",
            "quoteBy" => "Maya Angelou"
        ],
        [
            "quote" => "Try not to be a man of success, but rather try to become a man of value.",
            "quoteBy" => "Albert Einstein"
        ],
        [
            "quote" => "You won't get anything unless you have the vision to imagine it.",
            "quoteBy" => "John Lennon"
        ],
        [
            "quote" => "A man must be big enough to admit his mistakes, smart enough to profit from them, and strong enough to correct them.",
            "quoteBy" => "John C.\nMaxwell"
        ],
        [
            "quote" => "In the modern world of business, it is useless to be a creative, original thinker unless you can also sell what you create.",
            "quoteBy" => "David Ogilvy"
        ],
        [
            "quote" => "Success is doing what you want, where you want, when you want, with whom you want as much as you want.",
            "quoteBy" => "Tony Robbins"
        ],
        [
            "quote" => "If you don't have a competitive advantage",
            "quoteBy" => "don't compete.”"
        ],
        [
            "quote" => "You were born to win, but to be a winner, you must plan to win, prepare to win, and expect to win.",
            "quoteBy" => "Zig Ziglar"
        ],
        [
            "quote" => "If everything seems under control, you're just not going fast enough.",
            "quoteBy" => "Mario Andretti"
        ],
        [
            "quote" => "Don't be trapped by dogma",
            "quoteBy" => "which is living with the results of other people's thinking.”"
        ],
        [
            "quote" => "Do or do not.\nThere is no try.",
            "quoteBy" => "Yoda"
        ],
        [
            "quote" => "Show me a person who never made a mistake, and I will show you a person who never did anything.",
            "quoteBy" => "William Rosenberg, founder of Dunkin' Donuts"
        ],
        [
            "quote" => "Ideas are commodity. Execution of them is not.",
            "quoteBy" => "Michael Dell, CEO of Dell"
        ],
        [
            "quote" => "User experience is everything.\nIt always has been, but it's undervalued and underinvested in.\nIf you don't know user-centered design, study it. Hire people who know it. Obsess over it. Live and breathe it. Get your whole company on board.",
            "quoteBy" => "Evan Williams, co-founder of Twitter"
        ],
        [
            "quote" => "A pessimist sees the difficulty in every opportunity; an optimist sees the opportunity in every difficulty.",
            "quoteBy" => "Winston Churchill, British Prime Minister"
        ],
        [
            "quote" => "The man who does not work for the love of work but only for money is likely to neither make money nor find much fun in life.",
            "quoteBy" => "Charles M.\nSchwab, American steel magnate"
        ],
        [
            "quote" => "Be undeniably good. No marketing effort or social media buzzword can be a substitute for that.",
            "quoteBy" => "Anthony Volodkin, founder of Hype Machine"
        ],
        [
            "quote" => "Ideas are easy.\nImplementation is hard.",
            "quoteBy" => "Guy Kawasaki, founder of AllTop"
        ],
        [
            "quote" => "You can say anything to anyone, but how you say it will determine how they will react.",
            "quoteBy" => "John Rampton, entrepreneur and investor"
        ],
        [
            "quote" => "Always deliver more than expected.",
            "quoteBy" => "Larry Page, co-founder of Google"
        ],
        [
            "quote" => "Assume you have 90 seconds with a new user before they decide to use your app or delete it.",
            "quoteBy" => "Tamara Steffens, Acompli"
        ],
        [
            "quote" => "Even if you don't have the perfect idea to begin with, you can likely adapt.",
            "quoteBy" => "Victoria Ransom, co-founder of Wildfire Interactive"
        ],
        [
            "quote" => "Make your team feel respected, empowered and genuinely excited about the company's mission.",
            "quoteBy" => "Tim Westergen, founder of Pandora"
        ],
        [
            "quote" => "Stay self-funded as long as possible.",
            "quoteBy" => "Garrett Camp, co-founder of Stumbleupon"
        ],
        [
            "quote" => "In between goals is a thing called life, that has to be lived and enjoyed.",
            "quoteBy" => "Sid Caesar, Entertainer"
        ],
        [
            "quote" => "Wonder what your customer really wants? Ask. Don't tell.",
            "quoteBy" => "Lisa Stone, co-founder of BlogHer"
        ],
        [
            "quote" => "When times are bad is when the real entrepreneurs emerge.",
            "quoteBy" => "Robert Kiyosaki, author of Rich Dad Poor Dad"
        ],
        [
            "quote" => "What do you need to start a business? Three simple things: know your product better than anyone, know your customer, and have a burning desire to succeed.",
            "quoteBy" => "Dave Thomas, founder of Wendy's"
        ],
        [
            "quote" => "Get big quietly, so you don't tip off potential competitors.",
            "quoteBy" => "Chris Dixon, co-founder of Hunch"
        ],
        [
            "quote" => "Don't worry about failure; you only have to be right once.",
            "quoteBy" => "Drew Houston, founder of Dropbox"
        ],
        [
            "quote" => "Your time is limited, so don't waste it living someone else's life. Don't be trapped by dogma",
            "quoteBy" => "which is living with the results of other people's thinking. Don't let the noise of other's opinions drown out your own inner voice.\nAnd most important, have the courage to follow your heart and intuition.\nThey somehow already know what you truly want to become. Everything else is secondary.”"
        ],
        [
            "quote" => "Don't be cocky. Don't be flashy.\nThere's always someone better than you.",
            "quoteBy" => "Tony Hsieh, Zappos.com CEO"
        ],
        [
            "quote" => "Don't take too much advice.\nMost people who have a lot of advice to give — with a few exceptions — generalize whatever they did. Don't over-analyze everything.\nI myself have been guilty of over-thinking problems. Just build things and find out if they work.",
            "quoteBy" => "Ben Silbermann, co-founder of Pinterest"
        ],
        [
            "quote" => "Openly share and talk to people about your idea. Use their lack of interest or doubt to fuel your motivation to make it happen.",
            "quoteBy" => "Todd Garland, founder of BuySellAds"
        ],
        [
            "quote" => "How you climb a mountain is more important than reaching the top.",
            "quoteBy" => "Yvon Chouinard, founder of Patagonia"
        ],
        [
            "quote" => "Associate yourself with people of good quality, for it is better to be alone than in bad company.",
            "quoteBy" => "Booker T.\nWashington, Educator and author"
        ],
        [
            "quote" => "It's fine to celebrate success but it is more important to heed the lessons of failure.",
            "quoteBy" => "Bill Gates, co-founder of Microsoft"
        ],
        [
            "quote" => "It takes 20 years to build a reputation and five minutes to ruin it.\nIf you think about that, you'll do things differently.",
            "quoteBy" => "Warren Buffett, Investor"
        ],
        [
            "quote" => "Statistics suggest that when customers complain, business owners and managers ought to get excited about it.\nThe complaining customer represents a huge opportunity for more business.",
            "quoteBy" => "Zig Ziglar, author and motivational speaker"
        ],
        [
            "quote" => "There is only one success- to be able to spend your life in your own way.",
            "quoteBy" => "Christopher Morley, journalist"
        ],
        [
            "quote" => "Formal education will make you a living; self-education will make you a fortune.",
            "quoteBy" => "Jim Rohn, author and speaker"
        ],
        [
            "quote" => "When everything seems to be going against you, remember that the airplane takes off against the wind, not with it.",
            "quoteBy" => "Henry Ford, founder of Ford Motor Company"
        ],
        [
            "quote" => "Rarely have I seen a situation where doing less than the other guy is a good strategy.",
            "quoteBy" => "Jimmy Spithill, Australian yachtsman"
        ],
        [
            "quote" => "You miss 100 percent of the shots you don't take.",
            "quoteBy" => "Wayne Gretzky, NHL Hall of Famer"
        ],
        [
            "quote" => "The best time to plant a tree was 20 years ago.\nThe second best time is now.",
            "quoteBy" => "Chinese proverb"
        ],
        [
            "quote" => "The secret to successful hiring is this: look for the people who want to change the world.",
            "quoteBy" => "Marc Benioff, CEO of Salesforce"
        ],
        [
            "quote" => "Twenty years from now, you will be more disappointed by the things that you didn't do than by the ones you did do, so throw off the bowlines, sail away from safe harbor, catch the trade winds in your sails. Explore, Dream, Discover.",
            "quoteBy" => "Mark Twain, author"
        ],
        [
            "quote" => "If you've got an idea, start today.\nThere's no better time than now to get going.\nThat doesn't mean quit your job and jump into your idea 100% from day one, but there's always small progress that can be made to start the movement.",
            "quoteBy" => "Kevin Systrom, co-founder of Instagram"
        ],
        [
            "quote" => "It's almost always harder to raise capital than you thought it would be, and it always takes longer.\nSo plan for that.",
            "quoteBy" => "Richard Harroch, Venture Capitalist"
        ],
        [
            "quote" => "For all of the most important things, the timing always sucks.\nWaiting for a good time to quit your job? The stars will never align and the traffic lights of life will never all be green at the same time.\nThe universe doesn't conspire against you, but it doesn't go out of its way to line up the pins either. Conditions are never perfect.\nSomeday is a disease that will take your dreams to the grave with you. Pro and con lists are just as bad.\nIf it's important to you and you want to do it eventually, just do it and correct course along the way.",
            "quoteBy" => "Timothy Ferriss, author of The 4-Hour Work Week"
        ],
        [
            "quote" => "I don't have big ideas.\nI sometimes have small ideas, which seem to work out.",
            "quoteBy" => "Matt Mullenweg, founder of Automattic"
        ],
        [
            "quote" => "Fail often so you can succeed sooner.",
            "quoteBy" => "Tom Kelley, Ideo partner"
        ],
        [
            "quote" => "When you cease to dream you cease to live.",
            "quoteBy" => "Malcolm Forbes, chairman and editor in chief of Forbes Magazine"
        ],
        [
            "quote" => "Whatever the mind can conceive and believe, the mind can achieve.",
            "quoteBy" => "Dr. Napoleon Hill, author of Think and Grow Rich"
        ],
        [
            "quote" => "Running a start-up is like eating glass.\nYou just start to like the taste of your own blood.",
            "quoteBy" => "Sean Parker, co-founder of Napster"
        ],
        [
            "quote" => "My number one piece of advice is: you should learn how to program.",
            "quoteBy" => "Mark Zuckerberg, founder of Facebook"
        ],
        [
            "quote" => "The way to get started is to quit talking and begin doing.",
            "quoteBy" => "Walt Disney, co-founder of the Walt Disney Company"
        ],
        [
            "quote" => "The pace of change for entrepreneurs is rapidly accelerating, and the cost and risk of launching a new business and getting off the ground is just amazing.\nThe ability to gain user feedback really quickly and adapt to what your consumers want is totally different with the web as it is now.\nBut finding a new market, helping people and taking that original idea and turning it into a business is really exciting right now.",
            "quoteBy" => "Matt Mickiewicz, co-founder of 99 Designs"
        ],
        [
            "quote" => "Building and hanging on to an audience is the biggest role of social media.",
            "quoteBy" => "Matthew Inman, The Oatmeal"
        ],
        [
            "quote" => "Every feature has some maintenance cost, and having fewer features lets us focus on the ones we care about and make sure they work very well.",
            "quoteBy" => "David Karp, founder of Tumblr"
        ],
        [
            "quote" => "A poorly implemented feature hurts more than not having it at all.",
            "quoteBy" => "Noah Everett, founder of Twitpic"
        ],
        [
            "quote" => "This defines entrepreneur and entrepreneurship",
            "quoteBy" => "the entrepreneur always searches for change, responds to it, and exploits it as an opportunity.”― Peter F. Drucker, educator and author"
        ],
        [
            "quote" => "You don't learn to walk by following rules.\nYou learn by doing and falling over.",
            "quoteBy" => "Richard Branson, founder of Virgin Group"
        ],
        [
            "quote" => "The fastest way to change yourself is to hang out with people who are already the way you want to be.",
            "quoteBy" => "Reid Hoffman, co-founder of Linkedin"
        ],
        [
            "quote" => "Risk more than others think is safe. Dream more than others think is practical.",
            "quoteBy" => "Howard Schultz, CEO of Starbucks"
        ],
        [
            "quote" => "You shouldn't focus on why you can't do something, which is what most people do.\nYou should focus on why perhaps you can, and be one of the exceptions.",
            "quoteBy" => "Steve Case, co-founder of AOL"
        ],
        [
            "quote" => "The critical ingredient is getting off your butt and doing something.\nIt's as simple as that.\nA lot of people have ideas, but there are few who decide to do something about them now.\nNot tomorrow.\nNot next week.\nBut today.\nThe true entrepreneur is a doer not a dreamer.",
            "quoteBy" => "Nolan Bushnell, founder of Atari"
        ],
        [
            "quote" => "If you cannot do great things, do small things in a great way.",
            "quoteBy" => "Napoleon Hill, author"
        ],
        [
            "quote" => "Success is not what you have, but who you are.",
            "quoteBy" => "Bo Bennet, author and entrepreneur"
        ],
        [
            "quote" => "One of the huge mistakes people make is that they try to force an interest on themselves.\nYou don't choose your passions; your passions choose you.",
            "quoteBy" => "Jeff Bezos, founder and CEO of Amazon.com"
        ],
        [
            "quote" => "Lots of companies don't succeed over time.\nWhat do they fundamentally do wrong? They usually miss the future.",
            "quoteBy" => "Larry Page, CEO of Google"
        ],
        [
            "quote" => "It takes humility to realize that we don't know everything, not to rest on our laurels and know that we must keep learning and observing.\nIf we don't, we can be sure some startup will be there to take our place.",
            "quoteBy" => "Cher Wang, CEO of HTC"
        ],
        [
            "quote" => "There's an entrepreneur right now, scared to death, making excuses, saying, 'It's not the right time just yet.' There's no such thing as a good time. Get out of your garage and go take a chance, and start your business.",
            "quoteBy" => "Kevin Plank, CEO of Under Armour"
        ],
        [
            "quote" => "Be really picky with your hiring, and hire the absolute best people you possibly can. People are the most important component of almost every business, and attracting the best talent possible is going to make a huge difference.",
            "quoteBy" => "Peter Berg, founder of October Three"
        ],
        [
            "quote" => "Worry about being better; bigger will take care of itself.\nThink one customer at a time and take care of each one the best way you can.",
            "quoteBy" => "Gary Comer, founder of Land's End"
        ],
        [
            "quote" => "You have a viable business only if your product is either better or cheaper than the alternatives.\nIf it's not one or the other, you might make some money at first, but it's not a sustainable business.",
            "quoteBy" => "Jim Koch, founder of Boston Beer Co."
        ],
        [
            "quote" => "Every time I took these bigger risks, the opportunity for a bigger payout was always there.",
            "quoteBy" => "Casey Neistat, filmmaker"
        ],
        [
            "quote" => "If you can do something to get somebody excited — not everybody — but if you can be the best for somebody, then you can win.",
            "quoteBy" => "Ron Shaich, founder and CEO of Panera Bread"
        ],
        [
            "quote" => "Would you like me to give you a formula for success?\nIt's quite simple, really: Double your rate of failure.\nYou are thinking of failure as the enemy of success.\nBut it isn't at all.\nYou can be discouraged by failure or you can learn from it, so go ahead and make mistakes.\nMake all you can.\nBecause remember that's where you will find success.",
            "quoteBy" => "Thomas J.\nWatson, businessman"
        ],
        [
            "quote" => "People who succeed have momentum.\nThe more they succeed, the more they want to succeed, and the more they find a way to succeed.\nSimilarly, when someone is failing, the tendency is to get on a downward spiral that can even become a self-fulfilling prophecy.",
            "quoteBy" => "Tony Robbins, motivational speaker"
        ],
        [
            "quote" => "The only limit to our realization of tomorrow will be our doubts of today.",
            "quoteBy" => "Franklin D. Roosevelt"
        ],
        [
            "quote" => "The successful warrior is the average man, with laser-like focus.",
            "quoteBy" => "Bruce Lee, actor and martial arts instructor"
        ],
        [
            "quote" => "Fall seven times and stand up eight.",
            "quoteBy" => "Japanese proverb"
        ],
        [
            "quote" => "The successful man is the one who finds out what is the matter with his business before his competitors do.",
            "quoteBy" => "Roy L.\nSmith, animator and film director"
        ],
        [
            "quote" => "There's no shortage of remarkable ideas, what's missing is the will to execute them.",
            "quoteBy" => "Seth Godin, author and speaker"
        ],
        [
            "quote" => "Keep on going, and the chances are that you will stumble on something, perhaps when you are least expecting it.\nI never heard of anyone ever stumbling on something sitting down.",
            "quoteBy" => "Charles F.\nKettering, inventor"
        ],
        [
            "quote" => "A successful man is one who can lay a firm foundation with the bricks others have thrown at him.",
            "quoteBy" => "David Brinkley, newscaster"
        ],
        [
            "quote" => "Everyone is a genius.\nBut if you judge a fish by its ability to climb a tree, it will spend its whole life believing it is stupid.",
            "quoteBy" => "Albert Einstein, Theoretical Physicist"
        ],
        [
            "quote" => "Success in business requires training and discipline and hard work.\nBut if you're not frightened by these things, the opportunities are just as great today as they ever were.",
            "quoteBy" => "David Rockefeller, American banker"
        ],
        [
            "quote" => "It is never too late to be what you might have been.",
            "quoteBy" => "George Eliot, journalist and novelist"
        ],
        [
            "quote" => "I am not a product of my circumstances.\nI am a product of my decisions.",
            "quoteBy" => "Stephen Covey, author and businessman"
        ],
        [
            "quote" => "There is only one way to avoid criticism: Do nothing, say nothing, and be nothing.",
            "quoteBy" => "Aristotle, Greek philosopher"
        ],
        [
            "quote" => "Build your own dreams, or someone else will hire you to build theirs.",
            "quoteBy" => "Farrah Gray, investor and motivational speaker"
        ],
        [
            "quote" => "You may be disappointed if you fail, but you are doomed if you don't try.",
            "quoteBy" => "Beverly Sills, operatic soprano"
        ]
    ];

$numQuotes = count($quotes);
if ($numQuotes == 0) {
    $randomQuote = "These are not the quotes you are looking for...";
} else {
    $random = rand(0, $numQuotes - 1);
    $randomQuote = $quotes[$random];
}
echo json_encode($randomQuote);
