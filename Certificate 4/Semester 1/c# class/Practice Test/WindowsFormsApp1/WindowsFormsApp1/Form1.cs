﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApp1
{
    public partial class Form1 : Form
    {
        int int_total;
        public Form1()
        {
            InitializeComponent();
            txt_total.ReadOnly = true;
            txt_change.ReadOnly = true;
        }

        private void btn_fare_Click(object sender, EventArgs e)
        {
            if (!int.TryParse(txt_adults.Text, out int int_adults))
            {
                MessageBox.Show("Please enter a whole number into Adults text box");
            }
            if (!int.TryParse(txt_conc.Text, out int int_conc))
            {
                MessageBox.Show("Please enter a whole number into Concession text box");
            }

             int_total = (int_adults * 110) + (int_conc * 75);

            txt_total.Text = int_total.ToString("c");
            


        }

        private void btn_change_Click(object sender, EventArgs e)
        {
            if (!int.TryParse(txt_amount.Text, out int int_amount))
            {
                MessageBox.Show("Please enter a decimal number into Amount Received textbox");
            }

            int int_change = int_amount - int_total;
            txt_change.Text = int_change.ToString("c");
        }
    }
}
