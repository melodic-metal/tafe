﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace BrendanKellyJ082400Week05ExerciseSetA
{
    public partial class Form4 : Form
    {
        public Form4()
        {
            InitializeComponent();
        }

        private void btn_calc_Click(object sender, EventArgs e)
        {
            int int_halved_times = 0;
            float flt_temp;
            float.TryParse(txt_input.Text, out float flt_user_input);
            while (flt_user_input > 1)
            {
                
                flt_user_input = flt_user_input / 2;
                int_halved_times++;
                
            }

            lbl_output.Text = "That number can be halved " + int_halved_times.ToString() + " times";
        }
    }
}
