﻿namespace BrendanKellyj082400Week04ExercisesSetA
{
    partial class Form3
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.txt_sale_price = new System.Windows.Forms.TextBox();
            this.lbl_sale_price = new System.Windows.Forms.Label();
            this.lbl_cost = new System.Windows.Forms.Label();
            this.txt_cost = new System.Windows.Forms.TextBox();
            this.txt_output = new System.Windows.Forms.TextBox();
            this.lbl_output = new System.Windows.Forms.Label();
            this.btn_calc = new System.Windows.Forms.Button();
            this.grp_inputs = new System.Windows.Forms.GroupBox();
            this.SuspendLayout();
            // 
            // txt_sale_price
            // 
            this.txt_sale_price.Location = new System.Drawing.Point(141, 58);
            this.txt_sale_price.Name = "txt_sale_price";
            this.txt_sale_price.Size = new System.Drawing.Size(100, 20);
            this.txt_sale_price.TabIndex = 0;
            // 
            // lbl_sale_price
            // 
            this.lbl_sale_price.AutoSize = true;
            this.lbl_sale_price.Location = new System.Drawing.Point(52, 61);
            this.lbl_sale_price.Name = "lbl_sale_price";
            this.lbl_sale_price.Size = new System.Drawing.Size(55, 13);
            this.lbl_sale_price.TabIndex = 1;
            this.lbl_sale_price.Text = "Sale Price";
            // 
            // lbl_cost
            // 
            this.lbl_cost.AutoSize = true;
            this.lbl_cost.Location = new System.Drawing.Point(79, 106);
            this.lbl_cost.Name = "lbl_cost";
            this.lbl_cost.Size = new System.Drawing.Size(28, 13);
            this.lbl_cost.TabIndex = 2;
            this.lbl_cost.Text = "Cost";
            // 
            // txt_cost
            // 
            this.txt_cost.Location = new System.Drawing.Point(141, 106);
            this.txt_cost.Name = "txt_cost";
            this.txt_cost.Size = new System.Drawing.Size(100, 20);
            this.txt_cost.TabIndex = 3;
            // 
            // txt_output
            // 
            this.txt_output.Enabled = false;
            this.txt_output.Location = new System.Drawing.Point(141, 168);
            this.txt_output.Name = "txt_output";
            this.txt_output.Size = new System.Drawing.Size(100, 20);
            this.txt_output.TabIndex = 4;
            // 
            // lbl_output
            // 
            this.lbl_output.AutoSize = true;
            this.lbl_output.Location = new System.Drawing.Point(71, 168);
            this.lbl_output.Name = "lbl_output";
            this.lbl_output.Size = new System.Drawing.Size(39, 13);
            this.lbl_output.TabIndex = 5;
            this.lbl_output.Text = "Output";
            // 
            // btn_calc
            // 
            this.btn_calc.Location = new System.Drawing.Point(106, 214);
            this.btn_calc.Name = "btn_calc";
            this.btn_calc.Size = new System.Drawing.Size(75, 23);
            this.btn_calc.TabIndex = 6;
            this.btn_calc.Text = "Calculate";
            this.btn_calc.UseVisualStyleBackColor = true;
            this.btn_calc.Click += new System.EventHandler(this.btn_calc_Click);
            // 
            // grp_inputs
            // 
            this.grp_inputs.Location = new System.Drawing.Point(24, 27);
            this.grp_inputs.Name = "grp_inputs";
            this.grp_inputs.Size = new System.Drawing.Size(239, 117);
            this.grp_inputs.TabIndex = 7;
            this.grp_inputs.TabStop = false;
            this.grp_inputs.Text = "Inputs";
            // 
            // Form3
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(284, 261);
            this.Controls.Add(this.btn_calc);
            this.Controls.Add(this.lbl_output);
            this.Controls.Add(this.txt_output);
            this.Controls.Add(this.txt_cost);
            this.Controls.Add(this.lbl_cost);
            this.Controls.Add(this.lbl_sale_price);
            this.Controls.Add(this.txt_sale_price);
            this.Controls.Add(this.grp_inputs);
            this.Name = "Form3";
            this.Text = "Sales! Sales! Sales!";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox txt_sale_price;
        private System.Windows.Forms.Label lbl_sale_price;
        private System.Windows.Forms.Label lbl_cost;
        private System.Windows.Forms.TextBox txt_cost;
        private System.Windows.Forms.TextBox txt_output;
        private System.Windows.Forms.Label lbl_output;
        private System.Windows.Forms.Button btn_calc;
        private System.Windows.Forms.GroupBox grp_inputs;
    }
}