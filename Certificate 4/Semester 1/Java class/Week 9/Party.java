// Date: 13/09/2018
// Programmer: Brendan Kelly
// Purpose: Main Party class, with getters and setters, and a display invitation method.

public class Party {

   private int guests;
   
   public int getGuests() {
      return guests;
      
   }  
   public void setGuests(int numGuests) {
      guests = numGuests;
   }
   
   public void displayInvitation() {
      System.out.println("Please come to my party!");
   }
      
}