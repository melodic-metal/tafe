// Date: 13/09/2018
// Programmer: Brendan Kelly
// Purpose: Extended candle class, for to hold data for scent.

public class ScentedCandle extends Candle {

   private static String scent;
   private static int height;
   
   public static void setScent(String sc) {
      scent = sc;
   }
   
   
   public static String getScent()  {
      return scent;
   }
   
   
   public static void setHeight(int hei) {
      height = hei * 3;
   }  

}