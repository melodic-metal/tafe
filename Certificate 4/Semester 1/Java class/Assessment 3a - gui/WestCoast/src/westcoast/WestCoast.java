/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author: brendan
 * @date: 11/10/2018
 * @version: 1.0
 */
package westcoast;
import javax.swing.*;
import java.awt.*;
import java.awt.event.*;

public class WestCoast extends JFrame implements ItemListener {
    
    
    /**
     * @param args the command line arguments
     */
        // prices for service
        final int PC_MAC = 30;
        final int LAPTOP_TAB_IPAD = 20;
        final int PRINTER_SCANNER = 7;
        final int LESSON_COST = 5;
        int totalPrice = 0;
 
        
        JLabel mainHeader = new JLabel("West Coast Computer Rental");
        JTextField numHours = new JTextField("0", 5);
        JCheckBox pc = new JCheckBox("PC");
        JCheckBox mac = new JCheckBox("Mac");
        JCheckBox laptop = new JCheckBox("Laptop");
        JCheckBox ipad = new JCheckBox("iPad");
        JCheckBox printer = new JCheckBox("Printer");
        JCheckBox scanner = new JCheckBox("Scanner");
        JCheckBox lesson = new JCheckBox("Lesson");

        JLabel lessons = new JLabel("Lessons?");
        JLabel total = new JLabel("$0");
    
    public WestCoast()  {
        super("West Coast Computer Repairs");
        
        
        setLayout(new FlowLayout());
        add(mainHeader);
        add(numHours);
        add(pc);
        add(laptop);
        add(ipad);
        add(printer);
        add(scanner);
        add(total);
        add(lesson);

        pc.addItemListener(this);
        laptop.addItemListener(this);
        ipad.addItemListener(this);
        printer.addItemListener(this);
        scanner.addItemListener(this);
        lesson.addItemListener(this);

        
        
        
    
}
    public static void main(String[] args) {
      final int WIDTH = 400;
      final int HEIGHT = 400;
      WestCoast aFrame = new WestCoast();
      aFrame.setSize(WIDTH, HEIGHT);
      aFrame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
      aFrame.setVisible(true);
        
    }

    @Override
    public void itemStateChanged(ItemEvent e) {
        Object source = e.getSource();
        int select = e.getStateChange();
        
        if (source == pc || source == mac) {
            if(select == ItemEvent.SELECTED) {
                
                totalPrice += Integer.parseInt(numHours.getText()) * PC_MAC;
            }
            else {
                totalPrice -= Integer.parseInt(numHours.getText()) * PC_MAC;
            }
        }
        if (source == ipad || source == laptop) {
            if(select == ItemEvent.SELECTED) {
                
                totalPrice += Integer.parseInt(numHours.getText()) * LAPTOP_TAB_IPAD;
            }
            else {
                totalPrice -= Integer.parseInt(numHours.getText()) * LAPTOP_TAB_IPAD;
            }
        }
        if (source == printer || source == scanner) {
            if(select == ItemEvent.SELECTED) {
                
                totalPrice += Integer.parseInt(numHours.getText()) * PRINTER_SCANNER;
            }
            else {
                totalPrice -= Integer.parseInt(numHours.getText()) * PRINTER_SCANNER;
            }
        }
        if (source == lesson) {
            if (select == ItemEvent.SELECTED) {
                    totalPrice += LESSON_COST;
            }
            else {
                totalPrice -= 5;
            }
                  
        }

        total.setText("$" + totalPrice);
    }
    
}
