import java.util.Scanner;

public class IntegerDemoInteractive {

   public static void main(String[] args) {
   
      int anInt;
      byte aByte;
      short aShort;
      long aLong;
   
      Scanner input = new Scanner(System.in);
      
      System.out.print("Enter an int: ");
      anInt = input.nextInt();
      System.out.print("Enter a byte: ");
      aByte = input.nextByte();
      System.out.print("Enter a short: ");
      aShort = input.nextShort();
      System.out.print("Enter a long: ");
      aLong = input.nextLong();
      
      System.out.println("The int is: " + anInt + "\nThe byte is: " + aByte + "\nThe short is: " + aShort + "\nThe long is: " + aLong);
   
   }

}