﻿namespace BrendanKellyJ082400Week05ExerciseSetA
{
    partial class Form9
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btn_calc = new System.Windows.Forms.Button();
            this.lbl_investment = new System.Windows.Forms.Label();
            this.lbl_interest = new System.Windows.Forms.Label();
            this.txt_output = new System.Windows.Forms.TextBox();
            this.txt_investment = new System.Windows.Forms.TextBox();
            this.txt_interest = new System.Windows.Forms.TextBox();
            this.SuspendLayout();
            // 
            // btn_calc
            // 
            this.btn_calc.Location = new System.Drawing.Point(101, 214);
            this.btn_calc.Name = "btn_calc";
            this.btn_calc.Size = new System.Drawing.Size(75, 23);
            this.btn_calc.TabIndex = 0;
            this.btn_calc.Text = "Calculate";
            this.btn_calc.UseVisualStyleBackColor = true;
            this.btn_calc.Click += new System.EventHandler(this.btn_calc_Click);
            // 
            // lbl_investment
            // 
            this.lbl_investment.AutoSize = true;
            this.lbl_investment.Location = new System.Drawing.Point(13, 54);
            this.lbl_investment.Name = "lbl_investment";
            this.lbl_investment.Size = new System.Drawing.Size(97, 13);
            this.lbl_investment.TabIndex = 1;
            this.lbl_investment.Text = "Investment amount";
            // 
            // lbl_interest
            // 
            this.lbl_interest.AutoSize = true;
            this.lbl_interest.Location = new System.Drawing.Point(33, 97);
            this.lbl_interest.Name = "lbl_interest";
            this.lbl_interest.Size = new System.Drawing.Size(68, 13);
            this.lbl_interest.TabIndex = 2;
            this.lbl_interest.Text = "Interest Rate";
            // 
            // txt_output
            // 
            this.txt_output.Location = new System.Drawing.Point(12, 138);
            this.txt_output.Name = "txt_output";
            this.txt_output.Size = new System.Drawing.Size(260, 20);
            this.txt_output.TabIndex = 3;
            // 
            // txt_investment
            // 
            this.txt_investment.Location = new System.Drawing.Point(116, 51);
            this.txt_investment.Name = "txt_investment";
            this.txt_investment.Size = new System.Drawing.Size(100, 20);
            this.txt_investment.TabIndex = 4;
            // 
            // txt_interest
            // 
            this.txt_interest.Location = new System.Drawing.Point(116, 89);
            this.txt_interest.Name = "txt_interest";
            this.txt_interest.Size = new System.Drawing.Size(100, 20);
            this.txt_interest.TabIndex = 5;
            // 
            // Form9
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(284, 261);
            this.Controls.Add(this.txt_interest);
            this.Controls.Add(this.txt_investment);
            this.Controls.Add(this.txt_output);
            this.Controls.Add(this.lbl_interest);
            this.Controls.Add(this.lbl_investment);
            this.Controls.Add(this.btn_calc);
            this.Name = "Form9";
            this.Text = "Form9";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btn_calc;
        private System.Windows.Forms.Label lbl_investment;
        private System.Windows.Forms.Label lbl_interest;
        private System.Windows.Forms.TextBox txt_output;
        private System.Windows.Forms.TextBox txt_investment;
        private System.Windows.Forms.TextBox txt_interest;
    }
}