// Date: 13/09/2018
// Programmer: Brendan Kelly
// Purpose: Extending the party class

public class DinnerParty extends Party {

   private int dinnerChoice;
   public int getDinnerChoice() {
      return dinnerChoice;
   }
   
   public void setDinnerChoice(int choice) {
   
      dinnerChoice = choice;
   }   
  
}