﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace BrendanKellyJ082400ExerciseSetA
{
    public partial class Form5 : Form
    {
        int[] int_grades = { 98, 73, 47, 65, 58 };
        public Form5()
        {
            InitializeComponent();
        }

        private void btn_calc_Click(object sender, EventArgs e)
        {
            int int_total = 1;
            double dbl_average;
            for (int i = 0; i < int_grades.Length; i++)
            {
                int_total += int_grades[i];

            }
            dbl_average = int_total / int_grades.Length;
            txt_output.Text = dbl_average.ToString();
        }
    }
}
