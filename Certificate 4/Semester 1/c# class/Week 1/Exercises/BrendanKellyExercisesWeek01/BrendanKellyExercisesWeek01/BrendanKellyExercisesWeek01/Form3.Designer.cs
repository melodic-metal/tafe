﻿namespace BrendanKellyExercisesWeek01
{
    partial class Form3
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lblPoetry = new System.Windows.Forms.Label();
            this.rtfPoetry = new System.Windows.Forms.RichTextBox();
            this.SuspendLayout();
            // 
            // lblPoetry
            // 
            this.lblPoetry.AutoSize = true;
            this.lblPoetry.Location = new System.Drawing.Point(105, 40);
            this.lblPoetry.Name = "lblPoetry";
            this.lblPoetry.Size = new System.Drawing.Size(69, 13);
            this.lblPoetry.TabIndex = 0;
            this.lblPoetry.Text = "Poetry Quote";
            // 
            // rtfPoetry
            // 
            this.rtfPoetry.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(255)))));
            this.rtfPoetry.ForeColor = System.Drawing.Color.Red;
            this.rtfPoetry.Location = new System.Drawing.Point(43, 73);
            this.rtfPoetry.Name = "rtfPoetry";
            this.rtfPoetry.Size = new System.Drawing.Size(189, 98);
            this.rtfPoetry.TabIndex = 1;
            this.rtfPoetry.Text = "It was somewhere up in the country\nin a land of bush and scrub\nthey formed an ins" +
    "titution\ncalled the geebung polo club\n";
            // 
            // Form3
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(284, 261);
            this.Controls.Add(this.rtfPoetry);
            this.Controls.Add(this.lblPoetry);
            this.Name = "Form3";
            this.Text = "Question3";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lblPoetry;
        private System.Windows.Forms.RichTextBox rtfPoetry;
    }
}