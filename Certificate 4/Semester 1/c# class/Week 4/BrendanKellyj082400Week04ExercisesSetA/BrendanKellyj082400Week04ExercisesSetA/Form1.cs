﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace BrendanKellyj082400Week04ExercisesSetA
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void btn_calc_Click(object sender, EventArgs e)
        {
            int int_cost = 20;
            decimal dec_discount_amount, dec_amount_to_pay;
            int int_num_booms;

            if (!int.TryParse(txt_num_booms.Text, out int_num_booms)) {
                MessageBox.Show("Please enter a valid number"); 
                txt_num_booms.Clear(); //Clear textbox
                txt_num_booms.Focus(); // put focus back in text box
                return;
            }

            if (int_num_booms < 10)
            {
                int_cost = 20;
            }
            else
            {
                int_cost = 18;
            }

            dec_amount_to_pay = int_num_booms * int_cost;
            txt_cost.Text = dec_amount_to_pay.ToString("C");
        }
    }
}
