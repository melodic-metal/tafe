﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace BrendanKellyJ082400Week05ExerciseSetA
{
    public partial class Form6 : Form
    {
        public Form6()
        {
            InitializeComponent();
        }

        private void btn_calc_Click(object sender, EventArgs e)
        {
            String str_binary_number = "";
            int.TryParse(txt_input.Text, out int int_input_number);

            while (int_input_number > 0)
            {

                if (int_input_number % 2 == 1)
                {
                    str_binary_number = "1" + str_binary_number;
                    int_input_number = int_input_number / 2;
                }
                else if (int_input_number % 2 == 0)
                {
                    str_binary_number = "0" + str_binary_number;
                    int_input_number = int_input_number / 2;
                }

                else if (int_input_number == 1)
                {
                    str_binary_number = "1";
                }

                txt_output.Text = str_binary_number;
        }
    }
}

}
