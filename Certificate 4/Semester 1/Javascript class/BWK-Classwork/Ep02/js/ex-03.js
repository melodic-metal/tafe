/**
 *
 *
 * Calculations in JS
 *
 *
 * @File:       /Ep02/js/ex-03.js
 * @Project:    Classwork
 * @Author:     Brendan Kelly <j082400@tafe.wa.edu.au>
 * @Version:    1.0
 * @Copyright:  Brendan Kelly 2018
 *
 * History:
 * v1.0    2018-07-24
 *         Calculations and stuff
 *
 * v0.1    2018-07-24
 *         Template as provided by Adrian Gould
 *
 */

let base = 5;
let height = 6;
let length = 7;
let area = 0;

area = base * height / 2;
console.log("Triangle area: " + area)