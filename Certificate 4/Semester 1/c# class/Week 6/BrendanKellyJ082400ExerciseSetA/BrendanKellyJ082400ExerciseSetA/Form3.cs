﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace BrendanKellyJ082400ExerciseSetA
{
    public partial class Form3 : Form
    {
        bool bool_found = false;
        String[] str_doggos = { "Corgi", "Boxer", "Husky", "Beagle", "Collie" };
        public Form3()
        {
            InitializeComponent();
        }

        private void btn_dogs_Click(object sender, EventArgs e)
        {
            lst_doggos.Visible = true;
            for (int i = 0; i < str_doggos.Length; i++)
            {
                lst_doggos.Items.Add(i + 1 + "\t" + str_doggos[i]);

            }
        }

        private void btn_output_Click(object sender, EventArgs e)
        {
            for (int i = 0; i < str_doggos.Length; i++)
            {
                
                
                if (str_doggos[i] == txt_input.Text )
                {
                    lbl_output.Text = str_doggos[i] + " was found in the array";
                    bool_found = true;
                }
                if(!bool_found)
                {
                    lbl_output.Text = txt_input.Text + " was not found in the array";
                }
            }
        }
    }
    }

