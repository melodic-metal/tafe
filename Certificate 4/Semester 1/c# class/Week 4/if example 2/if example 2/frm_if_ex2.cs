﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace if_example_2
{
    public partial class frm_if_ex2 : Form
    {
        public frm_if_ex2()
        {
            InitializeComponent();
        }

        private void btn_calc_discount_Click(object sender, EventArgs e)
        {
            const double DISCOUNT_RATE = 0.1;   //10% = 0.1
            const decimal DISCOUNT_START_LEVEL = 500.0M; 
            
            decimal dec_purchase_amount, dec_discount, dec_amount_to_pay;

            // validate user entry is a number
            if (! decimal.TryParse(txt_amount_spent.Text, out dec_purchase_amount))
            {
		        MessageBox.Show("A number was not entered – please enter the amount spent again");
		        txt_amount_spent.Clear();		// since the entry is invalid remove it
		        txt_amount_spent.Focus();		// put the cursor back into the textbox for re-entry
		        return;				            // leave the event function since entry is invalid
	        }

            // determine discount
            if (dec_purchase_amount > DISCOUNT_START_LEVEL)
            {
                dec_discount = dec_purchase_amount * (decimal)DISCOUNT_RATE;
            }
            else
            {
                dec_discount = 0;
            }

            dec_amount_to_pay = dec_purchase_amount - dec_discount;

            // display outputs
            txt_discount.Text = dec_discount.ToString("C");
            txt_amount_to_pay.Text = dec_amount_to_pay.ToString("C");
        }
    }
}
