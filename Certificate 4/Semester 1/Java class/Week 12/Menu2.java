import javax.swing.*;

public class Menu2 {


   
      protected String[] entreeChoice = { "Tofu Stirfry", "Coconut Cream Pasta", "Paealla", "Salad" };
      private String menu = "";
      private int choice;
      protected char initial[] = new char[entreeChoice.length];
      
      public String displayMenu() throws MenuException {
         for (int i = 0;i < entreeChoice.length; i++) {
               menu = menu + "\n" + (i + 1) + " for " + entreeChoice[i];  
               initial[i] = entreeChoice[i].charAt(0);
         }     
      
      String input = JOptionPane.showInputDialog(null, "Type your choice" + menu);
      for(int j = 0; j< entreeChoice.length; j++) {
         if(input.charAt(0) == initial[j]) {
            throw (new MenuException(entreeChoice[j]));
         }
      }
      choice = Integer.parseInt(input);
      return(entreeChoice[choice - 1]);
      }
      
   


}