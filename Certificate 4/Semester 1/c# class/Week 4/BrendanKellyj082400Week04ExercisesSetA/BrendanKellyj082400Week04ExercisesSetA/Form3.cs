﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace BrendanKellyj082400Week04ExercisesSetA
{
    public partial class Form3 : Form
    {
        public Form3()
        {
            InitializeComponent();
        }

        private void btn_calc_Click(object sender, EventArgs e)
        {
            decimal dec_sale_price = 0, dec_cost_price = 0, dec_output = 0; // initialise cost and sale price to 0 as default

            if (!decimal.TryParse(txt_sale_price.Text, out dec_sale_price) || !decimal.TryParse(txt_cost.Text, out dec_cost_price))
            {
                MessageBox.Show("Please enter valid numbers");
                txt_cost.Clear();
                txt_sale_price.Clear();
                txt_sale_price.Focus();
                return;
            }

            dec_output = dec_sale_price - dec_cost_price; // calculate what the cost is of the item minus the cost price

            if (dec_output >= 0)
            {
                txt_output.Text = "Profit of: $" + dec_output;
            }
            else if (dec_output < 0)
            {
                txt_output.Text = "Loss of: $" + System.Math.Abs(dec_output);
            }


        }
    }
}
