﻿namespace Multi_Form_Example
{
    partial class frm_display
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btn_display_age_given_name = new System.Windows.Forms.Button();
            this.txt_age_F2 = new System.Windows.Forms.TextBox();
            this.txt_family_name_F2 = new System.Windows.Forms.TextBox();
            this.Label2 = new System.Windows.Forms.Label();
            this.Label1 = new System.Windows.Forms.Label();
            this.btn_close_form = new System.Windows.Forms.Button();
            this.btn_change_age = new System.Windows.Forms.Button();
            this.label3 = new System.Windows.Forms.Label();
            this.txt_given_name_F2 = new System.Windows.Forms.TextBox();
            this.SuspendLayout();
            // 
            // btn_display_age_given_name
            // 
            this.btn_display_age_given_name.Location = new System.Drawing.Point(32, 146);
            this.btn_display_age_given_name.Name = "btn_display_age_given_name";
            this.btn_display_age_given_name.Size = new System.Drawing.Size(113, 38);
            this.btn_display_age_given_name.TabIndex = 13;
            this.btn_display_age_given_name.Text = "Display Main Form Age and Given Name";
            this.btn_display_age_given_name.UseVisualStyleBackColor = true;
            this.btn_display_age_given_name.Click += new System.EventHandler(this.btn_display_age_Click);
            // 
            // txt_age_F2
            // 
            this.txt_age_F2.Location = new System.Drawing.Point(156, 113);
            this.txt_age_F2.Name = "txt_age_F2";
            this.txt_age_F2.Size = new System.Drawing.Size(100, 20);
            this.txt_age_F2.TabIndex = 12;
            // 
            // txt_family_name_F2
            // 
            this.txt_family_name_F2.Enabled = false;
            this.txt_family_name_F2.Location = new System.Drawing.Point(156, 42);
            this.txt_family_name_F2.Name = "txt_family_name_F2";
            this.txt_family_name_F2.Size = new System.Drawing.Size(100, 20);
            this.txt_family_name_F2.TabIndex = 11;
            // 
            // Label2
            // 
            this.Label2.AutoSize = true;
            this.Label2.Location = new System.Drawing.Point(12, 120);
            this.Label2.Name = "Label2";
            this.Label2.Size = new System.Drawing.Size(82, 13);
            this.Label2.TabIndex = 10;
            this.Label2.Text = "Update the Age";
            // 
            // Label1
            // 
            this.Label1.AutoSize = true;
            this.Label1.Location = new System.Drawing.Point(10, 45);
            this.Label1.Name = "Label1";
            this.Label1.Size = new System.Drawing.Size(135, 13);
            this.Label1.TabIndex = 9;
            this.Label1.Text = "Family Name from frm_main";
            // 
            // btn_close_form
            // 
            this.btn_close_form.Location = new System.Drawing.Point(106, 201);
            this.btn_close_form.Name = "btn_close_form";
            this.btn_close_form.Size = new System.Drawing.Size(75, 38);
            this.btn_close_form.TabIndex = 8;
            this.btn_close_form.Text = "Close this Form";
            this.btn_close_form.UseVisualStyleBackColor = true;
            this.btn_close_form.Click += new System.EventHandler(this.btn_close_form_Click);
            // 
            // btn_change_age
            // 
            this.btn_change_age.Location = new System.Drawing.Point(166, 146);
            this.btn_change_age.Name = "btn_change_age";
            this.btn_change_age.Size = new System.Drawing.Size(122, 38);
            this.btn_change_age.TabIndex = 7;
            this.btn_change_age.Text = "Accept Changed Age and Given name";
            this.btn_change_age.UseVisualStyleBackColor = true;
            this.btn_change_age.Click += new System.EventHandler(this.btn_change_age_Click);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(12, 87);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(106, 13);
            this.label3.TabIndex = 14;
            this.label3.Text = "Change Given Name";
            // 
            // txt_given_name_F2
            // 
            this.txt_given_name_F2.Location = new System.Drawing.Point(156, 80);
            this.txt_given_name_F2.Name = "txt_given_name_F2";
            this.txt_given_name_F2.Size = new System.Drawing.Size(100, 20);
            this.txt_given_name_F2.TabIndex = 15;
            // 
            // frm_display
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(300, 261);
            this.Controls.Add(this.txt_given_name_F2);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.btn_display_age_given_name);
            this.Controls.Add(this.txt_age_F2);
            this.Controls.Add(this.txt_family_name_F2);
            this.Controls.Add(this.Label2);
            this.Controls.Add(this.Label1);
            this.Controls.Add(this.btn_close_form);
            this.Controls.Add(this.btn_change_age);
            this.Name = "frm_display";
            this.Text = "frm_display";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        internal System.Windows.Forms.Button btn_display_age_given_name;
        internal System.Windows.Forms.TextBox txt_age_F2;
        internal System.Windows.Forms.TextBox txt_family_name_F2;
        internal System.Windows.Forms.Label Label2;
        internal System.Windows.Forms.Label Label1;
        internal System.Windows.Forms.Button btn_close_form;
        internal System.Windows.Forms.Button btn_change_age;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox txt_given_name_F2;
    }
}