﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace BrendanKellyJ082400Week12Exercises.Question5
{
    public partial class sub_form : Form
    {
        
        public sub_form()
        {
            InitializeComponent();
            for(int i = 0; i < start_form.names.Count; i++ )
            {
                lst_names.Items.Add(start_form.names[i]);
            }
        }

        private void btn_back_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
