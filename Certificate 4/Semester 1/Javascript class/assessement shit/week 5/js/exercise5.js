let array1 = [1,2,3,4,5,6,7,8,1,1,2,31];

function totalOfArray () {
    let i = 0, total = 0;
    let arrayLength = array.length;
    for (i = 0; i < arrayLength; i++) {
        total += array[i];
    }
    return total;

}

function meanOfArray() {
    let i = 0, mean = 0;
    let arrayLength = array.length;
    for (i = 0; i < arrayLength; i++) {
        mean += array[i];
    }
    return mean/arrayLength;
}

function checkMaxDigit(theArray) {
    return Math.max(...theArray);
}

function checkMaxDigitForLoop(theArray) {
    let i, maxDigit = 0, arrayLength = array.length;
    for (i = 0; i < arrayLength; i++)
        if (theArray[i] > maxDigit) {
            maxDigit = theArray[i];
        }
}

var array = [3, 4, 5, 23, 21, 9];
var largest = 0;

for (i=0; i<array.length; i++) {
    if (array[i]>largest) {
        console.log(largest + " " + array[i]);
        largest = array[i];
    }
}
console.log(largest);

function checkMinDigit() {
    return Math.min(...array);
}

function writeToPage() {
    let pageBody = document.getElementById("pageBody");
    pageBody.innerHTML += "<p>Total of array is: " +  totalOfArray() + "</p>";
    pageBody.innerHTML += "<p>mean of array is: " +  meanOfArray() + "</p>";
    pageBody.innerHTML += "<p>Maximum Digit of array is: " +  checkMaxDigit(array) + "</p>";
    pageBody.innerHTML += "<p>Maximum Digit of array is (with for loop): " +  checkMaxDigitForLoop(array) + "</p>";
    pageBody.innerHTML += "<p>Minimum Digit of array is: " +  checkMinDigit() + "</p>";
}

//writeToPage();