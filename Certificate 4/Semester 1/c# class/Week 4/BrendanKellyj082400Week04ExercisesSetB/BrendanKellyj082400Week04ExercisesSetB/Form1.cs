﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace BrendanKellyj082400Week04ExercisesSetB
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void btn_calc_Click(object sender, EventArgs e)
        {
            int int_mark = 0; // initialise and set to 0
            char chr_grade = ' ';
            //Boolean bool_check_grade = int.TryParse(txt_grade.Text, out int_)
            if (!int.TryParse(txt_mark.Text, out int_mark)) {
                MessageBox.Show("Please enter a valid mark/number");
                return;
            }

            if (int_mark >= 75 && int_mark <= 100)
            {
                chr_grade = 'A';
              
            }
            else if (int_mark >= 65 && int_mark <= 74)
            {
                chr_grade = 'B';
              
            }
            else if (int_mark >= 50 && int_mark <= 64)
            {
                chr_grade = 'C';
               
            }
            else if (int_mark >= 35 && int_mark <= 49)
            {
                chr_grade = 'F';
               
            }
            else
            {
                MessageBox.Show("No Grade");
            }
        

            txt_grade.Text = chr_grade.ToString();
        }
    }
}
