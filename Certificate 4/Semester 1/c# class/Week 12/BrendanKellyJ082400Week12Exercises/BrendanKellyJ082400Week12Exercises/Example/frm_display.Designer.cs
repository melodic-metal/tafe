﻿namespace BrendanKellyJ082400Week12Exercises.Example
{
    partial class frm_display
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.txt_age = new System.Windows.Forms.TextBox();
            this.txt_given = new System.Windows.Forms.TextBox();
            this.txt_family = new System.Windows.Forms.TextBox();
            this.lbl_age = new System.Windows.Forms.Label();
            this.lbl_given = new System.Windows.Forms.Label();
            this.lbl_family = new System.Windows.Forms.Label();
            this.btn_display = new System.Windows.Forms.Button();
            this.btn_accept = new System.Windows.Forms.Button();
            this.btn_close = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // txt_age
            // 
            this.txt_age.Location = new System.Drawing.Point(145, 107);
            this.txt_age.Name = "txt_age";
            this.txt_age.Size = new System.Drawing.Size(100, 20);
            this.txt_age.TabIndex = 11;
            // 
            // txt_given
            // 
            this.txt_given.Location = new System.Drawing.Point(145, 74);
            this.txt_given.Name = "txt_given";
            this.txt_given.Size = new System.Drawing.Size(100, 20);
            this.txt_given.TabIndex = 10;
            // 
            // txt_family
            // 
            this.txt_family.Location = new System.Drawing.Point(145, 41);
            this.txt_family.Name = "txt_family";
            this.txt_family.Size = new System.Drawing.Size(100, 20);
            this.txt_family.TabIndex = 9;
            // 
            // lbl_age
            // 
            this.lbl_age.AutoSize = true;
            this.lbl_age.Location = new System.Drawing.Point(29, 114);
            this.lbl_age.Name = "lbl_age";
            this.lbl_age.Size = new System.Drawing.Size(79, 13);
            this.lbl_age.TabIndex = 8;
            this.lbl_age.Text = "Enter Your Age";
            // 
            // lbl_given
            // 
            this.lbl_given.AutoSize = true;
            this.lbl_given.Location = new System.Drawing.Point(29, 81);
            this.lbl_given.Name = "lbl_given";
            this.lbl_given.Size = new System.Drawing.Size(94, 13);
            this.lbl_given.TabIndex = 7;
            this.lbl_given.Text = "Enter Given Name";
            // 
            // lbl_family
            // 
            this.lbl_family.AutoSize = true;
            this.lbl_family.Location = new System.Drawing.Point(29, 48);
            this.lbl_family.Name = "lbl_family";
            this.lbl_family.Size = new System.Drawing.Size(95, 13);
            this.lbl_family.TabIndex = 6;
            this.lbl_family.Text = "Enter Family Name";
            // 
            // btn_display
            // 
            this.btn_display.Location = new System.Drawing.Point(33, 148);
            this.btn_display.Name = "btn_display";
            this.btn_display.Size = new System.Drawing.Size(75, 54);
            this.btn_display.TabIndex = 12;
            this.btn_display.Text = "display info from frm_main";
            this.btn_display.UseVisualStyleBackColor = true;
            this.btn_display.Click += new System.EventHandler(this.btn_display_Click);
            // 
            // btn_accept
            // 
            this.btn_accept.Location = new System.Drawing.Point(145, 148);
            this.btn_accept.Name = "btn_accept";
            this.btn_accept.Size = new System.Drawing.Size(75, 54);
            this.btn_accept.TabIndex = 13;
            this.btn_accept.Text = "Accept Changed info\r\n";
            this.btn_accept.UseVisualStyleBackColor = true;
            this.btn_accept.Click += new System.EventHandler(this.btn_accept_Click);
            // 
            // btn_close
            // 
            this.btn_close.Location = new System.Drawing.Point(97, 208);
            this.btn_close.Name = "btn_close";
            this.btn_close.Size = new System.Drawing.Size(75, 23);
            this.btn_close.TabIndex = 14;
            this.btn_close.Text = "Close\r\n";
            this.btn_close.UseVisualStyleBackColor = true;
            this.btn_close.Click += new System.EventHandler(this.btn_close_Click);
            // 
            // frm_display
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(284, 261);
            this.Controls.Add(this.btn_close);
            this.Controls.Add(this.btn_accept);
            this.Controls.Add(this.btn_display);
            this.Controls.Add(this.txt_age);
            this.Controls.Add(this.txt_given);
            this.Controls.Add(this.txt_family);
            this.Controls.Add(this.lbl_age);
            this.Controls.Add(this.lbl_given);
            this.Controls.Add(this.lbl_family);
            this.Name = "frm_display";
            this.Text = "frm_display";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox txt_age;
        private System.Windows.Forms.TextBox txt_given;
        private System.Windows.Forms.Label lbl_age;
        private System.Windows.Forms.Label lbl_given;
        private System.Windows.Forms.Label lbl_family;
        private System.Windows.Forms.Button btn_display;
        internal System.Windows.Forms.TextBox txt_family;
        private System.Windows.Forms.Button btn_accept;
        private System.Windows.Forms.Button btn_close;
    }
}