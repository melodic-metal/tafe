﻿namespace BrendanKellyJ082400Week05ExercisesSetB
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lst_table = new System.Windows.Forms.ListBox();
            this.btn_twos = new System.Windows.Forms.Button();
            this.btn_threes = new System.Windows.Forms.Button();
            this.btn_ones = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // lst_table
            // 
            this.lst_table.FormattingEnabled = true;
            this.lst_table.Location = new System.Drawing.Point(73, 85);
            this.lst_table.Name = "lst_table";
            this.lst_table.Size = new System.Drawing.Size(120, 95);
            this.lst_table.TabIndex = 1;
            // 
            // btn_twos
            // 
            this.btn_twos.Location = new System.Drawing.Point(106, 37);
            this.btn_twos.Name = "btn_twos";
            this.btn_twos.Size = new System.Drawing.Size(75, 23);
            this.btn_twos.TabIndex = 2;
            this.btn_twos.Text = "Up By 2\'s";
            this.btn_twos.UseVisualStyleBackColor = true;
            this.btn_twos.Click += new System.EventHandler(this.btn_twos_Click);
            // 
            // btn_threes
            // 
            this.btn_threes.Location = new System.Drawing.Point(197, 37);
            this.btn_threes.Name = "btn_threes";
            this.btn_threes.Size = new System.Drawing.Size(75, 23);
            this.btn_threes.TabIndex = 3;
            this.btn_threes.Text = "Down By 3\'s";
            this.btn_threes.UseVisualStyleBackColor = true;
            this.btn_threes.Click += new System.EventHandler(this.btn_threes_Click);
            // 
            // btn_ones
            // 
            this.btn_ones.Location = new System.Drawing.Point(13, 36);
            this.btn_ones.Name = "btn_ones";
            this.btn_ones.Size = new System.Drawing.Size(75, 23);
            this.btn_ones.TabIndex = 4;
            this.btn_ones.Text = "Up By 1\'s";
            this.btn_ones.UseVisualStyleBackColor = true;
            this.btn_ones.Click += new System.EventHandler(this.btn_ones_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(284, 261);
            this.Controls.Add(this.btn_ones);
            this.Controls.Add(this.btn_threes);
            this.Controls.Add(this.btn_twos);
            this.Controls.Add(this.lst_table);
            this.Name = "Form1";
            this.Text = "Form1";
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.ListBox lst_table;
        private System.Windows.Forms.Button btn_twos;
        private System.Windows.Forms.Button btn_threes;
        private System.Windows.Forms.Button btn_ones;
    }
}

