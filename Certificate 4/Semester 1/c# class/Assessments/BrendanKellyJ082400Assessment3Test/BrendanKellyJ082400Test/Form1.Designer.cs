﻿namespace BrendanKellyJ082400Test
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lbl_children = new System.Windows.Forms.Label();
            this.lbl_adults = new System.Windows.Forms.Label();
            this.txt_adults = new System.Windows.Forms.TextBox();
            this.txt_children = new System.Windows.Forms.TextBox();
            this.btn_ticket_cost = new System.Windows.Forms.Button();
            this.lbl_ticket_cost = new System.Windows.Forms.Label();
            this.txt_ticket_cost = new System.Windows.Forms.TextBox();
            this.lbl_amount_from = new System.Windows.Forms.Label();
            this.txt_amount_from = new System.Windows.Forms.TextBox();
            this.btn_change = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this.txt_change_back = new System.Windows.Forms.TextBox();
            this.grp_money = new System.Windows.Forms.GroupBox();
            this.grp_tickets = new System.Windows.Forms.GroupBox();
            this.grp_money.SuspendLayout();
            this.grp_tickets.SuspendLayout();
            this.SuspendLayout();
            // 
            // lbl_children
            // 
            this.lbl_children.AutoSize = true;
            this.lbl_children.Location = new System.Drawing.Point(63, 72);
            this.lbl_children.Name = "lbl_children";
            this.lbl_children.Size = new System.Drawing.Size(99, 13);
            this.lbl_children.TabIndex = 0;
            this.lbl_children.Text = "Number Of Children";
            // 
            // lbl_adults
            // 
            this.lbl_adults.AutoSize = true;
            this.lbl_adults.Location = new System.Drawing.Point(63, 44);
            this.lbl_adults.Name = "lbl_adults";
            this.lbl_adults.Size = new System.Drawing.Size(90, 13);
            this.lbl_adults.TabIndex = 1;
            this.lbl_adults.Text = "Number Of Adults";
            // 
            // txt_adults
            // 
            this.txt_adults.Location = new System.Drawing.Point(112, 19);
            this.txt_adults.Name = "txt_adults";
            this.txt_adults.Size = new System.Drawing.Size(100, 20);
            this.txt_adults.TabIndex = 2;
            // 
            // txt_children
            // 
            this.txt_children.Location = new System.Drawing.Point(112, 47);
            this.txt_children.Name = "txt_children";
            this.txt_children.Size = new System.Drawing.Size(100, 20);
            this.txt_children.TabIndex = 3;
            // 
            // btn_ticket_cost
            // 
            this.btn_ticket_cost.Location = new System.Drawing.Point(97, 104);
            this.btn_ticket_cost.Name = "btn_ticket_cost";
            this.btn_ticket_cost.Size = new System.Drawing.Size(127, 23);
            this.btn_ticket_cost.TabIndex = 4;
            this.btn_ticket_cost.Text = "Calculate Ticket Cost";
            this.btn_ticket_cost.UseVisualStyleBackColor = true;
            this.btn_ticket_cost.Click += new System.EventHandler(this.btn_ticket_cost_Click);
            // 
            // lbl_ticket_cost
            // 
            this.lbl_ticket_cost.AutoSize = true;
            this.lbl_ticket_cost.Location = new System.Drawing.Point(66, 146);
            this.lbl_ticket_cost.Name = "lbl_ticket_cost";
            this.lbl_ticket_cost.Size = new System.Drawing.Size(61, 13);
            this.lbl_ticket_cost.TabIndex = 5;
            this.lbl_ticket_cost.Text = "Ticket Cost";
            // 
            // txt_ticket_cost
            // 
            this.txt_ticket_cost.Location = new System.Drawing.Point(156, 146);
            this.txt_ticket_cost.Name = "txt_ticket_cost";
            this.txt_ticket_cost.Size = new System.Drawing.Size(100, 20);
            this.txt_ticket_cost.TabIndex = 6;
            // 
            // lbl_amount_from
            // 
            this.lbl_amount_from.AutoSize = true;
            this.lbl_amount_from.Location = new System.Drawing.Point(66, 233);
            this.lbl_amount_from.Name = "lbl_amount_from";
            this.lbl_amount_from.Size = new System.Drawing.Size(93, 13);
            this.lbl_amount_from.TabIndex = 7;
            this.lbl_amount_from.Text = "Amount From Cust";
            // 
            // txt_amount_from
            // 
            this.txt_amount_from.Location = new System.Drawing.Point(112, 19);
            this.txt_amount_from.Name = "txt_amount_from";
            this.txt_amount_from.Size = new System.Drawing.Size(100, 20);
            this.txt_amount_from.TabIndex = 8;
            // 
            // btn_change
            // 
            this.btn_change.Location = new System.Drawing.Point(41, 52);
            this.btn_change.Name = "btn_change";
            this.btn_change.Size = new System.Drawing.Size(127, 23);
            this.btn_change.TabIndex = 9;
            this.btn_change.Text = "Calculate Change";
            this.btn_change.UseVisualStyleBackColor = true;
            this.btn_change.Click += new System.EventHandler(this.btn_change_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(12, 92);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(91, 13);
            this.label2.TabIndex = 10;
            this.label2.Text = "Customer Change";
            // 
            // txt_change_back
            // 
            this.txt_change_back.Location = new System.Drawing.Point(112, 89);
            this.txt_change_back.Name = "txt_change_back";
            this.txt_change_back.Size = new System.Drawing.Size(100, 20);
            this.txt_change_back.TabIndex = 11;
            // 
            // grp_money
            // 
            this.grp_money.Controls.Add(this.btn_change);
            this.grp_money.Controls.Add(this.label2);
            this.grp_money.Controls.Add(this.txt_change_back);
            this.grp_money.Controls.Add(this.txt_amount_from);
            this.grp_money.Location = new System.Drawing.Point(56, 211);
            this.grp_money.Name = "grp_money";
            this.grp_money.Size = new System.Drawing.Size(218, 129);
            this.grp_money.TabIndex = 12;
            this.grp_money.TabStop = false;
            this.grp_money.Text = "Money";
            // 
            // grp_tickets
            // 
            this.grp_tickets.Controls.Add(this.txt_adults);
            this.grp_tickets.Controls.Add(this.txt_children);
            this.grp_tickets.Location = new System.Drawing.Point(56, 22);
            this.grp_tickets.Name = "grp_tickets";
            this.grp_tickets.Size = new System.Drawing.Size(218, 158);
            this.grp_tickets.TabIndex = 13;
            this.grp_tickets.TabStop = false;
            this.grp_tickets.Text = "Tickets";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(304, 349);
            this.Controls.Add(this.lbl_amount_from);
            this.Controls.Add(this.txt_ticket_cost);
            this.Controls.Add(this.lbl_ticket_cost);
            this.Controls.Add(this.btn_ticket_cost);
            this.Controls.Add(this.lbl_adults);
            this.Controls.Add(this.lbl_children);
            this.Controls.Add(this.grp_money);
            this.Controls.Add(this.grp_tickets);
            this.Name = "Form1";
            this.Text = "West Coast Amusement Park";
            this.grp_money.ResumeLayout(false);
            this.grp_money.PerformLayout();
            this.grp_tickets.ResumeLayout(false);
            this.grp_tickets.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lbl_children;
        private System.Windows.Forms.Label lbl_adults;
        private System.Windows.Forms.TextBox txt_adults;
        private System.Windows.Forms.TextBox txt_children;
        private System.Windows.Forms.Button btn_ticket_cost;
        private System.Windows.Forms.Label lbl_ticket_cost;
        private System.Windows.Forms.TextBox txt_ticket_cost;
        private System.Windows.Forms.Label lbl_amount_from;
        private System.Windows.Forms.TextBox txt_amount_from;
        private System.Windows.Forms.Button btn_change;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox txt_change_back;
        private System.Windows.Forms.GroupBox grp_money;
        private System.Windows.Forms.GroupBox grp_tickets;
    }
}

