﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace BrendanKellyJ082400Week12Exercises.Question3
{
    public partial class large_car_form : Form
    {
        const int CAR_PRICE = 40;
        public large_car_form()
        {
            InitializeComponent();
        }

        private void btn_calc_Click(object sender, EventArgs e)
        {
            decimal sub_total;
            if (!int.TryParse(txt_hire.Text, out int int_hire_days))
            {
                MessageBox.Show("Please check input");
            }
            sub_total = CAR_PRICE * int_hire_days;
            start_form.dec_total = sub_total;
            this.Close();
        }
    }
}
