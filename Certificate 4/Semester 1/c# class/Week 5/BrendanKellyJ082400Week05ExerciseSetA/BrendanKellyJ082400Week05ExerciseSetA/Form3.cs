﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace BrendanKellyJ082400Week05ExerciseSetA
{
    public partial class Form3 : Form
    {
        public Form3()
        {
            InitializeComponent();
        }

        private void btn_calc_Click(object sender, EventArgs e)
        {
            int i = 0, int_investment_amount, int_temp = 0;
            decimal dec_interest;
            float flt_interest = 0, flt_temp;
            int.TryParse(txt_investment.Text, out int_investment_amount);
            decimal.TryParse(txt_interest.Text, out dec_interest);
            lst_table.Items.Add("Year\t\tInvestment");
            while (i <= 10)
            {
                flt_interest = int_investment_amount * (float)dec_interest * i;
                flt_interest = flt_interest + int_investment_amount;
                lst_table.Items.Add(i.ToString() + "\t\t" + flt_interest.ToString());
                i++;
            }
        }
    }
}
