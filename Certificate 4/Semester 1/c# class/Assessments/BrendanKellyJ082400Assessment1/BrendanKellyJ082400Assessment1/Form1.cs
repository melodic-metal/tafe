﻿// Date: 30/08/2018
// Programmer: Brendan Kelly
// Purpose: Unit conversions for assessment 1

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace BrendanKellyJ082400Assessment1
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void btn_exit_Click(object sender, EventArgs e)
        {   
            //close the application
            Application.Exit();
        }

        private void btn_inches_Click(object sender, EventArgs e)
        {
            const float INCHES_IN_CM = 2.54F; //declare constant value for inches in a cm
            float flt_output;
            if (!int.TryParse(txt_input.Text, out int int_user_input)) 
            {
                //check whether input is a valid integer
                MessageBox.Show("Please check that you have entered\na valid integer in Input field");
            }
            flt_output = int_user_input * INCHES_IN_CM; // perform the calulation to check how many inches are in a cm
            lbl_before.Text = "Inches"; //change text in label
            lbl_after.Text = "cm"; //change text in label
            txt_output.Text = flt_output.ToString(); //set the output box to have the text from flt_output
            show_labels(true); // show the labels on the form
        }

        private void btn_feet_Click(object sender, EventArgs e)
        {
            const float FEET_IN_INCHES = 0.3048F; //declare constant value for feet in inches
            float flt_output; 
            if (!int.TryParse(txt_input.Text, out int int_user_input))
            {
                //check whether input is a valid integer
                MessageBox.Show("Please check that you have entered\na valid integer in Input field");
            }
            flt_output = int_user_input * FEET_IN_INCHES; // perform the calulation
            lbl_before.Text = "feet"; //change text in label
            lbl_after.Text = "metres"; //change text in label
            txt_output.Text = flt_output.ToString(); //set the output box to have the text from flt_output
            show_labels(true); // show the labels on the form
        }

        private void btn_yards_Click(object sender, EventArgs e)
        {
            const float YARDS_IN_METRE = 0.9144F; //declare constant value for yards in metres
            float flt_output;
            if (!int.TryParse(txt_input.Text, out int int_user_input))
            {
                //check whether input is a valid integer
                MessageBox.Show("Please check that you have entered\na valid integer in Input field");
            }
            flt_output = int_user_input * YARDS_IN_METRE; // perform the calulation
            lbl_before.Text = "yard"; //change text in label
            lbl_after.Text = "metres"; //change text in label
            txt_output.Text = flt_output.ToString(); // set the output box to have the text from flt_output
            show_labels(true); // show the labels on the form
        }

        private void btn_miles_Click(object sender, EventArgs e)
        {
            const float MILE_IN_KM = 1.606F; //declare constant value for miles in km 
            float flt_output;
            if (!int.TryParse(txt_input.Text, out int int_user_input))
            {
                //check whether input is a valid integer
                MessageBox.Show("Please check that you have entered\na valid integer in Input field");
                return;
            }
            flt_output = int_user_input * MILE_IN_KM; // perform the calulation
            lbl_before.Text = "miles"; //change text in label
            lbl_after.Text = "kilometres"; //change text in label
            txt_output.Text = flt_output.ToString(); // set the output box to have the text from flt_output
            show_labels(true); // show the labels on the form
        }

        private void txt_input_Enter(object sender, EventArgs e)
            {
            // function to disable labels when input textbox is entered
            show_labels(false);
        }

        protected override bool ProcessCmdKey(ref Message msg, Keys keyData)
        {
            // this function closes the form when escape is pressed
            if (keyData == Keys.Escape)
            {
                this.Close();
                return true;
            }
            return base.ProcessCmdKey(ref msg, keyData);
        }

        private void show_labels(bool a)
        {
            // function to either show or hide the labels on the form
            lbl_before.Visible = a;
            lbl_after.Visible = a;
            lbl_equals.Visible = a;
            txt_output.Visible = a;
        }

    }
}
