﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace BrendanKellyJ082400ExerciseSetB
{

    public partial class Form1 : Form
    {
        String[] str_magazines = { "time", "australian geographic", "gourmet", "dolly", "wheels" };
        double[] dbl_magazine_costs = { 4.00, 6.50, 5.00, 4.50, 3.95 };
        
        public Form1()
        {
            InitializeComponent();
        }

        private void btn_calc_Click(object sender, EventArgs e)
        {
            bool bool_magazine_found = false;
            txt_output.Clear();
            for (int i = 0; i < str_magazines.Length; i++)
            {
                if (txt_input.Text.ToLower() == str_magazines[i])
                {
                    txt_output.Text = dbl_magazine_costs[i].ToString("c");
                    bool_magazine_found = true;
                }
                if (!bool_magazine_found)
                {
                    txt_output.Text = "Not Found";
                }

            }

        }

        private void btn_price_under_Click(object sender, EventArgs e)
        {
            lst_prices_under.Items.Clear();
            if(!double.TryParse(txt_prices_under.Text, out double dbl_prices_under))
            {
                MessageBox.Show("Please Enter a valid number");
            }

            for (int i = 0; i < dbl_magazine_costs.Length; i++)
            {
                if (dbl_prices_under >= dbl_magazine_costs[i] )
                {
                    lst_prices_under.Items.Add(str_magazines[i]);
                }
            }
        }
    }
}
