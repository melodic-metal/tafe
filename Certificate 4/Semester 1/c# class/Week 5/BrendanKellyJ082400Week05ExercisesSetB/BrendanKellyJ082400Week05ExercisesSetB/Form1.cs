﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace BrendanKellyJ082400Week05ExercisesSetB
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void btn_ones_Click(object sender, EventArgs e)
        {
            lst_table.Items.Clear();
            for(int i = 0; i <= 15; i++)
            {
                
                lst_table.Items.Add(i);
            }
        }

        private void btn_twos_Click(object sender, EventArgs e)
        {
            lst_table.Items.Clear();
            for (int i = 0; i <= 15; i = i + 2)
            {

                lst_table.Items.Add(i);
            }
        }

        private void btn_threes_Click(object sender, EventArgs e)
        {
            lst_table.Items.Clear();
            for (int i = 15; i >= 0; i = i - 3)
            {

                lst_table.Items.Add(i);
            }
        }
    }
}
