// Date: 1/09/2018
// Programmer: Brendan Kelly
// Purpose: class to create tax return objects

public class EmployeesTaxReturn {

   private int taxNumber, annualIncome, post;
   private String lastName, firstName, address;
   private int validReason;
   private boolean isValid = false; // declare all the required variables
   
   public EmployeesTaxReturn(int taxNum, String last, String first, String streetAddress, int income) {
      //constructor with arguments for each of the required information
      taxNumber = taxNum;
      lastName = last;
      firstName = first;
      address = streetAddress;
      annualIncome = income;

      // assign constructor variables to class variables
   }
   
   public void displayInformation() {
         // display information
         System.out.println("Name: " + firstName + " " + lastName);
         System.out.println("TFN: " + taxNumber);
         System.out.println("Address: " + address);
         System.out.println("Income: $" + annualIncome);
         System.out.println();
      }
   
   
   public int getTaxNumber() {
      return taxNumber;
   }
   
   public String getLastName() {
      return lastName;
   }
   public String getFirstName() {
      return firstName;
   }
   public String getAddress() {
      return address;
   }
   
   public int getAnnualIncome() {
      return annualIncome;
   }
   
   // ^ return information

}