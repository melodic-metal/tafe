public class TwelveInts {

   public static void main(String[] args) {
   
      int[] twelveInts = { 1,2,3,4,5,6,7,8,9,10,11,12 };
      
      for (int i = 0; i < twelveInts.length; i++) {
         System.out.print(twelveInts[i] + " ");
      }   
      System.out.println();
      for (int i = twelveInts.length - 1; i >= 0; i--) {
         System.out.print(twelveInts[i] + " ");
      }   
   
   }

}