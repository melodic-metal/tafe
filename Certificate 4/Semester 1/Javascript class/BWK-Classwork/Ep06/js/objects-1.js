"use strict";

let person = {
  "name": "George",
  "occupation" : "Truckie",
  "locations" : {
      "main" : "Perth",
      "secondary": "Joondalup",
      "ternary": null,
  },
    "height": 1.82,
};


let outputArea = document.getElementById("output");
console.log(person.occupation);

for (let key in person) {
    outputArea.innerHTML += key + " --> " + person[key] + "<br/>";
}