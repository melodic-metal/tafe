﻿namespace BrendanKellyj082400Week04ExercisesSetA
{
    partial class Form5
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lbl_num_days = new System.Windows.Forms.Label();
            this.txt_days = new System.Windows.Forms.TextBox();
            this.txt_start_odo = new System.Windows.Forms.TextBox();
            this.lbl_start_odo = new System.Windows.Forms.Label();
            this.lbl_cost = new System.Windows.Forms.Label();
            this.txt_cost = new System.Windows.Forms.TextBox();
            this.btn_calc = new System.Windows.Forms.Button();
            this.lbl_end_odo = new System.Windows.Forms.Label();
            this.txt_end_odo = new System.Windows.Forms.TextBox();
            this.SuspendLayout();
            // 
            // lbl_num_days
            // 
            this.lbl_num_days.AutoSize = true;
            this.lbl_num_days.Location = new System.Drawing.Point(72, 44);
            this.lbl_num_days.Name = "lbl_num_days";
            this.lbl_num_days.Size = new System.Drawing.Size(31, 13);
            this.lbl_num_days.TabIndex = 0;
            this.lbl_num_days.Text = "Days";
            // 
            // txt_days
            // 
            this.txt_days.Location = new System.Drawing.Point(137, 37);
            this.txt_days.Name = "txt_days";
            this.txt_days.Size = new System.Drawing.Size(100, 20);
            this.txt_days.TabIndex = 1;
            // 
            // txt_start_odo
            // 
            this.txt_start_odo.Location = new System.Drawing.Point(137, 79);
            this.txt_start_odo.Name = "txt_start_odo";
            this.txt_start_odo.Size = new System.Drawing.Size(100, 20);
            this.txt_start_odo.TabIndex = 2;
            // 
            // lbl_start_odo
            // 
            this.lbl_start_odo.AutoSize = true;
            this.lbl_start_odo.Location = new System.Drawing.Point(32, 86);
            this.lbl_start_odo.Name = "lbl_start_odo";
            this.lbl_start_odo.Size = new System.Drawing.Size(78, 13);
            this.lbl_start_odo.TabIndex = 3;
            this.lbl_start_odo.Text = "Start Odometer";
            // 
            // lbl_cost
            // 
            this.lbl_cost.AutoSize = true;
            this.lbl_cost.Location = new System.Drawing.Point(48, 165);
            this.lbl_cost.Name = "lbl_cost";
            this.lbl_cost.Size = new System.Drawing.Size(55, 13);
            this.lbl_cost.TabIndex = 4;
            this.lbl_cost.Text = "Total Cost";
            // 
            // txt_cost
            // 
            this.txt_cost.Enabled = false;
            this.txt_cost.Location = new System.Drawing.Point(137, 162);
            this.txt_cost.Name = "txt_cost";
            this.txt_cost.Size = new System.Drawing.Size(100, 20);
            this.txt_cost.TabIndex = 5;
            // 
            // btn_calc
            // 
            this.btn_calc.Location = new System.Drawing.Point(99, 215);
            this.btn_calc.Name = "btn_calc";
            this.btn_calc.Size = new System.Drawing.Size(75, 23);
            this.btn_calc.TabIndex = 6;
            this.btn_calc.Text = "Calculate";
            this.btn_calc.UseVisualStyleBackColor = true;
            this.btn_calc.Click += new System.EventHandler(this.btn_calc_Click);
            // 
            // lbl_end_odo
            // 
            this.lbl_end_odo.AutoSize = true;
            this.lbl_end_odo.Location = new System.Drawing.Point(32, 124);
            this.lbl_end_odo.Name = "lbl_end_odo";
            this.lbl_end_odo.Size = new System.Drawing.Size(75, 13);
            this.lbl_end_odo.TabIndex = 8;
            this.lbl_end_odo.Text = "End Odometer";
            // 
            // txt_end_odo
            // 
            this.txt_end_odo.Location = new System.Drawing.Point(137, 117);
            this.txt_end_odo.Name = "txt_end_odo";
            this.txt_end_odo.Size = new System.Drawing.Size(100, 20);
            this.txt_end_odo.TabIndex = 7;
            // 
            // Form5
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(284, 261);
            this.Controls.Add(this.lbl_end_odo);
            this.Controls.Add(this.txt_end_odo);
            this.Controls.Add(this.btn_calc);
            this.Controls.Add(this.txt_cost);
            this.Controls.Add(this.lbl_cost);
            this.Controls.Add(this.lbl_start_odo);
            this.Controls.Add(this.txt_start_odo);
            this.Controls.Add(this.txt_days);
            this.Controls.Add(this.lbl_num_days);
            this.Name = "Form5";
            this.Text = "Rent a Heap";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lbl_num_days;
        private System.Windows.Forms.TextBox txt_days;
        private System.Windows.Forms.TextBox txt_start_odo;
        private System.Windows.Forms.Label lbl_start_odo;
        private System.Windows.Forms.Label lbl_cost;
        private System.Windows.Forms.TextBox txt_cost;
        private System.Windows.Forms.Button btn_calc;
        private System.Windows.Forms.Label lbl_end_odo;
        private System.Windows.Forms.TextBox txt_end_odo;
    }
}