﻿namespace BrendanKellyJ082400Week12Exercises.Example
{
    partial class frm_main
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lbl_family = new System.Windows.Forms.Label();
            this.lbl_given = new System.Windows.Forms.Label();
            this.lbl_age = new System.Windows.Forms.Label();
            this.txt_family = new System.Windows.Forms.TextBox();
            this.txt_given = new System.Windows.Forms.TextBox();
            this.txt_age = new System.Windows.Forms.TextBox();
            this.btn_display = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // lbl_family
            // 
            this.lbl_family.AutoSize = true;
            this.lbl_family.Location = new System.Drawing.Point(31, 51);
            this.lbl_family.Name = "lbl_family";
            this.lbl_family.Size = new System.Drawing.Size(95, 13);
            this.lbl_family.TabIndex = 0;
            this.lbl_family.Text = "Enter Family Name";
            // 
            // lbl_given
            // 
            this.lbl_given.AutoSize = true;
            this.lbl_given.Location = new System.Drawing.Point(31, 84);
            this.lbl_given.Name = "lbl_given";
            this.lbl_given.Size = new System.Drawing.Size(94, 13);
            this.lbl_given.TabIndex = 1;
            this.lbl_given.Text = "Enter Given Name";
            // 
            // lbl_age
            // 
            this.lbl_age.AutoSize = true;
            this.lbl_age.Location = new System.Drawing.Point(31, 117);
            this.lbl_age.Name = "lbl_age";
            this.lbl_age.Size = new System.Drawing.Size(79, 13);
            this.lbl_age.TabIndex = 2;
            this.lbl_age.Text = "Enter Your Age";
            // 
            // txt_family
            // 
            this.txt_family.Location = new System.Drawing.Point(147, 44);
            this.txt_family.Name = "txt_family";
            this.txt_family.Size = new System.Drawing.Size(100, 20);
            this.txt_family.TabIndex = 3;
            // 
            // txt_given
            // 
            this.txt_given.Location = new System.Drawing.Point(147, 77);
            this.txt_given.Name = "txt_given";
            this.txt_given.Size = new System.Drawing.Size(100, 20);
            this.txt_given.TabIndex = 4;
            // 
            // txt_age
            // 
            this.txt_age.Location = new System.Drawing.Point(147, 110);
            this.txt_age.Name = "txt_age";
            this.txt_age.Size = new System.Drawing.Size(100, 20);
            this.txt_age.TabIndex = 5;
            // 
            // btn_display
            // 
            this.btn_display.Location = new System.Drawing.Point(5, 157);
            this.btn_display.Name = "btn_display";
            this.btn_display.Size = new System.Drawing.Size(280, 92);
            this.btn_display.TabIndex = 6;
            this.btn_display.Text = "Open Second Form";
            this.btn_display.UseVisualStyleBackColor = true;
            this.btn_display.Click += new System.EventHandler(this.btn_display_Click);
            // 
            // frm_main
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(284, 261);
            this.Controls.Add(this.btn_display);
            this.Controls.Add(this.txt_age);
            this.Controls.Add(this.txt_given);
            this.Controls.Add(this.txt_family);
            this.Controls.Add(this.lbl_age);
            this.Controls.Add(this.lbl_given);
            this.Controls.Add(this.lbl_family);
            this.Name = "frm_main";
            this.Text = "frm_main";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lbl_family;
        private System.Windows.Forms.Label lbl_given;
        private System.Windows.Forms.Label lbl_age;
        private System.Windows.Forms.TextBox txt_family;
        private System.Windows.Forms.TextBox txt_given;
        private System.Windows.Forms.TextBox txt_age;
        private System.Windows.Forms.Button btn_display;
    }
}