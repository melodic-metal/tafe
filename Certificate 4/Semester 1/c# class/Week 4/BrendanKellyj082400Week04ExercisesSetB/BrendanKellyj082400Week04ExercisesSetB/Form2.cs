﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace BrendanKellyj082400Week04ExercisesSetB
{
    public partial class Form2 : Form
    {
        public Form2()
        {
            InitializeComponent();
        }

        private void btn_clear_Click(object sender, EventArgs e)
        {
            txt_input.Clear();
            txt_output.Clear();
            txt_input.Focus();
        }

        private void btn_calc_Click(object sender, EventArgs e)
        {
            double dec_percent = 0, dbl_amount = 0, dbl_total = 0, dbl_discount_amount = 0;

            if (!double.TryParse(txt_input.Text, out dbl_amount))
            {
                MessageBox.Show("Please check input");
                return;
            
            }

            if (dbl_amount < 60)
            {
                dec_percent = .25;

            }
            else if (dbl_amount >= 60 && dbl_amount < 100)
            {
                dec_percent = .3;
            }
            else if (dbl_amount >= 100 && dbl_amount < 200)
            {
                dec_percent = .35;
            }
            else if (dbl_amount >= 200)
            {
                dec_percent = .4;
            }

            dbl_discount_amount = dbl_amount * dec_percent;
            txt_output.Text = dbl_discount_amount.ToString();

        }
    }
}
