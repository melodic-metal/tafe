// Date: 13/09/2018
// Programmer: Brendan Kelly
// Purpose: Demoing the DinnerParty class

import java.util.*;

public class UseDinnerParty {

   public static void main(String[] args) {
   
      int guests, choice;
      Party aParty = new Party();
      DinnerParty aDinnerParty = new DinnerParty();
      Scanner keyboard = new Scanner(System.in);
      
      System.out.println("Enter number of guests");
      guests = keyboard.nextInt();
      aParty.setGuests(guests);
      System.out.println("The party has " + aParty.getGuests() + " guests");
      aParty.displayInvitation();
      
      System.out.println("Enter number of guests for dinner party >> ");
      guests = keyboard.nextInt();
      aDinnerParty.setGuests(guests);
      
      System.out.println("Enter menu option -- 1 for tofu or 2 for rice");
      choice = keyboard.nextInt();
      aDinnerParty.setDinnerChoice(choice);
      System.out.println("The dinner party has " + aDinnerParty.getGuests() + " guests");
      System.out.println("The menu option is: " + aDinnerParty.getDinnerChoice());
      
      aDinnerParty.displayInvitation();
   }
   
   

}