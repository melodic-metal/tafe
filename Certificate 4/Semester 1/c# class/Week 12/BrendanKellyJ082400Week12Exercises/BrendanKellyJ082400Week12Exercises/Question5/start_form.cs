﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace BrendanKellyJ082400Week12Exercises.Question5
{
    public partial class start_form : Form
    {
        public static List<String> names = new List<String>();
        public start_form()
        {
            InitializeComponent();
        }

        private void btn_add_Click(object sender, EventArgs e)
        {
            names.Add(txt_input.Text);
            txt_input.Clear();
            txt_input.Focus();
            
   
        }

        private void btn_show_Click(object sender, EventArgs e)
        {
            sub_form sub_form = new sub_form();
            sub_form.ShowDialog();
        }
    }
}
