let array = [1,6,4,3,6,8,23,3,6,7,34,435,345,123,1];

function isArrayEmpty() {
    if (array.length  == 0) {
        return true;
    }
    else {
        return false;
    }
}

function writeToPage() {
    let pageBody = document.getElementById("pageBody");
    if (isArrayEmpty() == true) {
        pageBody.innerHTML = "<p> Array is empty </p>";
    }
    else {
        pageBody.innerHTML = "<p> Array length is " + array.length + "</p>";
    }
}
