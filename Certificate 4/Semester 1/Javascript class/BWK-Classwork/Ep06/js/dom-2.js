"use strict";

showName();


function getName(elementID) {

    let theNameControl = document.getElementById(elementID);
    console.log(theNameControl);
    let theName = theNameControl.value;
    return theName;
}

function showName() {
    let resultsArea = document.getElementById("resultsArea")
    document.getElementById("go").addEventListener(
        "click", function (event) {
            ///alert("Name: " + getName("yourName"))
            let theName = getName("yourName");
            if (theName>"") {
                resultsArea.innerHTML = "<p>Name: " + getName("yourName") + "</p>";
            }
            else {
                resultsArea.innerHTML = "<p> Please Enter a name</p>";
            }
        }
    );
    stopDefault("go");
    alert("Name: " + getName("yourName"))
}

function stopDefault(elementID) {
    document.getElementById(elementID).addEventListener("click", function (event) {
            event.preventDefault()
        }
    );
}
