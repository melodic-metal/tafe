﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

// Programmer    : Brian
// Date          : February 2014
// Purpose       : To calculate discount and amount to pay when discount of 10% is given for
//                 purchases above $500  (Example program on the use of the if statement)

namespace if_example_1
{
    public partial class frm_if_ex1 : Form
    {
        
        
        public frm_if_ex1()
        {
            InitializeComponent();
        }

        private void btn_calc_discount_Click(object sender, EventArgs e)
        {
            const double DISCOUNT_RATE = 0.1;   //10% = 0.1
            const decimal DISCOUNT_START_LEVEL = 500.0M; 
            
            decimal dec_purchase_amount, dec_discount, dec_amount_to_pay;
            
            // convert text box input to a number
            dec_purchase_amount = decimal.Parse(txt_amount_spent.Text);

            // determine discount
            if (dec_purchase_amount > DISCOUNT_START_LEVEL)
            {
                dec_discount = dec_purchase_amount * (decimal)DISCOUNT_RATE;
            }
            else
            {
                dec_discount = 0;
            }

            dec_amount_to_pay = dec_purchase_amount - dec_discount;

            // display outputs
            txt_discount.Text = dec_discount.ToString("C");
            txt_amount_to_pay.Text = dec_amount_to_pay.ToString("C");
        }

        private void txt_amount_spent_Enter(object sender, EventArgs e)
        {
            txt_amount_spent.Clear();
            txt_discount.Clear();
            txt_amount_to_pay.Clear();
        }
    }
}
