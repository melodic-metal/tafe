/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author kellyb
 */
package week.pkg13;
import javax.swing.*;

public class CreateJFrameWithComponents {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        JFrameWithComponents aFrame = new JFrameWithComponents();
        final int HEIGHT = 100;
        final int WIDTH = 350;
        aFrame.setSize(WIDTH, HEIGHT);
        aFrame.setVisible(true);
    }
    
}
