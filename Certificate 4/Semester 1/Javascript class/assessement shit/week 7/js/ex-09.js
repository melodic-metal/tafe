function countDown(count) {
    if (count >= 0) {
        let outputArea = document.getElementById("output");
        if(count == 3) {
            outputArea.innerHTML = "3: Main engine start...";
            let  sound  =  new  Howl({
                src: ['media/Rocket_Away.mp3','media/Rocket_Away.mp3','media/Rocket_Away.mp3'],

            });

            sound.play();
        }
        else if (count == 0) {
         outputArea.innerHTML = "0: We have lift off";
        }
        else {
            outputArea.innerHTML = count;
        }
        setTimeout(function () {
            countDown(count - 1);
        }, 1000);
    }
}
countDown(5);
