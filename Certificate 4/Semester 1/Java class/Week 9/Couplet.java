// Date: 13/09/2018
// Programmer: Brendan Kelly 
// Purpose: set up fields for a couplet 

public class Couplet extends Poem {
  
   private static String poemName;
   public Couplet(String name) {
      super(name, 2);
      poemName = name;
   }
      public static String getName() {
      return poemName;
   }

}