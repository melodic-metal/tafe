public class Poem {

   private static  String poemName;
   private static int numLines;
   
   public Poem(String name, int num) {
      poemName = name;
      numLines = num;         
   }
   
   public static String getName() {
      return poemName;
   }
   public static int getNumLines() {
      return numLines;
   }
   
   

}