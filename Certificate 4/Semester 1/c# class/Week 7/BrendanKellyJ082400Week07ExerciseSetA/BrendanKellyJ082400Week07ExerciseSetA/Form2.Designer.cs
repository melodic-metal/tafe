﻿namespace BrendanKellyJ082400Week07ExerciseSetA
{
    partial class Form2
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btn_reset = new System.Windows.Forms.Button();
            this.txt_cost = new System.Windows.Forms.TextBox();
            this.lbl_cost = new System.Windows.Forms.Label();
            this.lbl_method = new System.Windows.Forms.Label();
            this.lst_method = new System.Windows.Forms.ListBox();
            this.txt_passengers = new System.Windows.Forms.TextBox();
            this.lbl_passengers = new System.Windows.Forms.Label();
            this.lbL_header = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // btn_reset
            // 
            this.btn_reset.Location = new System.Drawing.Point(24, 144);
            this.btn_reset.Name = "btn_reset";
            this.btn_reset.Size = new System.Drawing.Size(75, 23);
            this.btn_reset.TabIndex = 17;
            this.btn_reset.Text = "Reset";
            this.btn_reset.UseVisualStyleBackColor = true;
            this.btn_reset.Click += new System.EventHandler(this.btn_reset_Click);
            // 
            // txt_cost
            // 
            this.txt_cost.Location = new System.Drawing.Point(135, 223);
            this.txt_cost.Name = "txt_cost";
            this.txt_cost.Size = new System.Drawing.Size(100, 20);
            this.txt_cost.TabIndex = 15;
            // 
            // lbl_cost
            // 
            this.lbl_cost.AutoSize = true;
            this.lbl_cost.Location = new System.Drawing.Point(60, 226);
            this.lbl_cost.Name = "lbl_cost";
            this.lbl_cost.Size = new System.Drawing.Size(61, 13);
            this.lbl_cost.TabIndex = 14;
            this.lbl_cost.Text = "Travel Cost";
            // 
            // lbl_method
            // 
            this.lbl_method.AutoSize = true;
            this.lbl_method.Location = new System.Drawing.Point(132, 119);
            this.lbl_method.Name = "lbl_method";
            this.lbl_method.Size = new System.Drawing.Size(121, 13);
            this.lbl_method.TabIndex = 13;
            this.lbl_method.Text = "Select Method of Travel";
            // 
            // lst_method
            // 
            this.lst_method.FormattingEnabled = true;
            this.lst_method.Items.AddRange(new object[] {
            "Bus",
            "Train",
            "Plane"});
            this.lst_method.Location = new System.Drawing.Point(135, 135);
            this.lst_method.Name = "lst_method";
            this.lst_method.Size = new System.Drawing.Size(118, 43);
            this.lst_method.TabIndex = 12;
            this.lst_method.SelectedIndexChanged += new System.EventHandler(this.lst_method_SelectedIndexChanged);
            // 
            // txt_passengers
            // 
            this.txt_passengers.Location = new System.Drawing.Point(135, 59);
            this.txt_passengers.Name = "txt_passengers";
            this.txt_passengers.Size = new System.Drawing.Size(100, 20);
            this.txt_passengers.TabIndex = 11;
            // 
            // lbl_passengers
            // 
            this.lbl_passengers.AutoSize = true;
            this.lbl_passengers.Location = new System.Drawing.Point(35, 62);
            this.lbl_passengers.Name = "lbl_passengers";
            this.lbl_passengers.Size = new System.Drawing.Size(94, 13);
            this.lbl_passengers.TabIndex = 10;
            this.lbl_passengers.Text = "No. of Passengers";
            // 
            // lbL_header
            // 
            this.lbL_header.AutoSize = true;
            this.lbL_header.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbL_header.Location = new System.Drawing.Point(96, 17);
            this.lbL_header.Name = "lbL_header";
            this.lbL_header.Size = new System.Drawing.Size(136, 20);
            this.lbL_header.TabIndex = 9;
            this.lbL_header.Text = "Perth to Sydney";
            // 
            // Form2
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(284, 261);
            this.Controls.Add(this.btn_reset);
            this.Controls.Add(this.txt_cost);
            this.Controls.Add(this.lbl_cost);
            this.Controls.Add(this.lbl_method);
            this.Controls.Add(this.lst_method);
            this.Controls.Add(this.txt_passengers);
            this.Controls.Add(this.lbl_passengers);
            this.Controls.Add(this.lbL_header);
            this.Name = "Form2";
            this.Text = "Fare Calculator";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btn_reset;
        private System.Windows.Forms.TextBox txt_cost;
        private System.Windows.Forms.Label lbl_cost;
        private System.Windows.Forms.Label lbl_method;
        private System.Windows.Forms.ListBox lst_method;
        private System.Windows.Forms.TextBox txt_passengers;
        private System.Windows.Forms.Label lbl_passengers;
        private System.Windows.Forms.Label lbL_header;
    }
}