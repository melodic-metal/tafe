setInterval(writeTime, 1000);

let outputArea = document.getElementById("output");

function writeTime() {

    outputArea.innerText = moment().format("HH:mm:ss");
}

