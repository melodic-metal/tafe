import javax.swing.*;

public class TryToParseDouble {

   public static void main(String[] args) {
      double userInput;
      try {
         userInput = Double.parseDouble(JOptionPane.showInputDialog(null, "Please enter a number"));
         JOptionPane.showMessageDialog(null, "You entered: " + userInput);
      }
      catch(NumberFormatException e) {
         JOptionPane.showMessageDialog(null, "Number format exception. Please try again");
      }
   
   }

}