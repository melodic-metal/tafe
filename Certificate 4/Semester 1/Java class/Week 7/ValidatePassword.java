import java.util.Scanner;

public class ValidatePassword {

   public static void main(String[] args) {
      int digits = 0, upper = 0, lower = 0;
      boolean valid = false;
      Scanner keyboard = new Scanner(System.in);
      System.out.println("Please enter a password");
      while(!valid) {
      
         String userInput = keyboard.nextLine();
         for (int i = 0; i < userInput.length(); i++) {
            if(userInput.charAt(i) >= 'a' && userInput.charAt(i) <= 'z') {
               lower++;
               
            }   
            if(userInput.charAt(i) >= 'A' && userInput.charAt(i) <= 'Z') {
               upper++;
            }
            if(userInput.charAt(i) >= '0' && userInput.charAt(i) <= '9') {
               digits++;
            }
            valid = true;
            
   
         }   
         
         
         if (lower < 2 || upper < 2 || digits < 2 ) {
           System.out.println("Password is invalid"); 
           System.out.println("Please enter a password"); 
           valid = false;  
         }
         else {
               System.out.println("Password is valid");   
         }   
               
      }
   }   
}
  