﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace BrendanKellyJ082400Week05ExerciseSetA
{
    public partial class Form5 : Form
    {
        public Form5()
        {
            InitializeComponent();
        }

        private void btn_calc_Click(object sender, EventArgs e)
        {
            float flt_population = 6000000000;
            int int_year = 1999;
            lst_population.Items.Add("Year\t\tPopulation");
            while (flt_population > 1000000)
            {
                flt_population = flt_population / 2;
                lst_population.Items.Add(int_year.ToString() + "\t\t" + flt_population);
                int_year -= 40;
            }

        }
    }
}
