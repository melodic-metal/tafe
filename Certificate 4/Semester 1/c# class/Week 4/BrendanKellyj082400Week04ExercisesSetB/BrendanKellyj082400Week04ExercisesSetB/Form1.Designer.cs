﻿namespace BrendanKellyj082400Week04ExercisesSetB
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lbl_mark = new System.Windows.Forms.Label();
            this.lbl_grade = new System.Windows.Forms.Label();
            this.txt_mark = new System.Windows.Forms.TextBox();
            this.txt_grade = new System.Windows.Forms.TextBox();
            this.btn_calc = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // lbl_mark
            // 
            this.lbl_mark.AutoSize = true;
            this.lbl_mark.Location = new System.Drawing.Point(12, 67);
            this.lbl_mark.Name = "lbl_mark";
            this.lbl_mark.Size = new System.Drawing.Size(91, 13);
            this.lbl_mark.TabIndex = 0;
            this.lbl_mark.Text = "Mark (as Percent)";
            // 
            // lbl_grade
            // 
            this.lbl_grade.AutoSize = true;
            this.lbl_grade.Location = new System.Drawing.Point(67, 125);
            this.lbl_grade.Name = "lbl_grade";
            this.lbl_grade.Size = new System.Drawing.Size(36, 13);
            this.lbl_grade.TabIndex = 1;
            this.lbl_grade.Text = "Grade";
            // 
            // txt_mark
            // 
            this.txt_mark.Location = new System.Drawing.Point(129, 67);
            this.txt_mark.Name = "txt_mark";
            this.txt_mark.Size = new System.Drawing.Size(100, 20);
            this.txt_mark.TabIndex = 2;
            // 
            // txt_grade
            // 
            this.txt_grade.Location = new System.Drawing.Point(129, 122);
            this.txt_grade.Name = "txt_grade";
            this.txt_grade.Size = new System.Drawing.Size(100, 20);
            this.txt_grade.TabIndex = 3;
            // 
            // btn_calc
            // 
            this.btn_calc.Location = new System.Drawing.Point(89, 186);
            this.btn_calc.Name = "btn_calc";
            this.btn_calc.Size = new System.Drawing.Size(75, 23);
            this.btn_calc.TabIndex = 4;
            this.btn_calc.Text = "Calculate";
            this.btn_calc.UseVisualStyleBackColor = true;
            this.btn_calc.Click += new System.EventHandler(this.btn_calc_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(284, 262);
            this.Controls.Add(this.btn_calc);
            this.Controls.Add(this.txt_grade);
            this.Controls.Add(this.txt_mark);
            this.Controls.Add(this.lbl_grade);
            this.Controls.Add(this.lbl_mark);
            this.Name = "Form1";
            this.Text = "Form1";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lbl_mark;
        private System.Windows.Forms.Label lbl_grade;
        private System.Windows.Forms.TextBox txt_mark;
        private System.Windows.Forms.TextBox txt_grade;
        private System.Windows.Forms.Button btn_calc;
    }
}

