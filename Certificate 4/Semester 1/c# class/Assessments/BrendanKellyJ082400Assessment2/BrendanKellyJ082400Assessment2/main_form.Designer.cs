﻿namespace BrendanKellyJ082400Assessment2
{
    partial class main_form
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.txt_price = new System.Windows.Forms.TextBox();
            this.lstProducts = new System.Windows.Forms.ListBox();
            this.lbl_price = new System.Windows.Forms.Label();
            this.pic_pic = new System.Windows.Forms.PictureBox();
            this.btn_sell = new System.Windows.Forms.Button();
            this.btn_summary = new System.Windows.Forms.Button();
            this.btn_confirm = new System.Windows.Forms.Button();
            this.btn_cancel = new System.Windows.Forms.Button();
            this.txt_quantity = new System.Windows.Forms.TextBox();
            this.txt_cost = new System.Windows.Forms.TextBox();
            this.lbl_quantity = new System.Windows.Forms.Label();
            this.lbl_cost = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.pic_pic)).BeginInit();
            this.SuspendLayout();
            // 
            // txt_price
            // 
            this.txt_price.Location = new System.Drawing.Point(201, 68);
            this.txt_price.Name = "txt_price";
            this.txt_price.Size = new System.Drawing.Size(100, 20);
            this.txt_price.TabIndex = 0;
            // 
            // lstProducts
            // 
            this.lstProducts.FormattingEnabled = true;
            this.lstProducts.Location = new System.Drawing.Point(22, 45);
            this.lstProducts.Name = "lstProducts";
            this.lstProducts.Size = new System.Drawing.Size(143, 186);
            this.lstProducts.TabIndex = 1;
            // 
            // lbl_price
            // 
            this.lbl_price.AutoSize = true;
            this.lbl_price.Location = new System.Drawing.Point(198, 52);
            this.lbl_price.Name = "lbl_price";
            this.lbl_price.Size = new System.Drawing.Size(61, 13);
            this.lbl_price.TabIndex = 2;
            this.lbl_price.Text = "Retail Price";
            // 
            // pic_pic
            // 
            this.pic_pic.Location = new System.Drawing.Point(201, 147);
            this.pic_pic.Name = "pic_pic";
            this.pic_pic.Size = new System.Drawing.Size(100, 84);
            this.pic_pic.TabIndex = 3;
            this.pic_pic.TabStop = false;
            // 
            // btn_sell
            // 
            this.btn_sell.Location = new System.Drawing.Point(28, 347);
            this.btn_sell.Name = "btn_sell";
            this.btn_sell.Size = new System.Drawing.Size(75, 41);
            this.btn_sell.TabIndex = 4;
            this.btn_sell.Text = "Sell Product";
            this.btn_sell.UseVisualStyleBackColor = true;
            this.btn_sell.Click += new System.EventHandler(this.btn_sell_Click);
            // 
            // btn_summary
            // 
            this.btn_summary.Location = new System.Drawing.Point(210, 347);
            this.btn_summary.Name = "btn_summary";
            this.btn_summary.Size = new System.Drawing.Size(75, 41);
            this.btn_summary.TabIndex = 5;
            this.btn_summary.Text = "Sale Summary";
            this.btn_summary.UseVisualStyleBackColor = true;
            this.btn_summary.Click += new System.EventHandler(this.btn_summary_Click);
            // 
            // btn_confirm
            // 
            this.btn_confirm.Location = new System.Drawing.Point(28, 394);
            this.btn_confirm.Name = "btn_confirm";
            this.btn_confirm.Size = new System.Drawing.Size(75, 34);
            this.btn_confirm.TabIndex = 6;
            this.btn_confirm.Text = "Confirm Sale";
            this.btn_confirm.UseVisualStyleBackColor = true;
            // 
            // btn_cancel
            // 
            this.btn_cancel.Location = new System.Drawing.Point(210, 394);
            this.btn_cancel.Name = "btn_cancel";
            this.btn_cancel.Size = new System.Drawing.Size(75, 34);
            this.btn_cancel.TabIndex = 7;
            this.btn_cancel.Text = "Cancel Sale";
            this.btn_cancel.UseVisualStyleBackColor = true;
            this.btn_cancel.Click += new System.EventHandler(this.btn_cancel_Click);
            // 
            // txt_quantity
            // 
            this.txt_quantity.Location = new System.Drawing.Point(106, 260);
            this.txt_quantity.Name = "txt_quantity";
            this.txt_quantity.Size = new System.Drawing.Size(100, 20);
            this.txt_quantity.TabIndex = 8;
            // 
            // txt_cost
            // 
            this.txt_cost.Location = new System.Drawing.Point(106, 300);
            this.txt_cost.Name = "txt_cost";
            this.txt_cost.Size = new System.Drawing.Size(100, 20);
            this.txt_cost.TabIndex = 9;
            // 
            // lbl_quantity
            // 
            this.lbl_quantity.AutoSize = true;
            this.lbl_quantity.Location = new System.Drawing.Point(25, 263);
            this.lbl_quantity.Name = "lbl_quantity";
            this.lbl_quantity.Size = new System.Drawing.Size(70, 13);
            this.lbl_quantity.TabIndex = 10;
            this.lbl_quantity.Text = "Sale Quantity";
            // 
            // lbl_cost
            // 
            this.lbl_cost.AutoSize = true;
            this.lbl_cost.Location = new System.Drawing.Point(25, 303);
            this.lbl_cost.Name = "lbl_cost";
            this.lbl_cost.Size = new System.Drawing.Size(64, 13);
            this.lbl_cost.TabIndex = 11;
            this.lbl_cost.Text = "Cost of Sale";
            // 
            // main_form
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(347, 440);
            this.Controls.Add(this.lbl_cost);
            this.Controls.Add(this.lbl_quantity);
            this.Controls.Add(this.txt_cost);
            this.Controls.Add(this.txt_quantity);
            this.Controls.Add(this.btn_cancel);
            this.Controls.Add(this.btn_confirm);
            this.Controls.Add(this.btn_summary);
            this.Controls.Add(this.btn_sell);
            this.Controls.Add(this.pic_pic);
            this.Controls.Add(this.lbl_price);
            this.Controls.Add(this.lstProducts);
            this.Controls.Add(this.txt_price);
            this.Name = "main_form";
            this.Text = "Form1";
            this.Load += new System.EventHandler(this.Form1_Load);
            ((System.ComponentModel.ISupportInitialize)(this.pic_pic)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox txt_price;
        private System.Windows.Forms.ListBox lstProducts;
        private System.Windows.Forms.Label lbl_price;
        private System.Windows.Forms.PictureBox pic_pic;
        private System.Windows.Forms.Button btn_sell;
        private System.Windows.Forms.Button btn_summary;
        private System.Windows.Forms.Button btn_confirm;
        private System.Windows.Forms.Button btn_cancel;
        private System.Windows.Forms.TextBox txt_quantity;
        private System.Windows.Forms.TextBox txt_cost;
        private System.Windows.Forms.Label lbl_quantity;
        private System.Windows.Forms.Label lbl_cost;
    }
}

