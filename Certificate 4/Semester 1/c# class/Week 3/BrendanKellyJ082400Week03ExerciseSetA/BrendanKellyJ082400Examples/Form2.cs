﻿// Programmer : Brendan Kelly 
// Date : August 2018
// Purpose : This application calculates the square of a number

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace BrendanKellyJ082400Examples
{
    public partial class Form2 : Form
    {
        public Form2()
        {
            InitializeComponent();
        }

        private void btnCalculate_Click(object sender, EventArgs e)
        {
            double dblNo1 = double.Parse(txtInput.Text);
            double dblResult = Math.Pow(dblNo1, 2);

            lblOutput.Text = "The square of " + dblNo1.ToString() + " is " + dblResult.ToString();
        }
    }
}
