﻿namespace BrendanKellyJ082400ExerciseSetA
{
    partial class Form2
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lst_doggos = new System.Windows.Forms.ListBox();
            this.btn_dogs = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // lst_doggos
            // 
            this.lst_doggos.FormattingEnabled = true;
            this.lst_doggos.Location = new System.Drawing.Point(82, 68);
            this.lst_doggos.Name = "lst_doggos";
            this.lst_doggos.Size = new System.Drawing.Size(120, 95);
            this.lst_doggos.TabIndex = 0;
            this.lst_doggos.Visible = false;
            // 
            // btn_dogs
            // 
            this.btn_dogs.Location = new System.Drawing.Point(97, 188);
            this.btn_dogs.Name = "btn_dogs";
            this.btn_dogs.Size = new System.Drawing.Size(75, 23);
            this.btn_dogs.TabIndex = 1;
            this.btn_dogs.Text = "do stuff";
            this.btn_dogs.UseVisualStyleBackColor = true;
            this.btn_dogs.Click += new System.EventHandler(this.btn_dogs_Click);
            // 
            // Form2
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(284, 261);
            this.Controls.Add(this.btn_dogs);
            this.Controls.Add(this.lst_doggos);
            this.Name = "Form2";
            this.Text = "Form2";
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.ListBox lst_doggos;
        private System.Windows.Forms.Button btn_dogs;
    }
}