// Date: 13/09/2018
// Programmer: Brendan Kelly
// Purpose: Hold data for pet sold

public class PetSold extends ItemSold {

   private static boolean vaccinated, neutered, houseBroken;

   public static void setVaccinated(boolean vac) {
      vaccinated = vac;
   }
   public static boolean getVaccinated() {
      return vaccinated;
   }
   public static void setNeutered(boolean neut) {
      neutered = neut;
   }
   public static boolean getNeutered() {
      return neutered;
   }
   public static void setHouseBroken(boolean hb) {
      houseBroken = hb;
   }
   public static boolean getHouseBroken() {
      return houseBroken;
   }
   
   

}