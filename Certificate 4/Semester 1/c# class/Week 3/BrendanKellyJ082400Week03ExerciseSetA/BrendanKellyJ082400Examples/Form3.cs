﻿// Programmer : Brendan Kelly 
// Date : August 2018
// Purpose : This application calculates number 1 divided by number 2

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace BrendanKellyJ082400Examples
{
    public partial class Form3 : Form
    {
        public Form3()
        {
            InitializeComponent();
        }

        private void btnCalc_Click(object sender, EventArgs e)
        {
            double dblNo1 = double.Parse(txtNo1.Text);
            double dblNo2 = double.Parse(txtNo2.Text);
            double dblResult;

            dblResult = dblNo1 / dblNo2;

            txtResult.Text = dblResult.ToString("F2");
        }

        private void txtNo2_TextChanged(object sender, EventArgs e)
        {

        }
    }
}
