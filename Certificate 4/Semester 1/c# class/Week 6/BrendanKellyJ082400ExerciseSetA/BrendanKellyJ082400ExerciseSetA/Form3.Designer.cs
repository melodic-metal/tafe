﻿namespace BrendanKellyJ082400ExerciseSetA
{
    partial class Form3
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btn_dogs = new System.Windows.Forms.Button();
            this.lst_doggos = new System.Windows.Forms.ListBox();
            this.txt_input = new System.Windows.Forms.TextBox();
            this.lbl_dog = new System.Windows.Forms.Label();
            this.btn_output = new System.Windows.Forms.Button();
            this.lbl_output = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // btn_dogs
            // 
            this.btn_dogs.Location = new System.Drawing.Point(97, 127);
            this.btn_dogs.Name = "btn_dogs";
            this.btn_dogs.Size = new System.Drawing.Size(75, 23);
            this.btn_dogs.TabIndex = 3;
            this.btn_dogs.Text = "do stuff";
            this.btn_dogs.UseVisualStyleBackColor = true;
            this.btn_dogs.Click += new System.EventHandler(this.btn_dogs_Click);
            // 
            // lst_doggos
            // 
            this.lst_doggos.FormattingEnabled = true;
            this.lst_doggos.Location = new System.Drawing.Point(80, 26);
            this.lst_doggos.Name = "lst_doggos";
            this.lst_doggos.Size = new System.Drawing.Size(120, 95);
            this.lst_doggos.TabIndex = 2;
            this.lst_doggos.Visible = false;
            // 
            // txt_input
            // 
            this.txt_input.Location = new System.Drawing.Point(144, 164);
            this.txt_input.Name = "txt_input";
            this.txt_input.Size = new System.Drawing.Size(100, 20);
            this.txt_input.TabIndex = 4;
            // 
            // lbl_dog
            // 
            this.lbl_dog.AutoSize = true;
            this.lbl_dog.Location = new System.Drawing.Point(42, 167);
            this.lbl_dog.Name = "lbl_dog";
            this.lbl_dog.Size = new System.Drawing.Size(96, 13);
            this.lbl_dog.TabIndex = 5;
            this.lbl_dog.Text = "Enter Type Of Dog";
            // 
            // btn_output
            // 
            this.btn_output.Location = new System.Drawing.Point(153, 200);
            this.btn_output.Name = "btn_output";
            this.btn_output.Size = new System.Drawing.Size(75, 49);
            this.btn_output.TabIndex = 6;
            this.btn_output.Text = "Do Other Stuff";
            this.btn_output.UseVisualStyleBackColor = true;
            this.btn_output.Click += new System.EventHandler(this.btn_output_Click);
            // 
            // lbl_output
            // 
            this.lbl_output.AutoSize = true;
            this.lbl_output.Location = new System.Drawing.Point(42, 218);
            this.lbl_output.Name = "lbl_output";
            this.lbl_output.Size = new System.Drawing.Size(0, 13);
            this.lbl_output.TabIndex = 7;
            // 
            // Form3
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(284, 261);
            this.Controls.Add(this.lbl_output);
            this.Controls.Add(this.btn_output);
            this.Controls.Add(this.lbl_dog);
            this.Controls.Add(this.txt_input);
            this.Controls.Add(this.btn_dogs);
            this.Controls.Add(this.lst_doggos);
            this.Name = "Form3";
            this.Text = "Form3";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btn_dogs;
        private System.Windows.Forms.ListBox lst_doggos;
        private System.Windows.Forms.TextBox txt_input;
        private System.Windows.Forms.Label lbl_dog;
        private System.Windows.Forms.Button btn_output;
        private System.Windows.Forms.Label lbl_output;
    }
}