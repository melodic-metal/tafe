﻿namespace BrendanKellyJ082400Week05ExerciseSetA
{
    partial class Form3
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lbl_investment = new System.Windows.Forms.Label();
            this.lbl_interest = new System.Windows.Forms.Label();
            this.lst_table = new System.Windows.Forms.ListBox();
            this.txt_investment = new System.Windows.Forms.TextBox();
            this.txt_interest = new System.Windows.Forms.TextBox();
            this.btn_calc = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // lbl_investment
            // 
            this.lbl_investment.AutoSize = true;
            this.lbl_investment.Location = new System.Drawing.Point(43, 31);
            this.lbl_investment.Name = "lbl_investment";
            this.lbl_investment.Size = new System.Drawing.Size(98, 13);
            this.lbl_investment.TabIndex = 0;
            this.lbl_investment.Text = "Investment Amount";
            // 
            // lbl_interest
            // 
            this.lbl_interest.AutoSize = true;
            this.lbl_interest.Location = new System.Drawing.Point(43, 70);
            this.lbl_interest.Name = "lbl_interest";
            this.lbl_interest.Size = new System.Drawing.Size(98, 13);
            this.lbl_interest.TabIndex = 1;
            this.lbl_interest.Text = "Interest (in decimal)";
            // 
            // lst_table
            // 
            this.lst_table.FormattingEnabled = true;
            this.lst_table.Location = new System.Drawing.Point(64, 104);
            this.lst_table.Name = "lst_table";
            this.lst_table.Size = new System.Drawing.Size(173, 108);
            this.lst_table.TabIndex = 2;
            // 
            // txt_investment
            // 
            this.txt_investment.Location = new System.Drawing.Point(147, 28);
            this.txt_investment.Name = "txt_investment";
            this.txt_investment.Size = new System.Drawing.Size(100, 20);
            this.txt_investment.TabIndex = 3;
            // 
            // txt_interest
            // 
            this.txt_interest.Location = new System.Drawing.Point(147, 67);
            this.txt_interest.Name = "txt_interest";
            this.txt_interest.Size = new System.Drawing.Size(100, 20);
            this.txt_interest.TabIndex = 4;
            // 
            // btn_calc
            // 
            this.btn_calc.Location = new System.Drawing.Point(106, 226);
            this.btn_calc.Name = "btn_calc";
            this.btn_calc.Size = new System.Drawing.Size(75, 23);
            this.btn_calc.TabIndex = 5;
            this.btn_calc.Text = "Calculate";
            this.btn_calc.UseVisualStyleBackColor = true;
            this.btn_calc.Click += new System.EventHandler(this.btn_calc_Click);
            // 
            // Form3
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(284, 261);
            this.Controls.Add(this.btn_calc);
            this.Controls.Add(this.txt_interest);
            this.Controls.Add(this.txt_investment);
            this.Controls.Add(this.lst_table);
            this.Controls.Add(this.lbl_interest);
            this.Controls.Add(this.lbl_investment);
            this.Name = "Form3";
            this.Text = "Form3";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lbl_investment;
        private System.Windows.Forms.Label lbl_interest;
        private System.Windows.Forms.ListBox lst_table;
        private System.Windows.Forms.TextBox txt_investment;
        private System.Windows.Forms.TextBox txt_interest;
        private System.Windows.Forms.Button btn_calc;
    }
}