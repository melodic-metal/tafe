// Date: 25/08/2018
// Programmer: Brendan Kelly J082400
// Purpose: Count by anything

import java.util.Scanner;

public class CountByAnything {

   public static void main(String[] args) {
      Scanner keyboard = new Scanner(System.in);
      System.out.print("Please enter a number to start counting up to: ");
      int number = keyboard.nextInt();
      System.out.print("Enter a number for multiples: ");
      int multiples = keyboard.nextInt();

      for(int i = 3; i <= number; i++){
         if (i % multiples ==0){
            System.out.print(i + " ");
         }
         if(i % 10 == 0){
            System.out.println();
         }
      }

    
      
   }
      
}