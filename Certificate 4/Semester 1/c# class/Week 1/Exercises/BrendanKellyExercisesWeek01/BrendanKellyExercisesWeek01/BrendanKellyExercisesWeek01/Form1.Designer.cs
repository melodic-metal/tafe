﻿namespace BrendanKellyExercisesWeek01
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnClick = new System.Windows.Forms.Button();
            this.lblFamilyName = new System.Windows.Forms.Label();
            this.lblGivenName = new System.Windows.Forms.Label();
            this.txtFamilyName = new System.Windows.Forms.TextBox();
            this.txtGivenName = new System.Windows.Forms.TextBox();
            this.SuspendLayout();
            // 
            // btnClick
            // 
            this.btnClick.Location = new System.Drawing.Point(23, 194);
            this.btnClick.Name = "btnClick";
            this.btnClick.Size = new System.Drawing.Size(75, 23);
            this.btnClick.TabIndex = 0;
            this.btnClick.Text = "Click";
            this.btnClick.UseVisualStyleBackColor = true;
            // 
            // lblFamilyName
            // 
            this.lblFamilyName.AutoSize = true;
            this.lblFamilyName.Location = new System.Drawing.Point(23, 67);
            this.lblFamilyName.Name = "lblFamilyName";
            this.lblFamilyName.Size = new System.Drawing.Size(67, 13);
            this.lblFamilyName.TabIndex = 1;
            this.lblFamilyName.Text = "Family Name";
            // 
            // lblGivenName
            // 
            this.lblGivenName.AutoSize = true;
            this.lblGivenName.Location = new System.Drawing.Point(23, 96);
            this.lblGivenName.Name = "lblGivenName";
            this.lblGivenName.Size = new System.Drawing.Size(38, 26);
            this.lblGivenName.TabIndex = 2;
            this.lblGivenName.Text = "Given \r\nName";
            // 
            // txtFamilyName
            // 
            this.txtFamilyName.BackColor = System.Drawing.Color.LightSkyBlue;
            this.txtFamilyName.ForeColor = System.Drawing.Color.Yellow;
            this.txtFamilyName.Location = new System.Drawing.Point(124, 67);
            this.txtFamilyName.Name = "txtFamilyName";
            this.txtFamilyName.Size = new System.Drawing.Size(100, 20);
            this.txtFamilyName.TabIndex = 3;
            this.txtFamilyName.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // txtGivenName
            // 
            this.txtGivenName.BackColor = System.Drawing.Color.LightSkyBlue;
            this.txtGivenName.ForeColor = System.Drawing.Color.Yellow;
            this.txtGivenName.Location = new System.Drawing.Point(124, 102);
            this.txtGivenName.Name = "txtGivenName";
            this.txtGivenName.Size = new System.Drawing.Size(100, 20);
            this.txtGivenName.TabIndex = 4;
            this.txtGivenName.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(284, 261);
            this.Controls.Add(this.txtGivenName);
            this.Controls.Add(this.txtFamilyName);
            this.Controls.Add(this.lblGivenName);
            this.Controls.Add(this.lblFamilyName);
            this.Controls.Add(this.btnClick);
            this.Name = "Form1";
            this.Text = "Question1";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnClick;
        private System.Windows.Forms.Label lblFamilyName;
        private System.Windows.Forms.Label lblGivenName;
        private System.Windows.Forms.TextBox txtFamilyName;
        private System.Windows.Forms.TextBox txtGivenName;
    }
}

