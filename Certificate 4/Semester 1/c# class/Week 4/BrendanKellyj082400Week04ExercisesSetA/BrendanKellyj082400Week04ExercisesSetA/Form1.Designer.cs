﻿namespace BrendanKellyj082400Week04ExercisesSetA
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lbl_num_boomerangs = new System.Windows.Forms.Label();
            this.lbl_cost = new System.Windows.Forms.Label();
            this.txt_num_booms = new System.Windows.Forms.TextBox();
            this.btn_calc = new System.Windows.Forms.Button();
            this.txt_cost = new System.Windows.Forms.TextBox();
            this.SuspendLayout();
            // 
            // lbl_num_boomerangs
            // 
            this.lbl_num_boomerangs.AutoSize = true;
            this.lbl_num_boomerangs.Location = new System.Drawing.Point(2, 56);
            this.lbl_num_boomerangs.Name = "lbl_num_boomerangs";
            this.lbl_num_boomerangs.Size = new System.Drawing.Size(146, 13);
            this.lbl_num_boomerangs.TabIndex = 0;
            this.lbl_num_boomerangs.Text = "Enter Number of Boomerangs";
            // 
            // lbl_cost
            // 
            this.lbl_cost.AutoSize = true;
            this.lbl_cost.Location = new System.Drawing.Point(120, 119);
            this.lbl_cost.Name = "lbl_cost";
            this.lbl_cost.Size = new System.Drawing.Size(28, 13);
            this.lbl_cost.TabIndex = 2;
            this.lbl_cost.Text = "Cost";
            // 
            // txt_num_booms
            // 
            this.txt_num_booms.Location = new System.Drawing.Point(172, 53);
            this.txt_num_booms.Name = "txt_num_booms";
            this.txt_num_booms.Size = new System.Drawing.Size(100, 20);
            this.txt_num_booms.TabIndex = 3;
            // 
            // btn_calc
            // 
            this.btn_calc.Location = new System.Drawing.Point(112, 186);
            this.btn_calc.Name = "btn_calc";
            this.btn_calc.Size = new System.Drawing.Size(75, 23);
            this.btn_calc.TabIndex = 4;
            this.btn_calc.Text = "Calculate";
            this.btn_calc.UseVisualStyleBackColor = true;
            this.btn_calc.Click += new System.EventHandler(this.btn_calc_Click);
            // 
            // txt_cost
            // 
            this.txt_cost.Enabled = false;
            this.txt_cost.Location = new System.Drawing.Point(172, 119);
            this.txt_cost.Name = "txt_cost";
            this.txt_cost.Size = new System.Drawing.Size(100, 20);
            this.txt_cost.TabIndex = 6;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(284, 261);
            this.Controls.Add(this.txt_cost);
            this.Controls.Add(this.btn_calc);
            this.Controls.Add(this.txt_num_booms);
            this.Controls.Add(this.lbl_cost);
            this.Controls.Add(this.lbl_num_boomerangs);
            this.Name = "Form1";
            this.Text = "Boomerangs \'r\' Us";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lbl_num_boomerangs;
        private System.Windows.Forms.Label lbl_cost;
        private System.Windows.Forms.TextBox txt_num_booms;
        private System.Windows.Forms.Button btn_calc;
        private System.Windows.Forms.TextBox txt_cost;
    }
}

