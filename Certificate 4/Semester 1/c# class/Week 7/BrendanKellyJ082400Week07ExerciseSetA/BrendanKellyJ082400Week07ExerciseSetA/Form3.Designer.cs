﻿namespace BrendanKellyJ082400Week07ExerciseSetA
{
    partial class Form3
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.txt_input = new System.Windows.Forms.TextBox();
            this.lbl_input = new System.Windows.Forms.Label();
            this.btn_number_of_names = new System.Windows.Forms.Button();
            this.btn_add = new System.Windows.Forms.Button();
            this.btn_delete_entered = new System.Windows.Forms.Button();
            this.btn_delete_selected = new System.Windows.Forms.Button();
            this.btn_sort = new System.Windows.Forms.Button();
            this.lbl_output_header = new System.Windows.Forms.Label();
            this.lst_output = new System.Windows.Forms.ListBox();
            this.SuspendLayout();
            // 
            // txt_input
            // 
            this.txt_input.Location = new System.Drawing.Point(12, 36);
            this.txt_input.Name = "txt_input";
            this.txt_input.Size = new System.Drawing.Size(114, 20);
            this.txt_input.TabIndex = 0;
            // 
            // lbl_input
            // 
            this.lbl_input.AutoSize = true;
            this.lbl_input.Location = new System.Drawing.Point(26, 13);
            this.lbl_input.Name = "lbl_input";
            this.lbl_input.Size = new System.Drawing.Size(73, 13);
            this.lbl_input.TabIndex = 1;
            this.lbl_input.Text = "Enter  a name";
            // 
            // btn_number_of_names
            // 
            this.btn_number_of_names.Location = new System.Drawing.Point(12, 62);
            this.btn_number_of_names.Name = "btn_number_of_names";
            this.btn_number_of_names.Size = new System.Drawing.Size(114, 35);
            this.btn_number_of_names.TabIndex = 2;
            this.btn_number_of_names.Text = "Display Number of names";
            this.btn_number_of_names.UseVisualStyleBackColor = true;
            this.btn_number_of_names.Click += new System.EventHandler(this.btn_number_of_names_Click);
            // 
            // btn_add
            // 
            this.btn_add.Location = new System.Drawing.Point(12, 105);
            this.btn_add.Name = "btn_add";
            this.btn_add.Size = new System.Drawing.Size(114, 23);
            this.btn_add.TabIndex = 3;
            this.btn_add.Text = "Add the name";
            this.btn_add.UseVisualStyleBackColor = true;
            this.btn_add.Click += new System.EventHandler(this.btn_add_Click);
            // 
            // btn_delete_entered
            // 
            this.btn_delete_entered.Location = new System.Drawing.Point(12, 134);
            this.btn_delete_entered.Name = "btn_delete_entered";
            this.btn_delete_entered.Size = new System.Drawing.Size(114, 40);
            this.btn_delete_entered.TabIndex = 4;
            this.btn_delete_entered.Text = "Delete Entered Name";
            this.btn_delete_entered.UseVisualStyleBackColor = true;
            this.btn_delete_entered.Click += new System.EventHandler(this.btn_delete_entered_Click);
            // 
            // btn_delete_selected
            // 
            this.btn_delete_selected.Location = new System.Drawing.Point(12, 180);
            this.btn_delete_selected.Name = "btn_delete_selected";
            this.btn_delete_selected.Size = new System.Drawing.Size(114, 40);
            this.btn_delete_selected.TabIndex = 5;
            this.btn_delete_selected.Text = "Remove the name selected";
            this.btn_delete_selected.UseVisualStyleBackColor = true;
            this.btn_delete_selected.Click += new System.EventHandler(this.btn_delete_selected_Click);
            // 
            // btn_sort
            // 
            this.btn_sort.Location = new System.Drawing.Point(12, 226);
            this.btn_sort.Name = "btn_sort";
            this.btn_sort.Size = new System.Drawing.Size(114, 23);
            this.btn_sort.TabIndex = 6;
            this.btn_sort.Text = "Sort the List";
            this.btn_sort.UseVisualStyleBackColor = true;
            this.btn_sort.Click += new System.EventHandler(this.btn_sort_Click);
            // 
            // lbl_output_header
            // 
            this.lbl_output_header.AutoSize = true;
            this.lbl_output_header.Location = new System.Drawing.Point(149, 13);
            this.lbl_output_header.Name = "lbl_output_header";
            this.lbl_output_header.Size = new System.Drawing.Size(77, 13);
            this.lbl_output_header.TabIndex = 7;
            this.lbl_output_header.Text = "Current Names";
            // 
            // lst_output
            // 
            this.lst_output.FormattingEnabled = true;
            this.lst_output.Location = new System.Drawing.Point(152, 36);
            this.lst_output.Name = "lst_output";
            this.lst_output.Size = new System.Drawing.Size(120, 212);
            this.lst_output.TabIndex = 8;
            // 
            // Form3
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(284, 260);
            this.Controls.Add(this.lst_output);
            this.Controls.Add(this.lbl_output_header);
            this.Controls.Add(this.btn_sort);
            this.Controls.Add(this.btn_delete_selected);
            this.Controls.Add(this.btn_delete_entered);
            this.Controls.Add(this.btn_add);
            this.Controls.Add(this.btn_number_of_names);
            this.Controls.Add(this.lbl_input);
            this.Controls.Add(this.txt_input);
            this.Name = "Form3";
            this.Text = "Form3";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox txt_input;
        private System.Windows.Forms.Label lbl_input;
        private System.Windows.Forms.Button btn_number_of_names;
        private System.Windows.Forms.Button btn_add;
        private System.Windows.Forms.Button btn_delete_entered;
        private System.Windows.Forms.Button btn_delete_selected;
        private System.Windows.Forms.Button btn_sort;
        private System.Windows.Forms.Label lbl_output_header;
        private System.Windows.Forms.ListBox lst_output;
    }
}