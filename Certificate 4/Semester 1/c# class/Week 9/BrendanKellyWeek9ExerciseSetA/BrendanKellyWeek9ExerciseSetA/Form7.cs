﻿// Date: 13/09/2018
// Programmer: Brendan Kelly
// Purpose: Find when repayments are due

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace BrendanKellyWeek9ExerciseSetA
{
    public partial class Form7 : Form
    {
        public Form7()
        {
            InitializeComponent();
        }

        private void btn_calc_Click(object sender, EventArgs e)
        {
            lst_output.Items.Clear();
            DateTime dte_now = DateTime.Now;
            


            if (!Double.TryParse(txt_interval.Text, out Double int_interval))
            {
                MessageBox.Show("Interval is not a whole number");
            }
            if (!int.TryParse(txt_repay.Text, out int int_repay))
            {
                MessageBox.Show("Repayments field is not a whole number");
            }
            
                lst_output.Items.Add("Date Payment to be made");

            

            for (int i = 0; i < int_repay; i++)
            {
                dte_now = dte_now.AddDays(int_interval);
                lst_output.Items.Add(dte_now);
            }
            
            
        }
    }
}
