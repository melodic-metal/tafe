<!doctype html>
<html lang="en">
<!--
ONE LINE TITLE FOR FILE

LONGER EXPLANATION OF THE FILE AND ITS PURPOSE
MAY BE MORE THAN ONE LINE IN LENGTH

@File:       /Template/index.html
@Project:    Classwork
@Author:     Brendan Kelly <082400@tafe.wa.edu.au>
@Version:    1.0
@Copyright:  YOUR NAME 2018

History:
v1.0    2018-07-24
        did stuff

v0.1    2018-07-24
        Template as provided by Adrian Gould
-->
<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <title>BWK - Classwork</title>

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="../assets/css/bootstrap.css">
    <!-- FontAwesome CSS -->
    <link rel="stylesheet" href="../assets/css/fontawesome-all.css">
    <!-- core first, then extra styles -->
    <link rel="stylesheet" href="../assets/css/app.css">

</head>
<body>


<nav class="navbar navbar-expand-lg navbar-dark bg-dark fixed-top">

    <a href="../" class="navbar-brand">BWK</a>

    <button class="navbar-toggler" type="button"
            data-toggle="collapse"
            data-target="#navbarContent"
            aria-controls="#navbarContent"
            aria-expanded="false" aria-label="toggle">
        <span class="navbar-toggler-icon"></span>
    </button>

    <div class="collapse navbar-collapse" id="navbarContent">
        <ul class="navbar-nav mr-auto">
            <li class="nav-item ">
                <a href="../index.php" class="nav-link">Home</a>
            </li>
            <li class="nav-item ">
                <a href="../Ep11/" class="nav-link">Ep11</a>
            </li>
            <li class="nav-item">
                <a href="../Ep12/" class="nav-link">Ep12</a>
            </li>
            <li class="nav-item">
                <a href="../Ep13/" class="nav-link">Ep13</a>
            </li>
            <li class="nav-item ">
                <a href="../Ep14/" class="nav-link">Ep14</a>
            </li>
            <li class="nav-item ">
                <a href="../Ep15/" class="nav-link">Ep15</a>
            </li>
            <li class="nav-item ">
                <a href="../Ep16/" class="nav-link">Ep16</a>
            </li>
            <li class="nav-item ">
                <a href="../Ep17/" class="nav-link">Ep17</a>
            </li>
            <li class="nav-item ">
                <a href="../Ep18/" class="nav-link">Ep18</a>
            </li>
            <li class="nav-item ">
                <a href="../Ep19/" class="nav-link">Ep19</a>
            </li>
        </ul>
    </div>
</nav>

<div class="container">

    <!-- Start Content goes here -->

    <?php
/**
 * Created by PhpStorm.
 * User: kellyb
 * Date: 18/09/2018
 * Time: 2:03 PM
 */

// one line comment
/*
multi line comment
*/

/** php doc comments
 *
 */

echo "<h1>Hello world!</h1>";
print "<h2>Hello world 2!</h2>";

$name = "BWK";

define("WELCOME_MESSAGE", "Hello World");
echo "<p>This site belongs to {$name} </p>";
echo '<p>This site belongs to {$name} </p>';

echo '<p>'.WELCOME_MESSAGE.'</p>';

?>

<hr>
<h3>Hello <?=$name;?></h3>

<?php
 oddOrEven();
function oddOrEven($num = 22) {

    if ($num % 2 == 0) {
        echo "even";
    }
    else {
        echo "odd";
    }
}

class Cars {
    public function __construct($make, $model, $numberOfWheels = 4) {
        $this->make = $make;
        $this->model = $model;
        $this->numberOfWheels = $numberOfWheels;


    }
    public function AWheelFellOff() {
            if ($this->numberOfWheels > 0) {
                $this->numberOfWheels--;
            }
    }
}
$mycar = new Car("Mitsubishi", "Pajero");
$mycar->AWheelFellOff();
echo $mycar->numberOfWheels;

$robertsCar= new Car("Honda", "Civic");

$cars = array($mycar, $robertsCar, new Car("Bleh", "Blehness",2));

foreach ($cars as $car) {
    echo $car->make . " " . $car->model . "<br>";
}

function compareCars($car1, $car2) {
    return strcmp($car1->make, $car2->make);
}

usort($cars, "compareCars");

foreach ($cars as $car) {
    echo $car->make . " " . $car->model . "<br>";
}?>

    <!-- End Content Goes here-->
    <a href="exercises.php">Exercises</a>
</div><!-- end container -->

<footer class="footer bg-dark text-light">
    <div class="container">
        <div class="row">
            <div class="col-4 small">
                <ul class="list-unstyled">
                    <li>&copy; Copyright 2018 Brendan Kelly</li>
                    <li><a href="#">Contact Us</a></li>
                    <li><a href="../template/index.html">Template</a></li>
                </ul>
            </div>

            <div class="col-4 small">
                <ul class="list-unstyled">
                    <li><a href="#">Site map</a></li>
                    <li><a href="#">Privacy policy</a></li>
                    <li><a href="#">Terms and Conditions</a></li>
                </ul>
            </div>

            <div class="col-4 small">
                <ul class="list-inline">
                    <li class="list-inline-item">
                        <a href="http://twitter.com">
                            <i class="fab fa-twitter"></i>
                            Twitter
                        </a>
                    </li>
                    <li class="list-inline-item">
                        <a href="http://facebook.com">
                            <i class="fab fa-facebook"></i>
                            Facebook
                        </a>
                    </li>
                    <li class="list-inline-item">
                        <a href="http://linkedin.com">
                            <i class="fab fa-linkedin"></i>
                            Linked in
                        </a>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</footer>


<script src="../assets/js/jquery-3.3.1.js"></script>
<script src="../assets/js/bootstrap.min.js"></script>
<script src="../assets/js/fontawesome-all.js"></script>

<!-- include page specific JS files -->
<script src="JS-FILENAME-GOES-HERE.JS"></script>
<!-- end of page specific JS files -->
</body>
</html>



