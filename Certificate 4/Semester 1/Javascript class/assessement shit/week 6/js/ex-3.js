"use strict";

document.getElementById("plus").addEventListener(
    "click", function (event) {
        let num1 = parseInt(getValue("number1"));
        let num2 = parseInt(getValue("number2"));
        calculatePlus(num1, num2);
    }
);

document.getElementById("subtract").addEventListener(
    "click", function (event) {
        let num1 = parseInt(getValue("number1"));
        let num2 = parseInt(getValue("number2"));
        calculateSubtract(num1, num2);
    }
);
document.getElementById("multiply").addEventListener(
    "click", function (event) {
        let num1 = parseInt(getValue("number1"));
        let num2 = parseInt(getValue("number2"));
        calculateMultiply(num1, num2);
    }
);

document.getElementById("divide").addEventListener(
    "click", function (event) {
        let num1 = parseInt(getValue("number1"));
        let num2 = parseInt(getValue("number2"));
        calculateDivide(num1, num2);
    }
);

stopDefault("plus");
stopDefault("subtract");
stopDefault("divide");
stopDefault("multiply");


function calculatePlus(number1, number2) {
        let resultsArea = document.getElementById("resultsArea");
        let result = number1 + number2;
        resultsArea.innerHTML = "<p>" + result.toString() + "</p>";

}

function calculateSubtract(number1, number2) {
    let resultsArea = document.getElementById("resultsArea");
    let result = number1 - number2;
    resultsArea.innerHTML = "<p>" + result.toString() + "</p>";

}

function calculateDivide(number1, number2) {
    let resultsArea = document.getElementById("resultsArea");
    let result = number1 / number2;
    resultsArea.innerHTML = "<p>" + result.toString() + "</p>";

}

function calculateMultiply(number1, number2) {
    let resultsArea = document.getElementById("resultsArea");
    let result = number1 * number2;
    resultsArea.innerHTML = "<p>" + result.toString() + "</p>";

}


function getValue(elementID) {

    let theControl = document.getElementById(elementID);
    return theControl.value;

}


function stopDefault(elementID) {
    document.getElementById(elementID).addEventListener("click", function (event) {
            event.preventDefault()
        }
    );
}
