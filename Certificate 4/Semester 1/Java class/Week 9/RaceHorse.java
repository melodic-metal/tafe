// Date: 13/09/2018
// Programmer: Brendan Kelly
// Purpose: Extending the RaceHorse class, to set races competed.

public class RaceHorse extends Horse {

   private static int racesCompeted;
   
   public static void setRacesCompeted(int races) {
      racesCompeted = races;
   }
   
   public static int getRacesCompeted() {
      return racesCompeted;   
   }   

}