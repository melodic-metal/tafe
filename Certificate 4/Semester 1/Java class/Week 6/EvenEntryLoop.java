// Date: 25/08/2018
// Programmer: Brendan Kelly J082400
// Purpose: Get a number and return a message depending on whether it's odd or even

import java.util.Scanner;

public class EvenEntryLoop {

   public static void main(String[] args) {
      Scanner keyboard = new Scanner(System.in);
      System.out.println("Please type a number. 999 to exit");
      while (true)
       {
        
         int number = keyboard.nextInt();
         
            if (number == 999) {
               System.out.println("System exiting! Goodbye!");
               break;
            }
            else if (number % 2 == 0)
             {
               
               System.out.println("Good job!");
            }
            else if (number % 2 == 1) {
               
               System.out.println("ERROR! ERROR! Please enter another number!");
               
            }      
            
          
         }
   }
}   
