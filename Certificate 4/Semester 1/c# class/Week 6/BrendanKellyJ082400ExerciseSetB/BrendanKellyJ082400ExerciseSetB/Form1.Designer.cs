﻿namespace BrendanKellyJ082400ExerciseSetB
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.txt_input = new System.Windows.Forms.TextBox();
            this.btn_calc = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this.txt_output = new System.Windows.Forms.TextBox();
            this.lst_prices_under = new System.Windows.Forms.ListBox();
            this.btn_price_under = new System.Windows.Forms.Button();
            this.lbl_price_under = new System.Windows.Forms.Label();
            this.txt_prices_under = new System.Windows.Forms.TextBox();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(12, 52);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(124, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Enter Name of Magazine";
            // 
            // txt_input
            // 
            this.txt_input.Location = new System.Drawing.Point(154, 49);
            this.txt_input.Name = "txt_input";
            this.txt_input.Size = new System.Drawing.Size(100, 20);
            this.txt_input.TabIndex = 1;
            // 
            // btn_calc
            // 
            this.btn_calc.Location = new System.Drawing.Point(106, 111);
            this.btn_calc.Name = "btn_calc";
            this.btn_calc.Size = new System.Drawing.Size(75, 23);
            this.btn_calc.TabIndex = 2;
            this.btn_calc.Text = "Find Price";
            this.btn_calc.UseVisualStyleBackColor = true;
            this.btn_calc.Click += new System.EventHandler(this.btn_calc_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(47, 85);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(89, 13);
            this.label2.TabIndex = 3;
            this.label2.Text = "Cost of Magazine";
            // 
            // txt_output
            // 
            this.txt_output.Location = new System.Drawing.Point(154, 85);
            this.txt_output.Name = "txt_output";
            this.txt_output.ReadOnly = true;
            this.txt_output.Size = new System.Drawing.Size(100, 20);
            this.txt_output.TabIndex = 4;
            // 
            // lst_prices_under
            // 
            this.lst_prices_under.FormattingEnabled = true;
            this.lst_prices_under.Location = new System.Drawing.Point(24, 156);
            this.lst_prices_under.Name = "lst_prices_under";
            this.lst_prices_under.Size = new System.Drawing.Size(230, 95);
            this.lst_prices_under.TabIndex = 5;
            // 
            // btn_price_under
            // 
            this.btn_price_under.Location = new System.Drawing.Point(94, 285);
            this.btn_price_under.Name = "btn_price_under";
            this.btn_price_under.Size = new System.Drawing.Size(87, 23);
            this.btn_price_under.TabIndex = 6;
            this.btn_price_under.Text = "Prices Under";
            this.btn_price_under.UseVisualStyleBackColor = true;
            this.btn_price_under.Click += new System.EventHandler(this.btn_price_under_Click);
            // 
            // lbl_price_under
            // 
            this.lbl_price_under.AutoSize = true;
            this.lbl_price_under.Location = new System.Drawing.Point(12, 260);
            this.lbl_price_under.Name = "lbl_price_under";
            this.lbl_price_under.Size = new System.Drawing.Size(127, 13);
            this.lbl_price_under.TabIndex = 7;
            this.lbl_price_under.Text = "Display magazines under:";
            // 
            // txt_prices_under
            // 
            this.txt_prices_under.Location = new System.Drawing.Point(154, 257);
            this.txt_prices_under.Name = "txt_prices_under";
            this.txt_prices_under.Size = new System.Drawing.Size(100, 20);
            this.txt_prices_under.TabIndex = 8;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(284, 320);
            this.Controls.Add(this.txt_prices_under);
            this.Controls.Add(this.lbl_price_under);
            this.Controls.Add(this.btn_price_under);
            this.Controls.Add(this.lst_prices_under);
            this.Controls.Add(this.txt_output);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.btn_calc);
            this.Controls.Add(this.txt_input);
            this.Controls.Add(this.label1);
            this.Name = "Form1";
            this.Text = "Form1";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox txt_input;
        private System.Windows.Forms.Button btn_calc;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox txt_output;
        private System.Windows.Forms.ListBox lst_prices_under;
        private System.Windows.Forms.Button btn_price_under;
        private System.Windows.Forms.Label lbl_price_under;
        private System.Windows.Forms.TextBox txt_prices_under;
    }
}

