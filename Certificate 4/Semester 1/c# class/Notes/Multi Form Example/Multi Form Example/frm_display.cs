﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

// programmer   : Brian
// date         : April 2014
// purpose      : to demonstrate multi form applications aand passing data between froms

namespace Multi_Form_Example
{
    public partial class frm_display : Form
    {
        public int int_age;
        
        public frm_display()
        {
            InitializeComponent();
        }

        private void btn_display_age_Click(object sender, EventArgs e)
        {
            //   display the age placed in this form's public variable from within the main form
            txt_age_F2.Text = int_age.ToString();
            //   display Given Name in main form public static variable
            txt_given_name_F2.Text = frm_main.str_given_name;
         }

        private void btn_change_age_Click(object sender, EventArgs e)
        {
            int int_new_age;
            if (!int.TryParse(txt_age_F2.Text, out int_new_age))
            {
                MessageBox.Show("The entered age is not a number - Enter it again");
                txt_age_F2.Clear();
                txt_age_F2.Focus();
            }
            else
            {                
                //  Change the this forms public variable value
                int_age = int_new_age;
                // change the frm_main's public static variable
                frm_main.str_given_name = txt_given_name_F2.Text;
            }
        }

        private void btn_close_form_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
