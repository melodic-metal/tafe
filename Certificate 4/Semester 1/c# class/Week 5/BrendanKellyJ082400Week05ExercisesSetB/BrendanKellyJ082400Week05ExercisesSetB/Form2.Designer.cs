﻿namespace BrendanKellyJ082400Week05ExercisesSetB
{
    partial class Form2
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btn_ones = new System.Windows.Forms.Button();
            this.btn_fours = new System.Windows.Forms.Button();
            this.btn_twos = new System.Windows.Forms.Button();
            this.lst_table = new System.Windows.Forms.ListBox();
            this.SuspendLayout();
            // 
            // btn_ones
            // 
            this.btn_ones.Location = new System.Drawing.Point(13, 58);
            this.btn_ones.Name = "btn_ones";
            this.btn_ones.Size = new System.Drawing.Size(75, 23);
            this.btn_ones.TabIndex = 8;
            this.btn_ones.Text = "Up By 1\'s";
            this.btn_ones.UseVisualStyleBackColor = true;
            this.btn_ones.Click += new System.EventHandler(this.btn_ones_Click);
            // 
            // btn_fours
            // 
            this.btn_fours.Location = new System.Drawing.Point(197, 59);
            this.btn_fours.Name = "btn_fours";
            this.btn_fours.Size = new System.Drawing.Size(75, 23);
            this.btn_fours.TabIndex = 7;
            this.btn_fours.Text = "Down By 4\'s";
            this.btn_fours.UseVisualStyleBackColor = true;
            this.btn_fours.Click += new System.EventHandler(this.btn_fours_Click);
            // 
            // btn_twos
            // 
            this.btn_twos.Location = new System.Drawing.Point(106, 59);
            this.btn_twos.Name = "btn_twos";
            this.btn_twos.Size = new System.Drawing.Size(75, 23);
            this.btn_twos.TabIndex = 6;
            this.btn_twos.Text = "Up By 2\'s";
            this.btn_twos.UseVisualStyleBackColor = true;
            this.btn_twos.Click += new System.EventHandler(this.btn_twos_Click);
            // 
            // lst_table
            // 
            this.lst_table.FormattingEnabled = true;
            this.lst_table.Location = new System.Drawing.Point(43, 115);
            this.lst_table.Name = "lst_table";
            this.lst_table.Size = new System.Drawing.Size(199, 95);
            this.lst_table.TabIndex = 5;
            // 
            // Form2
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(284, 261);
            this.Controls.Add(this.btn_ones);
            this.Controls.Add(this.btn_fours);
            this.Controls.Add(this.btn_twos);
            this.Controls.Add(this.lst_table);
            this.Name = "Form2";
            this.Text = "Form2";
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button btn_ones;
        private System.Windows.Forms.Button btn_fours;
        private System.Windows.Forms.Button btn_twos;
        private System.Windows.Forms.ListBox lst_table;
    }
}