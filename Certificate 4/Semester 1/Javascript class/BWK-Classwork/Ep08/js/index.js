let outputZone = document.getElementById("outputZone");

outputZone.innerHTML = "Process Started...";

if (window.XMLHttpRequest || window.ActiveXObject) {
    if (window.ActiveXObject) {
        try {
            xhr = new ActiveXObject("Msxml2.XMLHTTP");
        } catch (exception) {
            xhr = new ActiveXObject("Microsoft.XMLHTTP");
        }
    } else {
        xhr = new XMLHttpRequest();
    }
} else {
    alert("Your browser does not support XMLHTTP Request...!");
}

let timeDelay = 0;
let theRecursiveTimer = setTimeout(function request() {
    getAQuote();
    let delayProbability = Math.random();
    timeDelay += (delayProbability > 0.8 ? 250 : (delayProbability < 0.2 ? -250 : 0));
    timeDelay = timeDelay < 1000 ? 1000 : timeDelay > 3500 ? 3500 : timeDelay;
    theRecursiveTimer = setTimeout(request, timeDelay);
}, timeDelay);


function getAQuote() {
    xhr.open("GET", "http://web.tdm.local/NMT/JS-Classwork/Ep08/files/random-quote.php", false);    // Make sure file is in same server
    // xhr.open("GET", "./files/random-quote.php", true);    // Make sure file is in same server
    xhr.send(null);

    xhr.onreadystatechange = function () {
        if (xhr.readyState == 4) {
            if (xhr.status == 200) {
                // alert(xhr.responseText);
                let quoteResponse = xhr.responseText;
                let quoteDetails = JSON.parse(quoteResponse, (key, value) => {
                    if (typeof value === 'string') {
                        return value.replace("\"", "&ldquo;", 1)
                            .replace("\"", "&rdquo;", 1)
                            .replace("'", "&lsquo;")
                            .replace("\n", "<br/>")
                            .replace("...", "&hellip;")
                    }
                    return value;
                });
                outputZone.innerHTML =
                    //"<p>Response State : " + xhr.readyState + " / Delay Time: " + timeDelay + "</p>" +
                    "<blockquote class='blockquote'>" +
                    quoteDetails.quote + " <br/>" +
                    "<em><small>" + quoteDetails.quoteBy + "</small></em>" +
                    "</blockquote>";
            } else {
                outputZone.innerHTML =
                    "<p>Error Code: " + xhr.status + "</p>" +
                    "<p>Error Message: " + xhr.statusText + "</p>";
            }
        }
    };
}
