"use strict";

let people = [{
    "name": "George",
    "occupation": "Truckie",
    "locations": {
        "main": "Perth",
        "secondary": "Joondalup",
        "ternary": null,
    },
    "height": 1.82,
},
    {
        "name": "Alan",
        "occupation": "chef",
        "locations": {
            "main": "Joondalup",
        },
        "height": 1.7,
    },
];


let outputArea = document.getElementById("output");

for (let count in people) {
    outputArea.innerHTML += "<br><p>";
    for (let key in people[count]) {
        outputArea.innerHTML += key + " --> " + people[count][key] + "<br>";
    }
    outputArea.innerHTML += "</p>";
}