import java.util.Scanner;

public class IntegerDemoInteractiveWithName {

   public static void main(String[] args) {
   
      int anInt;
      byte aByte;
      short aShort;
      long aLong;
      String name;
   
      Scanner input = new Scanner(System.in);
      
      System.out.print("Enter an int: ");
      anInt = input.nextInt();
      System.out.print("Enter a byte: ");
      aByte = input.nextByte();
      System.out.print("Enter a short: ");
      aShort = input.nextShort();
      System.out.print("Enter a long: ");
      aLong = input.nextLong();
      input.nextLine();
      System.out.print("Please enter your name: ");
      name = input.nextLine();
      
      System.out.println("Thank you, " + name);
      System.out.println("The int is: " + anInt + "\nThe byte is: " + aByte + "\nThe short is: " + aShort + "\nThe long is: " + aLong);

   
   }

}