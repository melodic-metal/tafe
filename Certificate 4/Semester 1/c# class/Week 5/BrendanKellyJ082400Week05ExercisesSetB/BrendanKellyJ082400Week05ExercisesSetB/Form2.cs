﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace BrendanKellyJ082400Week05ExercisesSetB
{
    public partial class Form2 : Form
    {
        public Form2()
        {
            InitializeComponent();
        }

        private void btn_ones_Click(object sender, EventArgs e)
        {
            lst_table.Items.Clear();
            lst_table.Items.Add("Number\t\tSquared");
            for (int i = 0; i <= 12; i++)
            {
                lst_table.Items.Add(i.ToString() + "\t\t" + Math.Pow(i, 2));
            }
        }

        private void btn_twos_Click(object sender, EventArgs e)
        {
            lst_table.Items.Clear();
            lst_table.Items.Add("Number\t\tSquared");
            for (int i = 0; i <= 12; i = i + 2)
            {
                lst_table.Items.Add(i.ToString() + "\t\t" + Math.Pow(i, 2));
            }

        }

        private void btn_fours_Click(object sender, EventArgs e)
        {
            lst_table.Items.Clear();
            lst_table.Items.Add("Number\t\tSquared");
            for (int i = 25; i >= 0; i = i - 4)
            {
                lst_table.Items.Add(i.ToString() + "\t\t" + Math.Pow(i, 2));
            }
        }
    }
}
