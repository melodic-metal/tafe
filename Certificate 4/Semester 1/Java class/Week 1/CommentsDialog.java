import javax.swing.JOptionPane;
public class CommentsDialog {

   public static void main(String[] args) {
   //This is a single line comment
   /* This is
      a multi line
      comment
   */

   JOptionPane.showMessageDialog(null, "Program comments are nonexecuting statements you add to a file for the purpose of documentation.");
   
   }

}