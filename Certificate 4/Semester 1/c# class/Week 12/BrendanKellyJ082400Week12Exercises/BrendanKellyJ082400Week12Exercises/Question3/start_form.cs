﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace BrendanKellyJ082400Week12Exercises.Question3
{
    public partial class start_form : Form
    {
        public static decimal dec_total;
        public start_form()
        {
            InitializeComponent();
            txt_total.ReadOnly = true;
            
        }

        private void btn_small_Click(object sender, EventArgs e)
        {
            small_car_form SCForm = new small_car_form();
            SCForm.ShowDialog();
            txt_total.Text = dec_total.ToString();
        }

        private void btn_med_Click(object sender, EventArgs e)
        {
            medium_car_form MCForm = new medium_car_form();
            MCForm.ShowDialog();
            txt_total.Text = dec_total.ToString();
        }

        private void btn_large_Click(object sender, EventArgs e)
        {
            large_car_form LCForm = new large_car_form();
            LCForm.ShowDialog();
            txt_total.Text = dec_total.ToString();
        }
    }
}
