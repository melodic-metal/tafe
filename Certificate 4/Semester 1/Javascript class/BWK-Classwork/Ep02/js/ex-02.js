/**
 *
 *
 * Calculations in JS
 *
 *
 * @File:       /Ep02/js/ex-02.js
 * @Project:    Classwork
 * @Author:     Brendan Kelly <j082400@tafe.wa.edu.au>
 * @Version:    1.0
 * @Copyright:  Brendan Kelly 2018
 *
 * History:
 * v1.0    2018-07-24
 *         Calculations and stuff
 *
 * v0.1    2018-07-24
 *         Template as provided by Adrian Gould
 *
 */

let length = 6;
let height = 12;

console.log("Area of rectangle is: " + length * height);