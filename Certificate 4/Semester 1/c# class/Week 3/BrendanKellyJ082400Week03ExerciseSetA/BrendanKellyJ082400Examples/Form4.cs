﻿// Programmer : Brendan Kelly 
// Date : August 2018
// Purpose : This application calculates number of weeks and days in a given number of weeks

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace BrendanKellyJ082400Examples
{
    public partial class Form4 : Form
    {
        public Form4()
        {
            InitializeComponent();
        }

        private void btnCalculate_Click(object sender, EventArgs e)
        {
            int intWeeks, intDays;

            intWeeks = int.Parse(txtInput.Text) / 7;
            intDays = int.Parse(txtInput.Text) % 7;

            lblOutput.Text = "That is " + intWeeks + " weeks and " + intDays + " days";
        }
    }
}
