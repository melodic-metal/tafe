// Date: 25/08/2018
// Programmer: Brendan Kelly J082400
// Purpose: Demo Increment

public class DemoIncrement {

   public static void main(String[] args) {
      int v = 4;
      int plusPlusV = ++v;
      v = 4;
      int vPlusPlus = v++;
      
      System.out.println("v is " + v);
      System.out.println("++v is " + plusPlusV);
      System.out.println("v++ is " + vPlusPlus);
      
   }

}