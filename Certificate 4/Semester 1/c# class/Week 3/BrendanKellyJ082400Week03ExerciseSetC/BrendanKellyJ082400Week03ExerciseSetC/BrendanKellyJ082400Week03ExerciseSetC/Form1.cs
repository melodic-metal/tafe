﻿// Programmer : Brendan Kelly 
// Date : August 2018
// Purpose : Appplication takes in input of sphere radius, then calculates the volume of the sphere

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace BrendanKellyJ082400Week03ExerciseSetC
{
    public partial class Form1:Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void btnCalculate_Click(object sender, EventArgs e)
        {
            double dblRadius = double.Parse(txtInput.Text);
            double dblVolume = (4.00 / 3.00) * Math.PI * Math.Pow(dblRadius, 3);
            lblOutput.Text = "The Volume is: " + dblVolume.ToString();
            
        }
    }
}
