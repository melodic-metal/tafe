// Date: 13/09/2018
// Programmer: Brendan Kelly
// Purpose: hold data regarding items sold

public class ItemSold {

   private static int invoiceNumber;
   private static String description;
   private static double price;
   
   public int getInvoiceNumber() {
      return invoiceNumber;
   }
   public static String getDescription() {
      return description;
   }
   public static double getPrice() {
      return price;      
   }
   
   public static void setInvoiceNumber(int in) {
      invoiceNumber = in;
   }
   public static void setDescription(String desc) {
      description = desc;
   }
   public static void setPrice(double pr) {
      price = pr;
   }
   
   
}