﻿namespace BrendanKellyJ082400Week12Exercises.Question3
{
    partial class small_car_form
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lbl_hire = new System.Windows.Forms.Label();
            this.txt_hire = new System.Windows.Forms.TextBox();
            this.btn_calc = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // lbl_hire
            // 
            this.lbl_hire.AutoSize = true;
            this.lbl_hire.Location = new System.Drawing.Point(29, 62);
            this.lbl_hire.Name = "lbl_hire";
            this.lbl_hire.Size = new System.Drawing.Size(101, 13);
            this.lbl_hire.TabIndex = 0;
            this.lbl_hire.Text = "Number of days hire";
            // 
            // txt_hire
            // 
            this.txt_hire.Location = new System.Drawing.Point(161, 58);
            this.txt_hire.Name = "txt_hire";
            this.txt_hire.Size = new System.Drawing.Size(100, 20);
            this.txt_hire.TabIndex = 1;
            // 
            // btn_calc
            // 
            this.btn_calc.Location = new System.Drawing.Point(97, 150);
            this.btn_calc.Name = "btn_calc";
            this.btn_calc.Size = new System.Drawing.Size(75, 23);
            this.btn_calc.TabIndex = 2;
            this.btn_calc.Text = "Calculate";
            this.btn_calc.UseVisualStyleBackColor = true;
            this.btn_calc.Click += new System.EventHandler(this.btn_calc_Click);
            // 
            // SmallCarForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(284, 261);
            this.Controls.Add(this.btn_calc);
            this.Controls.Add(this.txt_hire);
            this.Controls.Add(this.lbl_hire);
            this.Name = "SmallCarForm";
            this.Text = "SmallCarForm";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lbl_hire;
        private System.Windows.Forms.TextBox txt_hire;
        private System.Windows.Forms.Button btn_calc;
    }
}