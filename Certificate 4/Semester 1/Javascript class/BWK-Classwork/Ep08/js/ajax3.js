/*
 * AJAX3.JS
 *
 * Initialise the XML HTTP request
 *
 */

let xhr = null;
if (window.XMLHttpRequest || window.ActiveXObject) {
    if (window.XMLHttpRequest) {
        xhr = new XMLHttpRequest();
    }
    else {
        try {
            xhr = new ActiveXObject("Msxml2.XMLHTTP");
        }
        catch (exception) {
            xhr = new ActiveXObject("Microsoft.XMLHTTP");
        }
    }
    console.log(xhr);
    doAjaxRequest();
}

else {
    alert("no ajax 4 u");
}

function doAjaxRequest() {
        //xhr.open("GET", "FILE/RESOURCE", true);
        xhr.open("GET", "files/welcome.txt", true);
        xhr.send();
        console.log(xhr);
}