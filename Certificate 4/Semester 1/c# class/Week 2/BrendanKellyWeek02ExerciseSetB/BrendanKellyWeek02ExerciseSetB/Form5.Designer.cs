﻿namespace BrendanKellyWeek02ExerciseSetB
{
    partial class Form5
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lblChanged = new System.Windows.Forms.Label();
            this.txtChanged = new System.Windows.Forms.TextBox();
            this.SuspendLayout();
            // 
            // lblChanged
            // 
            this.lblChanged.AutoSize = true;
            this.lblChanged.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblChanged.Location = new System.Drawing.Point(67, 138);
            this.lblChanged.Name = "lblChanged";
            this.lblChanged.Size = new System.Drawing.Size(2, 15);
            this.lblChanged.TabIndex = 0;
            // 
            // txtChanged
            // 
            this.txtChanged.Location = new System.Drawing.Point(70, 84);
            this.txtChanged.Name = "txtChanged";
            this.txtChanged.Size = new System.Drawing.Size(100, 20);
            this.txtChanged.TabIndex = 1;
            this.txtChanged.TextChanged += new System.EventHandler(this.txtChanged_TextChanged);
            // 
            // Form5
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(284, 261);
            this.Controls.Add(this.txtChanged);
            this.Controls.Add(this.lblChanged);
            this.Name = "Form5";
            this.Text = "Form5";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lblChanged;
        private System.Windows.Forms.TextBox txtChanged;
    }
}